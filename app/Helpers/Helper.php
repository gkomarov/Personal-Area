<?php

namespace App\Helpers;

class Helper
{
    public static function isLimitExceeded(int $currentQuantity)
    {
        $maxQuantity = 5;

        if ($currentQuantity >= $maxQuantity) {
            return true;
        }

        return false;
    }
}