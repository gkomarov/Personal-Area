<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use App\Services\Payment\IPayment as IPaymentService;
use App\Models\Payment\IPayment as IPaymentModel;
use App\Services\Pdf\IPdf;
use App\Models\Additional;
use App\Models\AdditionalList;
use App\Models\Auto;
use App\Models\Users;
use League\Flysystem\Config;
use DB;

class AdditionalController extends Controller
{
    private $additionalModel;
    private $userModel;
    private $pdfService;

    public function __construct(Additional $additionalModel, Users $userModel, IPdf $pdfService)
    {
        $this->additionalModel = $additionalModel;
        $this->userModel = $userModel;
        $this->pdfService = $pdfService;
        $this->middleware('admin_user');
    }

    public function index()
    {
        if (Auth::user()->role < 3) {
            $additionals = $this->additionalModel->getAll();
            $act_additionals = $this->additionalModel->getDone();

            return view('additionals.admin', [
                'additionals' => $additionals,
                'act_additionals' => $act_additionals
            ]);
        } else {
            $additionals = $this->additionalModel->getAllByUser(Auth::id());
            
            return view('additionals.index', ['additionals' => $additionals]);
        }
    }

    public function store(Request $request)
    {
        if (Auth::user()->role > 3) {
            App::abort(403,'Нет прав');
        }

        $this->additionalModel->create($request->all());
        $newAdditional = $this->additionalModel->getSingleByUser();

        return json_encode($newAdditional);
    }

    public function update(Request $request)
    {
        return $this->additionalModel->where('id', '=', $request->additional)->update([
            'status' => $request->status
        ]);
    }

    public function lists()
    {
        $additionalList = AdditionalList::all();
        return json_encode($additionalList);
    }

    /**
     * Collections data for payment
     *
     * @param IPaymentService $payment_service - implementing interface IPaymentService
     * @param IPaymentModel $payment_model - implementing interface IPaymentModel
     * @param Additional $model - model for work with data
     * @return status code
     */
    public function pay(IPaymentService $payment_service, IPaymentModel $payment_model, Request $request)
    {
        if ($payment_service->checkTotalPrice($this->additionalModel, $request)) {
            $data['sum'] = $request->total_price;
            $payment_id = $payment_model->store($data);
            $payment = $payment_model->show($payment_id);

            $additionals = [];
            foreach ($_POST['additionals'] as $key => $additional) {
                $additionals[$key]['additional_id'] = $additional['additional_id'];
                $additionals[$key]['payment_id'] = $payment_id;
                //$additionals[$key]['close_date'] = $additional['close_date'];
                $additionals[$key]['comment'] = $additional['comment'];
                $additionals[$key]['status'] = 0;
                $additionals[$key]['user_id'] = Auth::id();
            }

            $this->additionalModel->order($additionals);

            $amount = $request->total_price;
            $payment_description = 'Дополнительные услуги';
            $payment_params = $payment_service->formPaymentParams($amount, $payment->num, $payment_description, $request->redirect_url);

            return response()->json($payment_params);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Bad request'
            ], 400);
        }
    }

    /**
     * Pays unpaid payments
     *
     * @param Additional $additional - model for work with data
     * @param IPaymentService $payment_service - realization interface IPaymentService
     * @param IPaymentModel $payment_model - realization interface IPaymentModel
     * @param Request $request - current request
     * @return payment params or error
     */
    public function payUnpaid(IPaymentService $payment_service, IPaymentModel $payment_model, Request $request)
    {
        if ($payment_service->checkTotalPrice($this->additionalModel, $request)) {
            $additional = $this->additionalModel->getUnpaid(Auth::user()->id);
            $payment = $payment_model->show($additional->payment_id);

            $payment_description = 'Дополнительные услуги';
            $payment_params = $payment_service->formPaymentParams($payment->sum, $payment->num, $payment_description, $request->redirect_url);

            return response()->json($payment_params);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Bad request'
            ], 400);
        } 
    }

    public function makePdfAct(Request $request)
    {
        $selected_additional = $this->additionalModel->find($request->id);
        $data['payment_id'] = $selected_additional->payment_id;
        $data['additionals'] = $this->additionalModel->getAllByPayment($data['payment_id']);
        $data['user'] = $this->userModel->getUser($selected_additional->user_id);
        $data['date'] = date("Y-m-d H:i:s");
        $data['date_format'] = 'full';
        $data['page_orientation'] = 'P';
        $data['template'] = 'additionals.act';

        return $this->pdfService->output($data, $output='display', $data['user']->email);
    }

    public function sendAct(Request $request)
    {
        $request->validate([
            'payment_id' => 'required',
            'date' => 'required'
        ]);

        $data['payment_id'] = (int) $request->payment_id;

        $additionals = $this->additionalModel->getAllByPayment($data['payment_id']);
        $data['additionals'] = $additionals->where('status', 3)->all();

        $selected_additional = $this->additionalModel->where('payment_id', $request->payment_id)->first();
        $data['user'] = $this->userModel->getUser($selected_additional->user_id);

        $data['date'] = $request->date;
        $data['date_format'] = 'full';
        $data['page_orientation'] = 'P';
        $data['template_pdf_content'] = 'additionals.act';
        $data['template_email'] = 'emails.act';
        $data['subject_email'] = 'Акт №'.$data['payment_id'];
        $data['file_name'] = ' Акт('.$data['payment_id'].').pdf';

        $emails = config('contacts.act_emails');

        array_push($emails, $data['user']->email);

        $this->pdfService->output($data, $output='email', $emails);

        return $this->additionalModel->changeStatus('payment_id', $request->payment_id, 5);
    }
}