<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Street;
use App\Models\Building;
use Auth;

class BuildingController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'type' => 'required|string',
            'building_number' => 'required|string|max:5',
            'apart_type' => 'string|max:50|nullable',
            'apart_number' => 'string|max:20|nullable',
            'floor' => 'integer|nullable',
            'street_id' => 'integer',
        ]);

        return $validator;
    }

    public function index(Request $request)
    {
        $streets = Street::all();
        
        if (!$request['street']) {
            $request['street'] = 1;
        }

        $active_street = $request['street'];

        $buildings = Building::getBuildingsByStreet($request['street']);

        return view('admin.index', [
            'streets' => $streets,
            'buildings' => $buildings, 
            'active_street' => $active_street
        ]);
    }

    public function store(Building $buildingModel, Request $request)
    {
        $error = $this->validator($request->all())->validate();

        if ($error) {
            return $error;
        }

        return $buildingModel->create($request->all());
    }

    public function edit(Building $buildingModel)
    {
        $single = $buildingModel->getBuildingById($_GET['id']);
        return json_encode($single);
    }

    public function update(Building $buildingModel, Request $request)
    {
        $error = $this->validator($request->all())->validate();

        if ($error){
            return $error;
        }

        return $buildingModel->where('id', '=', $request['id'])->update($request->except(['_method','_token']));
    }

    public function remove(Building $buildingModel)
    {
        return $buildingModel->where('id', '=', $_POST['id'])->update(
            ['alive'=>false]
        );
    }
}
