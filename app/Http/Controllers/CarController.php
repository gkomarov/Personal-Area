<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\Car;
use App\Services\OpenGate\IOpenGate;
use DB;

class CarController extends Controller
{
    private $open_gate_service;
    private $number_is_not_unique = true;

    public function __construct(IOpenGate $open_gate)
    {
        $this->open_gate_service = $open_gate;
    }

    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'mark' => 'required|string|max:255',
            'number' => 'required|string|min:6|max:10',
            'model' => 'string|max:255'
        ]);

        $validator->after(function ($validator) {
            if ($this->number_is_not_unique == false) {
                $validator->errors()->add('number', 'Машина с таким номером уже существует');
            }
        });

        return $validator;
    }

    public function store(Car $carModel, Request $request)
    {
        $numberWithoutSpaces = str_replace(' ', '', $request->number);

        Input::merge([
            'number' => mb_convert_case($numberWithoutSpaces, MB_CASE_UPPER, 'UTF-8'),
            'mark' => mb_convert_case($request->mark, MB_CASE_UPPER, 'UTF-8'),
            'model' => mb_convert_case($request->model, MB_CASE_UPPER, 'UTF-8')
        ]);

        $request['user_id'] = (Auth::id());
        
        $car_in_open_gate = $this->open_gate_service->createCar($request->all());

        if ($car_in_open_gate == 2601) {
            $this->number_is_not_unique = false;
        }

        $this->validator($request->all())->validate();

        if ($car_in_open_gate != NULL) {
            return response('Internal Server Error', 500);
        }

        $result = $carModel->create($request->all());

        $str = '<tr class="table-item" id="'.$result['id'].'">
                    <td>
                        <div>
                            <button class="button button-transparent button-icon" action="guests" data-mode="delete">
                            <img src="/img/icon/admin/street/delete.svg"></button>
                        </div>
                  </td>
                  <td>
                    <div>'.$result['number'].'</div>
                  </td>';

        if (!$result->personal){
            $str .= $str .= '<td>'.$result['close_date'].'<span class="color-green"> ('.$result['close_count'].'д.)</span></td>';
        }

        $str .= '<td>'.$result['mark'].' '.$result['model'].'</td>
                </tr>';

        return $str;
    }

    public function edit(Car $carModel, Request $request)
    {
        $single = $carModel->getCarById($request->id);
        
        if ($single->personal == 1) {
            unset($single->close_date);
        }

        return json_encode($single);
    }

    public function update(Car $carModel, Request $request)
    {
        $numberWithoutSpaces = str_replace(' ', '', $request->number);

        Input::merge([
            'number' => mb_convert_case($numberWithoutSpaces, MB_CASE_UPPER, 'UTF-8'),
            'mark' => mb_convert_case($request->mark, MB_CASE_UPPER, 'UTF-8'),
            'model' => mb_convert_case($request->model, MB_CASE_UPPER, 'UTF-8'),
            'old_number' => $carModel->where('id', '=', $request->id)->first()->number,
        ]);

        $car_in_open_gate = $this->open_gate_service->updateCar($request->old_number, $request->all());

        if ($car_in_open_gate == 2601) {
            $this->number_is_not_unique = false;
        }

        $this->validator($request->all())->validate();

        if ($car_in_open_gate != NULL) {
            return response('Internal Server Error', 500);
        }

        return $carModel->where('id', '=', $request->id)->update($request->except(['_method', '_token', 'databaseId']));
    }

    public function remove(Car $carModel, Request $request)
    {
        $number = $carModel->where('id', '=', $request->id)->first()->number;

        if ($this->open_gate_service->deleteCar($number) != NULL) {
            return response('Internal Server Error', 500);
        }
        
        return $carModel->where('id', '=', $request->id)->update(
            ['alive' => false]
        );
    }
}
