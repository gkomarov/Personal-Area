<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function index(Contact $contactModel)
    {
        $contacts = $contactModel->getAllContacts();
        return view('contact.index', ['contacts' => $contacts]);
    }

    public function store(Contact $contactModel, Request $request)
    {
        var_dump(2); die();
        $contactModel->create($request->all());
        return redirect()->route('contacts.index');
    }
    public function edit(Contact $contactModel)
    {
        $single = $contactModel->getcontactById($_GET['contact']);
        return json_encode($single);
    }

    public function remove(Contact $contactModel)
    {
        return $contactModel->where('id', '=', $_POST['contact'])->update(['alive'=>false]);
    }

    public function update(Contact $contactModel, Request $request)
    {
        $contactModel->where('id', '=', $request['id'])->update($request->except(['_method','_token']));
        return redirect()->back();
    }
}
