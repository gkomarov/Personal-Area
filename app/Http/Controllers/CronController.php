<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use App\Models\Invoice\IInvoice;
use App\Services\OneC\IOneC;
use App\Services\DateTime\IDateTime;
use App\Models\Payment\IPayment;
use App\Models\Building;
use App\Models\Receipt;
use App\Services\Pdf\IPdf;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use App\Models\Car;
use App\Services\OpenGate\IOpenGate;

class CronController extends Controller
{
    private $one_c;
    private $datetime_service;
    private $invoice;

    public function __construct(
        IOneC $one_c, 
        IDateTime $datetime_service, 
        IInvoice $invoice, 
        Building $building,
        IPdf $pdf_service
    )
    {
        $this->one_c = $one_c;
        $this->datetime_service = $datetime_service;
        $this->pdf_service = $pdf_service;
        $this->invoice = $invoice;
        $this->building = $building;
    }

    /** 
     * Collects data for confirm payments
     *
     * @param IPayment $payment - realization interface IPayment
     * @param IOneC $one_c - realization interface IOneC
     */
    public function confirmPayments(IPayment $payment)
    {
        $success_payments = $payment->getSuccessPayments();

        $confirm_payments = [];

        $i = 0;
        foreach ($success_payments as $success_payment) {
            if ($this->one_c->checkExistsPayment($success_payment->num)) {
                $confirm_payments[$i] = $success_payment->num;
                $i++; 
            }
        }

        return $payment->confirm($confirm_payments);
    }

    /**
     * Adds counteragent key to user
     *
     * @param IOneC $one_c - realization interface IOneC
     * @return counteragent key
     */
    public function addCounteragentKey()
    {
        $users_without_counteragent_key = DB::table('users')->where([
            ['counteragent_key', '=', ''], 
            ['role', '>', 1]
        ])->get();
        
        if ($users_without_counteragent_key) {
            /*
                Сделанно так потому что multiple запросы в 1C работают дольше, да и вообще он работает дольше.
            */
            foreach ($users_without_counteragent_key as $user) {
                $user_invoice_num = (int) $user->invoice_num;
                $counteragent_key = $this->one_c->getCounteragentKey($user_invoice_num);
                
                if ($counteragent_key) {
                    DB::table('users')->where('invoice_num', $user->invoice_num)->update(
                        ['counteragent_key' => $counteragent_key]
                    );
                }
            }
        }
    }

    /**
     * Marks unpaid payments (change status on 5)
     *
     * @return true or false
     */
    public function markUnpaidPayments()
    {
        DB::transaction(function() {
            DB::table('payments')->where('status', '=', 0)->update(['status' => 5]);
            DB::table('additionals')->where('status', '=', 0)->update(['status' => 5]);
        });
    }

    /**
     * Collects data for form invoice in pdf
     *
     * @param $output - 
     * @param $user - 
     * @return invoice pdf
     */
    public function showInPdf($user, $output='display')
    {
        $date_start = '';
        $modify_start = '- 1 month';
        $period['start'] = $this->datetime_service->formStartPeriod($date_start, $modify_start);

        $date_end = '';
        $modify_end = '- 1 month';
        $period['end'] = $this->datetime_service->formEndPeriod($date_end, $modify_end);

        $data['invoice'] = $this->invoice->getPreviousMonth($period, $user['counteragent_key']);

        if ($data['invoice'] == null) {
            return null;
        }

        $data['balance'] = $this->one_c->getClientBalancePrev($user['invoice_num']);
        $data['building'] = $this->building->getBuildingById($user['building_id']);

        if ($data['invoice'] && $data['balance'] && $data['building']) {

            $data['invoice_num'] = preg_replace('~[^0-9]+~','', $data['balance']->Name);
            $data['file_name'] = ' Квитанция('.$data['invoice_num'].').pdf';
            $data['date'] = $data['invoice']->Date;
            $data['date_format'] = 'shirt';
            $data['template_pdf_content'] = 'invoice.pdf';
            $data['template_email'] = 'emails.receipt';
            $data['subject_email'] = 'Квитанция за';

            return $this->pdf_service->output(
                $data,
                $output, 
                $user['email']
            );
        }
    }

    public function sentEmailReceiptPdf(Users $userModel, Receipt $receiptModel)
    {
        /**Функция должна выполняться порционно. Каждая итерация ресурсоёмкая, поэтому запрос
         * быстро возвращает Bad Getaway. Стоит cron, который выполняется раз в 5 минут.
         * Так порционно отправляются все квитанции.
         */

        $users = $userModel->getAllLodgers();
        
        $receipts = $receiptModel->getAll();

        /**Этот блок добавляет пользователей, которых нет в таблице квитанций*/
        if (count($receipts) == 0) {
            foreach ($users as $user) {
                $receiptModel->create([
                    'title' => 'Квитанция', 
                    'user_id' => $user->id, 
                    'sent' => 0
                ]);
            }

            //Новая выборка после добавления
            $receipts = $receiptModel->getAll();
        } elseif (count($receipts) < count($users)) {
            $receipts_user_ids = [];

            foreach ($receipts as $receipt){
                array_push($receipts_user_ids, $receipt->user_id);
            }

            foreach ($users as $user){
                if (!in_array($user->id, $receipts_user_ids)) {
                    $receiptModel->create([
                        'title' => 'Квитанция', 
                        'user_id' => $user->id, 
                        'sent' => 0
                    ]);
                }
            }

            //Новая выборка после добавления
            $receipts = $receiptModel->getAll();
        }

        /**Выборка из списка квитанций всех, тех, кому они не отправились*/
        $not_recived_receipts = $receipts->filter(function($item){ return $item->sent == 0; });

        if (count($not_recived_receipts) != 0) {
            foreach ($users as $user){
                foreach ($not_recived_receipts as $item) {
                    if ($user->id == $item->user_id) {
                        $send = $this->showInPdf($user, 'email');
                        if ($send != null){
                            $receiptModel->where("user_id", "=", $user->id)->update(["sent" => 1]);
                        }
                    }
                }
            }
        }
    }

    public function resetReceipt(Receipt $receiptModel)
    {
        $receiptModel->where("user_id", "!=", "0")->update(["sent" => 0]);
    }

    public function deleteCarsWithExpiredPermition(Car $cars, IOpenGate $open_gate)
    {
        $expired_cars = $cars->getExpired();

        foreach ($expired_cars as $expired_car) {
            $open_gate->deleteCar($expired_car->number);
        }

        return 'deleteCarsWithExpiredPermition';
    }

    public function test(IOpenGate $open_gate)
    {
        // $personals = DB::table('cars')->select(DB::raw('DISTINCT(number) as number'))->where([
        //     ['personal', '=', 1],
        //     ['alive', '=', 1]
        // ])->get();

        // $data = [];
        // foreach ($personals as $personal) {
        //     $data['number'] = $personal->number;
        //     $data['databaseId'] = 2;
        //     $open_gate->createCar($data);
        // }

        // $guests = DB::table('cars')->select(DB::raw('DISTINCT(number) as number'))->where([
        //     ['personal', '=', 0],
        //     ['alive', '=', 1]
        // ])->get();

        // $data = [];
        // foreach ($guests as $guest) {
        //     $data['number'] = $guest->number;
        //     $data['databaseId'] = 4;
        //     $open_gate->createCar($data);
        // }

        // dd($open_gate->test());
    }
}