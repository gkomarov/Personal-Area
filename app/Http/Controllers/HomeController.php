<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Models\AdditionalList;
use App\Models\Additional;
use App\Models\Indication;
use App\Models\Balance\IBalance;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request, IBalance $balance)
    {
        $this->addUserPhoneToSession($request);

        //$balance = $balance->show(Auth::user()->invoice_num);
        //$indications = Indication::getByUser(Auth::user()->id);
        $additionals = Additional::getAllByUser(Auth::user()->id);
        $additional_list = AdditionalList::all();

        $additionals_count = 0;
        foreach ($additionals as $item) {
            if ($item->status < 3){
                $additionals_count ++;
            }
        }

        return view('user.index', [
            'additionals' => $additionals,
            'additionals_count' => $additionals_count,
            'additional_list' => $additional_list,
            //'indications' => $indications,
            //'balance' => $balance,
        ]);
    }

    public function addUserPhoneToSession($request)
    {
        $user_first_phone = DB::table('user_phones')->select('phone')->where('user_id', 1)->get()->first();

        if ($user_first_phone) {
            return $request->session()->put('user_phone', $user_first_phone->phone);
        }

        return '';
    }
}
