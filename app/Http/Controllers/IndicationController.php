<?php

namespace App\Http\Controllers;

use App\Models\Indication;
use App\Models\Street;
use App\Models\User;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class IndicationController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'water' => 'required|string',
            'energy' => 'required|string',
        ]);

        return $validator;
    }

    public function index(Request $request, Indication $indicationModel, Users $usersModel)
    {
        $streets = Street::all();

        if (!$request['street']) {
            $request['street'] = 2;
        }

        $indications_by_street = $indicationModel->getByStreet($request['street']);
        $users = $usersModel->getByStreet($request['street']);
        $users_without_indications = $usersModel->getUsersWithoutIndications();

        return view('indications.index', [
            'streets' => $streets, 
            'indications' => $indications_by_street,
            'users_without_indications' => $users_without_indications,
            'active_street' => $request['street'],
        ]);
    }


    public function store(Indication $indicationModel, Request $request)
    {
        return $indicationModel->create($request->all());
    }

    public function edit(Indication $indicationModel, Request $request)
    {
        $single = $indicationModel->getById($request->id);
        return json_encode($single);
    }

    public function update(Indication $indicationModel, Request $request)
    {
        $error = $this->validator($request->all())->validate();

        if ($error){
            return $error;
        };
        return $indicationModel->where('id', '=', $request->id)->update($request->except(['_method', '_token']));
    }

}
