<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice\IInvoice;
use App\Services\OneC\IOneC;
use App\Services\Payment\IPayment as IPaymentService;
use App\Models\Payment\IPayment as IPaymentModel;
use App\Services\DateTime\IDateTime;
use App\Services\Pdf\IPdf;
use App\Models\Additional;
use App\Models\Building;
use App\Models\Users;
use Illuminate\Support\Facades\Redirect;
use function PHPSTORM_META\map;
use DateTime;
use Auth;

class InvoiceController extends Controller
{
    private $invoice;
    private $one_c;
    private $datetime_service;
    private $pdf_service;
    private $building;

    public function __construct(IInvoice $invoice, IOneC $one_c, IDateTime $datetime_service, IPdf $pdf_service, Building $building)
    {
        $this->invoice = $invoice;
        $this->one_c = $one_c;
        $this->datetime_service = $datetime_service;
        $this->pdf_service = $pdf_service;
        $this->building = $building;
    }

    /**
     * Collects invoice previous month from current 
     *
     * @param IInvoice $invoice - realization interface IInvoice
     * @param IOneC $one_c - realization interface IOneC
     * @return html view
     */
    public function index()
    {
        $date_start = '';
        $modify_start = '- 1 month';
        $period['start'] = $this->datetime_service->formStartPeriod($date_start, $modify_start);

        $date_end = '';
        $modify_end = '- 1 month';
        $period['end'] = $this->datetime_service->formEndPeriod($date_end, $modify_end);

        if (Auth::user()->counteragent_key) {
            $invoice = $this->invoice->getPreviousMonth($period, Auth::user()->counteragent_key);
            $invoices = $invoice;
        } else {
            $invoices = [];
        }

        return view('invoice.index', [
            'invoices' => $invoices, 
            'period' => $period
        ]);
    }

    /**
     * Collects data for form invoice in pdf
     *
     * @param $output - 
     * @param $user - 
     * @return invoice pdf
     */
    public function showInPdf(Request $request, $output='display')
    {
        $date_start = '';
        $modify_start = '- 1 month';
        $period['start'] = $this->datetime_service->formStartPeriod($date_start, $modify_start);

        $date_end = '';
        $modify_end = '- 1 month';
        $period['end'] = $this->datetime_service->formEndPeriod($date_end, $modify_end);
        
        $data['invoice'] = $this->invoice->getPreviousMonth($period, Auth::user()->counteragent_key);
        $data['balance'] = $this->one_c->getClientBalancePrev(Auth::user()->invoice_num);
        $data['building'] = $this->building->getBuildingById(Auth::user()->building_id);

        if ($data['invoice'] && $data['balance'] && $data['building']) {
            $data['invoice_num'] = preg_replace('~[^0-9]+~','', $data['balance']->Name);
            $data['file_name'] = ' Квитанция('.$data['invoice_num'].').pdf';
            $data['date'] = $data['invoice']->Date;
            $data['date_format'] = 'shirt';
            $data['template_pdf_content'] = 'invoice.pdf';

            return $this->pdf_service->output(
                $data,
                $output,
                Auth::user()->email
            );
        }

        return abort(404);
    }

    /**
     * Gets all invoices by select period
     *
     * @param IInvoice $invoice - realization interface IInvoice
     * @param IOneC $one_c - realization interface IOneC
     * @param Request $request - data current request
     * @param $star - date start
     * @param $end - date end
     * @return invoices in json
     */
    public function getAllByPeriod(Request $request)
    {
        $request->validate([
            'start' => 'required'
        ]);

        $new_start = $this->datetime_service->convertPeriod($request['start']);

        $modify_start = '';
        $period['start'] = $this->datetime_service->formStartPeriod($new_start, $modify_start);

        $modify_end = 'last day of';
        $period['end'] = $this->datetime_service->formEndPeriod($new_start, $modify_end);

        $invoice = $this->invoice->getAllByPeriod($period);

        if ($request->type == 'json') {
            return response()->json([
                'invoice' => $invoice
            ]);
        }

        return view('invoice.index', [
            'invoices' => $invoice, 
            'period' => $period
        ]);
    }
}