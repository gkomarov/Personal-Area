<?php

namespace App\Http\Controllers;

use App\Models\Log\ILog;

class LogController extends Controller
{
    public function index(ILog $log)
    {
        $logs = $log->getAll();
        return json_encode($logs);
    }
}