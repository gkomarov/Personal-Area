<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\User;
use App\Models\Users;

class NewsController extends Controller
{
    public function index(News $newsModel, Users $userList) 
    {
        $users = $userList->getAllUsers();
        $news = $newsModel->getAllNews();

        return view('news.index', [
            'news' => $news, 
            'users' => $users
        ]);
    }

    public function store(News $newsModel, Request $request)
    {
        $newsModel->create($request->all());
        return redirect()->route('news.index');
    }

    public function edit(News $newsModel, Request $request)
    {
        $single = $newsModel->getNewsById($request->news);
        return json_encode($single);
    }

    public function update(News $newsModel, Request $request)
    {
        $newsModel->where('id', '=', $request->id)->update($request->except(['_method','_token']));
        return redirect()->route('news.index');
    }

    public function remove(News $newsModel, Request $request)
    {
        return $newsModel->where('id', '=', $request->news)->update(['alive' => false]);
    }
}
