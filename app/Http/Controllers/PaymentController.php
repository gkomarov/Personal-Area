<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\Payment\IPayment as IPaymentService;
use App\Models\Payment\IPayment as IPaymentModel;
use App\Services\Cashbox\ICashbox;
use App\Models\Additional;
use App\Models\Users;
use App\Services\Sms\ISms;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Checks payment sum
     *
     * @param $sum - payment sum
     * @return true or false
     */
    public function checkSum(int $sum)
    {
        if ($sum > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Forms valid price with kopeck (100 руб. = 10000)
     * 
     * @param $price - price in decimal format from db
     * @return valid price in integer format
     */
    public function formValidPrice(string $price)
    {
        $valid_price = str_replace('.', '', $price);
        return (int) $valid_price;
    }

    /**
     * Collects data for saving in db
     *
     * @param IPaymentService $payment_service - realization interface IPaymentService
     * @param IPaymentModel $payment_model - realization interface IPaymentModel
     * @return payments params or error in json
     */
	public function store(IPaymentService $payment_service, IPaymentModel $payment_model, Request $request)
	{
        $data['sum'] = $request->sum;
        $payment_id = $payment_model->store($data);
        $payment = $payment_model->show($payment_id);
        $payment_description = 'Жку';

        $payment_params = $payment_service->formPaymentParams($request->sum, $payment->num, $payment_description, $request->redirect_url);

        if ($this->checkSum($request->sum)) {
            return response()->json($payment_params);
        } else {
            return response()->json([
                'status' => 400,
                'message' => 'Bad request',
            ]);
        }
	}

    /**
     * Handlers response payment system
     *
     * @param IPaymentService $payment_service - realization interface IPaymentService
     * @param IPaymentModel $payment_model - realization interface IPaymentModel
     * @return true or result function cancel
     */
    public function handler(IPaymentService $payment_service, IPaymentModel $payment_model, ICashbox $cashbox_service, Additional $additional, Users $users, ISms $smsService)
    {
        $payment = explode('~', $_POST['DESC']);

        $data['invoice_num'] = (int) $payment[0];
        $data['payment_num'] = $payment[1];
        $data['RC'] = $_POST['RC'];

        $user = $users->getByInvoiceNum($data['invoice_num']);

        if (!$payment_service->checkResponse($data)) {
            // return $payment_service->cancel($payment_model, $data['payment_num']);
            return 1;
        } else {
            $payment_model->updateByNum($data);

            $payment = $payment_model->getByNum($data['payment_num']);

            $additional->updateByPayment($payment->id);
            $additionals = $additional->getAllByPayment($payment->id);

            if ($additionals->count() > 0) {

                $phone = config('contacts.phones.administrator');
                $content = urlencode(
                    'Заказана новая доп.услуга.' . PHP_EOL .
                    'Заказ №' . $payment->id . PHP_EOL .
                    'Закзачик: ' . $user->last_name . ' ' . $user->first_name . ' ' . $user->middle_name
                );
                $smsService->sendSmsMessage($content, $phone);

                $total_price = 0;

                foreach ($additionals as $key => $additional) {
                    $price = $this->formValidPrice($additional->price);

                    $payments[$key]['price'] = $price;
                    $payments[$key]['description'] = $additional->title;

                    $total_price += $price;
                }
            } else {
                $total_price = $this->formValidPrice($payment->sum);

                $payments[0]['price'] = $total_price;
                $payments[0]['description'] = 'Жку';
            }

            return $cashbox_service->sendCashVoucher($payments, $total_price, $user->email);
        } 
    }

    // метод для тестов разной хуйни
    public function cashbox(IPaymentService $payment_service, IPaymentModel $payment_model, ICashbox $cashbox_service, Additional $additional, Users $users, ISms $smsService)
    {
        $user = $users->getByInvoiceNum(113);

        $phone = config('contacts.phones.administrator');

        $content = urlencode(
            'Заказана новая доп.услуга.' . PHP_EOL .
            'Заказ №' . '548' . PHP_EOL .
            'Закзачик: ' . $user->last_name . ' ' . $user->first_name . ' ' . $user->middle_name
        );

        $smsService->sendSmsMessage($content, $phone);

        return 1;
    }
}