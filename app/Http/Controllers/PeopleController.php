<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
// use App\Helpers\Helper;
use App\Models\People;
use DB;

class PeopleController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'middle_name' => 'string|max:255',
            'birthday' => 'required|string',
            'close_date' => 'required',
        ]);

        return $validator;
    }

    public function store(People $peopleModel, Request $request)
    {
        // $currentQuantity = $peopleModel->where([
        //     ['alive', '=', 1],
        //     ['user_id', '=', Auth::user()->id],
        //     ['personal', '=', 0]
        // ])->get()->count();

        // if (Helper::isLimitExceeded($currentQuantity)) {
        //     return response()->json([
        //         'message' => 'Bad request',
        //         'overview' => 'Вы привысили лимит записей (максимальное кол-во 5)'
        //     ], 400);
        // }

        Input::merge([
            'user_id' => Auth::user()->id,
            'last_name' => mb_convert_case($request->last_name, MB_CASE_TITLE, 'UTF-8'),
            'first_name' => mb_convert_case($request->first_name, MB_CASE_TITLE, 'UTF-8'),
            'middle_name' => mb_convert_case($request->middle_name, MB_CASE_TITLE, 'UTF-8'),
        ]);
        
        $error = $this->validator($request->all())->validate();

        if ($error){
            return $error;
        }

        $result = $peopleModel->create($request->all());

        $str = '<tr class="table-item" id="'.$result['id'].'">
                    <td>
                        <div>
                            <button class="button button-transparent button-icon" action="peoples" data-mode="delete">
                            <img src="/img/icon/admin/street/delete.svg"></button>
                        </div>
                  </td>
                  <td>
                    <div>'.$result['last_name'].'</div>
                    <div>'.$result['first_name'].' '.$result['middle_name'].'</div>
                  </td>';

        if (!$result->personal) {
            $str .= '<td>'.$result['close_date'].'<span class="color-green"> ('.$result['close_count'].'д.)</span></td>';
        }

        $str .= '</tr>';

        return $str;
    }

    public function edit(People $peopleModel, Request $request)
    {
        $people = $peopleModel->getGuestById($request->id);
        return json_encode($people);
    }

    public function update(People $peopleModel, Request $request)
    {
        Input::merge([
            'user_id' => Auth::user()->id,
            'last_name' => mb_convert_case($request->last_name, MB_CASE_TITLE, 'UTF-8'),
            'first_name' => mb_convert_case($request->first_name, MB_CASE_TITLE, 'UTF-8'),
        ]);

        $error = $this->validator($request->all())->validate();

        if ($error){
            return $error;
        }

        return $peopleModel->where('id', '=', $request->id)->update($request->except(['_method', '_token']));
    }

    public function remove(People $peopleModel, Request $request)
    {
        return $peopleModel->where('id', '=', $request->id)->update(
            ['alive' => false]
        );
    }
}
