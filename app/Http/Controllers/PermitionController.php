<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Car;
use App\Models\People;
use App\Models\Profile;

class PermitionController extends Controller
{
    public function index()
    {
        $userRole = Auth::user()->role;
        $userId = Auth::user()->id;

        if ($userRole < 3) {
            $peoples = People::getAllUnPersonal();
            $cars = Car::getAllUnPersonal();
        } elseif ($userRole >= 3){
            $cars = Car::getAllByUser($userId);
            $peoples = People::getAllByUser($userId);
        }
        
        return view('permition.index', [
            'peoples' => $peoples,
            'cars' => $cars
        ]);
    }

    public function list()
    {
        $peoples = People::getActive();
        $cars = Car::getActive();
        
        $lastIds = collect([
            'people' => $peoples->first()->id,
            'car' => Car::orderBy('id', 'desc')->first()->id
        ]);

        return view('permition.list', [
            'peoples' => $peoples,
            'cars' => $cars,
            'lastIds' => $lastIds
        ]);
    }

    public function check(Request $request)
    {
        if ($this->existsNewPermitions($request)) {        
            return response()->json([
                'newPermitions' => true  
            ]);
        }

        return response()->json([
            'newPermitions' => false
        ]);
    }

    public function existsNewPermitions(Request $request)
    {
        $lastPeople = People::orderBy('id', 'desc')->first(); 
        $lastCar = Car::orderBy('id', 'desc')->first();

        $peopleId = (int) $request->people;
        $carId = (int) $request->car;

        if ($peopleId < $lastPeople->id || $carId < $lastCar->id) {
            return true;
        }

        return false;
    }
}
