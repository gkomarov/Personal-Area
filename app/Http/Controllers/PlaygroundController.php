<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Playground;

class PlaygroundController extends Controller
{
    //
    public function index(){
        return view("playground.index");
    }

    public function event(Playground $playgroundModel){
        return $playgroundModel->all();
}

    public function store(Request $request, Playground $playgroundModel){

        $request['user_id'] = Auth::id();
        $end = date('Y-m-d H:i',strtotime('+3 hours',strtotime($request->start)));
        $request['end'] = $end;
        return $playgroundModel->create($request->all());
    }
}