<?php

namespace App\Http\Controllers;

use App\Models\Poll;
use App\Models\PollsAnswer;
use App\Models\PollsName;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{

    //
    public function index(PollsName $nameModel){
        $polls = $nameModel->getAll();

        return view('polls.index',['polls'=>$polls]);
    }

    public function store(Poll $pollModel,PollsAnswer $answerModel, PollsName $nameModel, Request $request){

        $poll_id = $nameModel->create(['title'=>$request->title]);
        if ($poll_id->id){
            foreach ($request->answer as $answer){
                $answerModel->create(['poll_id'=>$poll_id->id, 'answer'=>$answer]);
            }
            return redirect()->route('polls.index');
        }
        else {
            abort(404);
        };
    }

    public function insert(Poll $pollModel, Request $request){
        $polls = $pollModel->create(['poll_id'=>$request->poll_id, 'answer_id'=>$request->answer_id,'user_id'=>Auth::id()]);
        return $polls;
    }
}
