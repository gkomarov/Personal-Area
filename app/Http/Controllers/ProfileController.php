<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use App\Models\Profile;
use App\Models\Car;
use App\Models\People;
use App\Models\Users;
use App\Helpers\Helper;
use Session;

class ProfileController extends Controller
{
    public function index(Users $userModel)
    {
        $userId = Auth::user()->id;
        $userModel->DeleteTrashPhonesByUser($userId);
        $about = $userModel->getUser($userId);
        $about->phones = explode(', ', $about->phones);
        $peoples = People::getAllPersonal($userId);
        $cars = Car::getAllPersonal($userId);

        return view('profile.index', [
            'peoples' => $peoples,
            'cars' => $cars, 
            'about' => $about
        ]);
    }
}

