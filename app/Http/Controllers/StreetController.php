<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Models\Street;
use App\Models\Building;
use Illuminate\Http\Request;

class StreetController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'street_name' => 'required|string'
        ]);

        return $validator;
    }

    public function store(Street $streetModel, Request $request)
    {
        $error = $this->validator($request->all())->validate();

        if ($error) {
            return $error;
        };
        return $streetModel->create([
          'name' => $request->street_name
        ]);
    }
}
