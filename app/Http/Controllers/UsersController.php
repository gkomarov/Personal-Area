<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\OneC\IOneC;
use App\Models\Balance\IBalance;
use App\Services\Sms\ISms;
use App\Models\Street;
use App\Models\Users;
use App\Models\Building;
use DB;

class UsersController extends Controller
{
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'invoice_num' => 'integer',
        ]);

        return $validator;
    }

    protected function password_validator(array $data)
    {
        $validator = Validator::make($data, [
            'old_password' => 'required|string',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        return $validator;
    }

    public function index(Users $allUsers, Request $request)
    {
        if (!$request['street']) {
            $request['street'] = 1;
        }
        
        $users = $allUsers->getByStreet($request['street']);
        $streets = Street::all();
        $active_street = $request['street'];
        $buildings = Building::getFreeBuildings();

        return view('users.index', [
            'users' => $users,
            'streets' => $streets,
            'buildings' => $buildings,
            'active_street' => $active_street
        ]);
    }

    public function edit(Request $request, Users $singleUser)
    {
        $single = $singleUser->getUserForEdit($request->id);
        return json_encode($single);
    }

    public function changePassword(Request $request)
    {
        if (password_verify($request['old_password'], Auth::user()->password)) {
            $this->password_validator($request->all())->validate();
            Auth::user()->password = Hash::make($request['new_password']);
            Auth::user()->save();
            event(new PasswordReset(Auth::user()));
            $result = 'Пароль изменён!';
            return $result;
        } else {
            //Для того чтобы стандартные и сампоисные ошибки выводились одинаково, привожу к единому виду объект
            $errors = ['old_password' => [0 => 'Старый пароль введён не верно']];
            return response(json_encode(['errors' => $errors]), 400);
        }
    }

    public function handlerConfirmationPhone(Request $request, Users $userModel, ISms $smsService)
    {
        $confirmationCode = rand(1000, 9000);

        $confirmedPhones = $userModel->getConfirmedPhones();

        if ($this->isPhoneConfirmation($request->phone, $confirmedPhones)) {
            return response()->json([
                'message' => 'Bad Request',
                'overview' => 'Данный телефон уже подтверждён!'
            ], 400);
        } else {
            $content = urlencode("Код подтверждения: " . $confirmationCode);

            $userModel->createPhoneForConfirm($request, $confirmationCode);
            $smsService->sendSmsMessage($content, $request->phone);
        }
    }

    public function isPhoneConfirmation($currentPhone, $confirmedPhones)
    {   
        $phone = $confirmedPhones->whereIn('phone', $currentPhone);

        if ($phone->isNotEmpty()) {
            return true;
        }

        return false;
    }

    public function confirmPhone(Request $request, Users $usersModel)
    {
        $user = $usersModel->getUserPhoneCode(Auth::user()->id);

        if ((int) $request->confirmation_code == $user->confirmation_code) {
            $status = 1;
            return $usersModel->changeStatusPhone($request, $status);
        } else {
            return response()->json([
                'message' => 'Bad request',
                'overview' => 'Не правильный код подтверждения'
            ], 400);
        }
    }

    public function handlerConfirmationEmail(Request $request)
    {
        $url = route('users.email-confirm', [
            'token' => $request->token,
            'email' => $request->email
        ]);

        Mail::send('emails.confirm_email', array('url' => $url), function ($message) use ($request) {
            $message->to($request->email, $request->name)->subject('Подтверждение e-mail. Личный кабинет "Донской"');
        });
    }

    public function confirmEmail(Request $request, Users $usersModel)
    {
        $usersModel->where('id', '=', Auth::id())->update(['email' => $request->email]);
        return redirect()->route('profiles.index');
    }

    public function street(Users $userModel)
    {
        $streets = Street::all();

        $users = $userModel->getByStreet($_GET['street']);

        return view('users.list', [
            'streets' => $streets, 
            'users' => $users, 
            'street' => $_GET['street']
        ]);
    }

    public function update(Users $updateUser, Request $request)
    {
        $error = $this->validator($request->all())->validate();

        if ($error) {
            return $error;
        }

        $result = $updateUser->where('id', '=', $request->id)->update($request->except(['_token', '_method', 'street_id']));

        return $result;
    }

    public function remove(Users $removeUser)
    {
        return $removeUser->where('id','=',$_POST['id'])->update(['alive'=>false]);
    }

    /**
     * Unbind user the apartment
     */
    public function unbind(Request $request)
    {
        DB::beginTransaction();

        try {
            Users::where('id', $request->id)->update(['building_id' => NULL]);
            Building::where('user_id', $request->id)->update(['user_id' => NULL]);
        } catch (Exception $e) {
            DB::rollback();       
        }

        DB::commit();
        return 1;
    }

    public function getBalance(IBalance $balance, IOneC $one_c_service)
    {
        $balance = $balance->show(Auth::user()->invoice_num, $one_c_service);
        return json_encode($balance);
    }
}
