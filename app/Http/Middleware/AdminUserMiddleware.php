<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {        
            if (Auth::user()->role != 1 && Auth::user()->role != 3 && Auth::user()->role != 4) {
                return redirect(route('home'));
            }

            return $next($request);
        } else {
            return redirect(route('login'));   
        }
    }
}