<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CommonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {        
            if (Auth::user()->role == 1) {
                return redirect('buildings');
            } elseif (Auth::user()->role == 5) {
                return redirect(route('permitions_list'));
            }

            return $next($request);
        } else {
            return redirect(route('login'));   
        }
    }
}