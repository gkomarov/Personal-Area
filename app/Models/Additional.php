<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Additional extends Model
{
    protected $fillable = [
        'additional_id', 'comment', 'status', 'user_id'
    ];

    public function getAll()
    {
        return $this->additionals()
                    ->whereBetween('a.status', [1, 4])
                    ->orderBy('a.payment_id', 'desc')
                    ->get();
    }

    public function getDone()
    {
        return DB::select(DB::raw("SELECT DISTINCT a.payment_id FROM additionals AS a WHERE (SELECT DISTINCT payment_id FROM additionals WHERE status != 3 AND status != 4 AND payment_id = a.payment_id) IS NULL"));
    }

    public function getByDays()
    {
        return $this->additionals()
                    ->where('a.status', '!=', 0)
                    ->where(DB::raw('DATEDIFF(a.updated_at, NOW())'), '>', -4)
                    ->orderBy('a.id','desc')
                    ->get();
    }

    public static function getAllByUser($user_id)
    {
        return Additional::additionals()
                        ->where('a.user_id', '=', $user_id)
                        ->orderBy('a.payment_id', 'desc')
                        ->get();
    }

    public static function getStory($user_id)
    {
        return Additional::additionals()
                        ->where('a.user_id', '=', $user_id)
                        ->orderBy('id', 'desc')
                        ->paginate(20);
    }


    public static function getStoryByFilter($user_id, $request)
    {
        return Additional::additionals()
                        ->where('a.user_id', '=', $user_id)
                        ->firstdate($request['first_date'])
                        ->lastdate($request['last_date'])
                        ->additionalfilter($request['additional_id'])
                        ->orderBy('a.updated_at','desc')
                        ->paginate(20);
    }

    public function getSingleByUser()
    {
        return $this->additionals()
                    ->where('a.user_id', '=', Auth::id())
                    ->where('a.id', '=', $this->select()->max('id'))
                    ->orderBy('a.updated_at', 'desc')
                    ->get();
    }

    public function scopeAdditionals($query)
    {
        $query->select(
            'a.id', 'a.additional_id', 'a.payment_id', 'l.title', 'a.status', 'a.comment', 'l.price',
            DB::raw('DATE_FORMAT(a.updated_at, "%d.%m.%Y") AS updated_date'),
            'u.last_name', 'u.first_name', 'u.middle_name'
        )->tables();
    }

    public function scopeTables($query)
    {
        $query->from('additionals as a')
                ->leftJoin('additional_lists as l', 'l.id', '=', 'a.additional_id')
                ->leftJoin('users as u', 'u.id', '=', 'a.user_id');
    }

    public function scopeAdditionalFilter($query, $additional_id)
    {
        if ($additional_id) {
            $query->where('a.additional_id', '=', $additional_id);
        }
    }

    public function scopeFirstDate($query, $firstDate)
    {
        if ($firstDate) {
            $query->where('a.updated_at', '>=', $firstDate);
        }
    }

    public function scopeLastDate($query, $lastDate)
    {
        if ($lastDate) {
            $query->where('a.updated_at', '<=', $lastDate);
        }
    }

    /**
     * Order additional services
     *
     * @param $request - data request
     * @return true or false
     */
    public function order($request)
    {
        return DB::table('additionals')->insert($request);
    }

    /**
     * Gets total price additionals by id
     *
     * @param $ids - additionals ids
     * @return total price
     */
    public function getTotalPrice(array $ids)
    {
        return DB::table('additional_lists')->select('price')->whereIn('id', $ids)->sum('price');
    }

    /**
     * Gets all additional services by payment id
     *
     * @param $payment_id - payment id
     * @return return collection data or false
     */
    public function getAllByPayment(int $payment_id)
    {
        return DB::table('additionals')
                ->join('additional_lists', 'additionals.additional_id', '=', 'additional_lists.id')
                ->select('additional_lists.title', 'additional_lists.price', 'additionals.status')
                ->where('payment_id', '=', $payment_id)
                ->get();
    }

    /**
     * Updates additional services by payment id
     *
     * @param $payment_id - payment id
     * @return true or false
     */
    public function updateByPayment(int $payment_id)
    {
        return DB::table('additionals')->where('payment_id', '=', $payment_id)->update(['status' => 1]);
    }

    /**
     * Gets unpaid additional services
     *
     * @param $user_id - user id who want pay unpaid services
     * @return collection unpaid additional servicec or null
     */
    public function getUnpaid(int $user_id)
    {
        return DB::table('additionals')->where([
            ['user_id', '=', $user_id],
            ['status', '=', 0]
        ])->first();
    }

    public function changeStatus(string $field_name, $value, int $status)
    {
        return DB::table('additionals')->where($field_name, $value)->update(['status' => $status]);
    }
}
