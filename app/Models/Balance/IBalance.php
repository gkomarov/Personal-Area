<?php

namespace App\Models\Balance;

use App\Services\OneC\IOneC;

interface IBalance 
{
    public function show(string $invoice_num);
}