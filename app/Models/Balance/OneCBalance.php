<?php

namespace App\Models\Balance;

use App\Models\Balance\IBalance;
use App\Services\OneC\IOneC;

class OneCBalance implements IBalance
{
    private $one_c;

    public function __construct(IOneC $one_c)
    {
        $this->one_c = $one_c;
    }

    /**
     * Shows balance by client id
     *
     * @param $invoice_num - user invoice_num
     * @return array data balance
     */
    public function show(string $invoice_num)
    {
        return $this->one_c->getClientBalance($invoice_num);
    }
}