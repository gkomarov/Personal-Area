<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = [
        'type','building_number','floor','area','apart_type','apart_number','space_area','street_id','user_id',
    ];

    public static function getAllBuildings()
    {
        $buildings = Building::select('b.id','b.building_number','b.apart_number','b.type','b.apart_type','b.floor','b.area','b.space_area','u.last_name','u.first_name','u.middle_name','s.name as street_name','u.id as user_id')
            ->buildings()->paginate(15)
            ->get();
        return $buildings;
    }

    public static function getFreeBuildings()
    {
        $buildings = Building::select('b.id','b.building_number','b.apart_number','b.type','b.apart_type','b.floor','b.area','b.space_area','u.last_name','u.first_name','u.middle_name','s.name as street_name','u.id as user_id')
            ->buildings()
            ->whereNull('b.user_id')
            ->get();
        return $buildings;
    }

    public static function getBuildingsByStreet($street)
    {
        $all_by_street = Building::select('b.id','b.building_number','b.apart_number','b.type','b.apart_type','b.floor','b.area','b.space_area','u.last_name','u.first_name','u.middle_name','s.name as street_name','u.id as user_id')
            ->buildings()
            ->where('b.street_id','=',$street)->paginate(15);
        return $all_by_street;
    }

    public function getBuildingById($id)
    {
        return Building::select('b.id','b.building_number','b.apart_number','b.type','b.apart_type','b.floor','b.area','b.space_area','u.last_name','u.first_name','u.middle_name','s.name as street_name','u.id as user_id')
            ->buildings()
            ->where('b.id','=',$id)
            ->first();
    }

    public function scopeBuildings($query)
    {
        $query->from('buildings as b')
            ->leftJoin('users as u', 'u.id', '=', 'b.user_id')
            ->leftJoin('streets as s', 's.id', '=', 'b.street_id')
            ->where('b.alive', '=', true);
    }
}
