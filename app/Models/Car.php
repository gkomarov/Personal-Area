<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Car extends Model
{
    protected $fillable = [
        'number',
        'number_mask',
        'mark',
        'model',
        'note',
        'close_date', 
        'user_id', 
        'personal'
    ];

    public static function getAll()
    {
        return Car::cars()->orderBy('c.id', 'desc')->get();
    }

    public static function getActive()
    {
        return Car::cars()
           ->whereColumn('c.close_date', '>=', DB::raw('CURDATE()'))
           ->orderBy('c.id', 'desc')
           ->get();
    }

    public static function getExpired()
    {
        return DB::table('cars')
            ->select('number')
            ->whereColumn('close_date', '<', DB::raw('CURDATE()'))
            ->where('personal', '=', 0)
            ->where('alive', '=', 1)
            ->get();
    }

    public static function getByDays()
    {
        return Car::cars()
            ->where(DB::raw('DATEDIFF(c.updated_at, NOW())'), '>', -3)
            ->where('c.updated_at', '<', Carbon::now())
            ->get();
    }

    public static function getAllByUser($user_id)
    {
        return Car::cars()
            ->where('c.user_id','=', $user_id)
            ->where('c.personal', '=', false)
            ->get();
    }

    public static function getAllPersonal($user_id)
    {
        return Car::cars()
            ->where('c.user_id', '=', $user_id)
            ->where('c.personal', '=', true)
            ->where('c.alive', '=', true)
            ->get();
    }

    public static function getAllUnPersonal()
    {
        return Car::cars()->where('personal', '=', 0)->get();
    }

    public function getCarById($id)
    {
        return Car::cars()
            ->where('c.id', '=', $id)
            ->first();
    }

    public function scopeCars($query)
    {
        $query->select('c.id', 'c.number', 'c.number_mask', 'c.mark', 'c.model', 'c.note', DB::raw('DATEDIFF(c.close_date, NOW()) as close_count', 'p.number_car as num_car', 'p.model_car as mod_car', 'p.mark_car as mar_car'),

        DB::raw('DATE_FORMAT(c.close_date, "%d.%m.%Y") AS close_date'), 'c.personal')
            ->from('cars as c')
            ->leftJoin('users as u', 'c.user_id', '=', 'u.id')
            ->leftJoin('buildings as b', 'u.id', '=', 'b.user_id')
            ->leftJoin('streets as s', 'b.street_id', '=', 's.id')
            ->where('c.alive', '=', 1)
            ->where(DB::raw('DATEDIFF(c.close_date, NOW())'), '>', -14);
    }
}
