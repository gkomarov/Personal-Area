<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $fillable = [
        'name','man','number',
    ];
    //
    public function getAllContacts() {
        $Contact = Contact::select()
            ->where('alive','=',true)
            ->get();
        return $Contact;
    }

    /**
     * @param $id
     * @return string
     */
    public function getContactById($id) {
        $Contact = Contact::select()
            ->where('id','=',$id)
            ->where('alive','=',true)
            ->get();
        return $Contact;
    }
}
