<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Indication extends Model
{
    protected $fillable = [
        'water','energy','user_id'
    ];

    public function getCurrent()
    {
        return Indication::select()
            ->whereMonth('updated_at', Carbon::now()->month)
            ->get();
    }

    public function getAll()
    {
        return $this->select('user_id')->get();
    }

    public function getByStreet($street)
    {
        return Indication::select('i.id', 'i.user_id', 'i.energy', 'i.water','i.updated_at','b.street_id',
                                          'u.first_name', 'u.last_name', 'u.middle_name', 's.name as street_name',
                                          'b.building_number', 'b.apart_number', DB::raw('DATE_FORMAT(i.updated_at, "%d.%m.%Y") AS updated_date'))
            ->tables()
            ->where('b.street_id', '=', $street)
            ->get();
    }

    public static function getByUser($user_id)
    {
        return Indication::select('i.id', 'i.user_id', 'i.energy', 'i.water',
            DB::raw('DATE_FORMAT(i.updated_at, "%d.%m.%Y") AS updated_date'))
            ->tables()
            ->where('i.user_id', '=', $user_id)
            ->latest('updated_date')
            ->limit(1)
            ->get();
    }

    public function getById($id)
    {
        return Indication::from('indications as i')
            ->select('i.id', 'u.last_name', 'u.first_name', 'u.middle_name', 's.name as street_name', 'b.building_number as building_number', 'b.apart_number', 'i.water', 'i.energy')
            ->leftJoin('users as u', 'u.id', '=', 'i.user_id')
            ->leftJoin('buildings as b', 'b.id', '=', 'u.building_id')
            ->leftJoin('streets as s', 's.id', '=', 'b.street_id')
            ->where('i.id', '=', $id)
            ->first();
    }

    public function getByMonth($street, $month)
    {
        return Indication::select('i.id', 'i.user_id', 'i.energy', 'i.water','i.updated_at')
            ->tables()
            ->where('b.street_id', '=', $street)
            ->whereMonth('i.updated_at', $month)
            ->get();
    }

    public function scopeTables($query)
    {
        $query->from("indications as i")
            ->leftJoin("users as u", "u.id", "=", "i.user_id")
            ->leftJoin("buildings as b", "b.id", "=", "u.building_id")
            ->leftJoin('streets as s', 's.id', '=', 'b.street_id')
            ->where("u.role", ">=", 3);
    }
}


