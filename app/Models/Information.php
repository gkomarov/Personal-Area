<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Information extends Model
{
    //

    protected $fillable=[
      'header','article'
    ];


    public static function getAll(){
        return Information::select('sys_id','header','article')->
                where("sys_id","=",$_GET["sys_id"])->
                get();
    }

}


