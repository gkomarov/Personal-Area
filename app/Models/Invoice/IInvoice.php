<?php

namespace App\Models\Invoice;

use App\Models\Invoice\IInvoice;

interface IInvoice
{
    public function getAllByPeriod(array $period, string $counteragent_key='');
    public function getPreviousMonth(array $period, string $counteragent_key);
}