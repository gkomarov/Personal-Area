<?php

namespace App\Models\Invoice;

use App\Models\Invoice\IInvoice;
use App\Services\OneC\IOneC;
use AlgoWeb\PODataLaravel\Models\MetadataTrait;
use Illuminate\Support\Facades\Auth;
use POData\Common\ODataConstants;
use POData\OperationContext\HTTPRequestMethod;
use POData\OperationContext\IHTTPRequest;
use App\Services\DateTime\IDateTime;

class OneCInvoice implements IInvoice
{
    protected $one_c;

    public function __construct(IOneC $one_c)
    {
        $this->one_c = $one_c;
    }
    /**
     * Shows invoice by num
     *
     * @param $period - data period
     * @return result function getInvoicesByPeriod
     */
    public function getAllByPeriod(array $period, string $counteragent_key='')
    {
        return $this->one_c->getClientInvoicesByPeriod($period, $counteragent_key);
    }

    /**
     * Gets invoice for previous month
     *
     * @param $period - data period
     * @return result function getClientInvoice
     */
    public function getPreviousMonth(array $period, string $counteragent_key)
    {
        return $this->one_c->getClientInvoicePreviousMonth($period, $counteragent_key);
    }
}