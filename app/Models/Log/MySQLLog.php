<?php

namespace App\Models\Log;

use DB;

class MySQLLog implements ILog
{
    public function getAll()
    {
        return DB::table('logs')->whereRaw('created_at > NOW() - INTERVAL 3 DAY')->orderBy('id', 'desc')->get();
    }
}