<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class News extends Model
{
    protected $fillable = [
        'user_id','text_news','header_news',
    ];
    //
    public function getAllNews() {
        $news = News::news()->get();
        return $news;
    }

    public static function getUserNews($user_id) {
        $news = News::news()
            ->where('user_id','=',$user_id)
            ->orWhereNull('user_id')
            ->get();
        return $news;
    }

    /**
     * @param $id
     * @return string
     */
    public function getNewsById($id) {
        $news = News::news()
            ->where('id','=',$id)
            ->get();
        return $news;
    }

    public function scopeNews($query){
        $query ->select('id','text_news','header_news','user_id',DB::raw('DATE_FORMAT(updated_at, "%d.%m.%Y") AS updated_date'))
               ->where('alive','=',true);

    }
}
