<?php

namespace App\Models\Payment;

interface IPayment
{
    public function store(array $data);
    public function remove(string $num);
    public function confirm(array $payments_nums);
    public function show(int $id);
    public function getSuccessPayments();
    public function getByNum(string $num);
    public function updateByNum(array $payment);
}