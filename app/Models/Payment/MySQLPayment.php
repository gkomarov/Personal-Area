<?php

namespace App\Models\Payment;

use App\Models\Payment\IPayment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MySQLPayment implements IPayment
{
    /**
     * Creates additional services payment
     *
     * @param $data - payment data
     * @return false or payment id
     */
    public function store(array $data)
    {
        return DB::table('payments')->insertGetId([
            'user_id' => Auth::id(),
            'num' => uniqid() . Auth::id(),
            'sum' => $data['sum'],
            'status' => 0
        ]);
    }

    /**
     * Confirms all executed payments
     *
     * @param payments_nums - nums payments
     * @return trur or false
     */
    public function confirm(array $payments_nums)
    {
        return DB::table('payments')->whereIn('num', $payments_nums)->update(['status' => 4]);
    }

    /**
     * Removes payment from db
     *
     * @param $id - payment num
     * @return response
     */
    public function remove(string $num)
    {
        return DB::table('payments')->where('num', '=', $num)->delete();
    }

    /**
     * Gets selected payment from db
     *
     * @param $id - payment id
     * @return payment data or false
     */
    public function show(int $id)
    {
        return DB::table('payments')->where('id', '=', $id)->first();
    }

    /**
     * Gets success payments where status = 1
     *
     * @return array payments or null
     */
    public function getSuccessPayments()
    {
        return DB::table('payments')->where('status', '=', 1)->get();
    }

    /**
     * Gets payment by num
     *
     * @param $num - payment num
     * @return data payment
     */
    public function getByNum(string $num)
    {
        return DB::table('payments')->select('id', 'sum')->where('num', '=', $num)->first();
    }

    /**
     * Updates payment by num
     *
     * @param $payment - info about success payment
     * @return true or false
     */
    public function updateByNum(array $payment)
    {
        $payment_num = $payment['payment_num'];

        return DB::table('payments')->where('num', '=', $payment_num)->update([
            'status' => 1
        ]);
    }
}