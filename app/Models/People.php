<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class People extends Model
{
    protected $table = 'peoples';

    protected $fillable = [
        'user_id',
        'last_name', 
        'first_name',
        'middle_name',
        'birthday',
        'kin',
        'note', 
        'close_date', 
        'personal'
    ];
    
    public static function getAll()
    {
        return People::peoples()->orderBy('p.id', 'desc')->get();
    }

    public static function getActive()
    {
        return People::peoples()
            ->where('p.alive', '=', 1)
            ->whereColumn('p.close_date', '>=', DB::raw('CURDATE()'))
            ->orderBy('p.id', 'desc')
            ->get();
    }

    public static function getByDays()
    {
        return People::peoples()
            ->where(DB::raw('DATEDIFF(p.updated_at, NOW())'), '>', -3)
            ->where('p.updated_at', '<=', Carbon::now())
            ->get();
    }

    public static function getAllByUser($user_id)
    {
       return People::peoples()
            ->where('p.user_id', '=', $user_id)
            ->where('p.personal', '=', false)
            ->where('p.personal', '=', 0)
            ->get();
    }

    public static function getAllPersonal($user_id)
    {
        return People::select()
            ->where('user_id','=', $user_id)
            ->where('personal', '=', true)
            ->where('alive', '=', true)
            ->get();
    }

    public static function getAllUnPersonal()
    {
        return People::peoples()->where('personal', '=', 0)->get();
    }

    public function getGuestById($id)
    {
        return People::peoples()
            ->where('p.id', '=', $id)
            ->first();
    }

    public static function last()
    {
        return People::orderBy('id', 'desc')->first();
    }

    public static function getActiveWithCar()
    {
        return People::select('p.id', 's.name as street_name', 'b.building_number')
            ->from('peoples as p')
            ->leftJoin('buildings as b', 'b.user_id', '=', 'p.user_id')
            ->leftJoin('streets as s', 's.id', '=', 'b.street_id')
            ->whereColumn('close_date', '>=', DB::raw('NOW()'))
            ->whereNotNull('number_car')
            ->where('p.number_car', '!=', '')
            ->where('p.alive', '=', 1)
            ->orderBy('p.id', 'desc')
            ->get();
    }

    public function scopePeoples($query)
    {
        $query->select('p.id', 'p.last_name', 'p.first_name', 'p.middle_name', 'p.note', DB::raw('DATEDIFF(p.close_date, NOW()) as close_count', 'p.number_mask'),

        DB::raw('DATE_FORMAT(p.close_date, "%d.%m.%Y") AS close_date'), 's.name as street_name', 'b.building_number', 'u.last_name as user_last_name', 'u.first_name as user_first_name', 'u.middle_name as user_middle_name','p.personal')
        ->from('peoples as p')
        ->leftJoin('users as u', 'p.user_id', '=', 'u.id')
        ->leftJoin('buildings as b', 'u.id', '=', 'b.user_id')
        ->leftJoin('streets as s', 'b.street_id', '=', 's.id')
        ->where('p.alive', '=', 1)
        ->where(DB::raw('DATEDIFF(p.close_date, NOW())'), '>', -14);
    }
}
