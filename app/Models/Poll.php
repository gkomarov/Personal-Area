<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Poll extends Model
{
    protected $fillable=[
        'poll_id','answer_id','user_id'
    ];
    //
    public function getMaxId(){
        $max = Poll::max('id')
            ->from('polls')
            ->limit('1')
            ->get();
        return $max;
    }

    public function getAnswers($poll){
        $res = Poll::select()
            -> from('poll_answers')
            -> where('pool_id', '=', '$poll')
            -> orderBy('id')
            -> get();
        return $res;

    }

    public static function getByUser(){

        $polls = DB::select("SELECT n.id,n.title,n.text 
                              FROM polls_names n 
                              WHERE n.alive=true 
                              AND NOT EXISTS( SELECT 1 
                                                    FROM polls 
                                                    WHERE n.id = poll_id 
                                                    AND user_id = ".Auth::id().")
                              ORDER BY n.id DESC
                              LIMIT 1");
        foreach ($polls as $poll) {
            $poll->answer = PollsAnswer::from('polls_answers')
                ->where('poll_id','=', $poll->id)
                ->pluck('answer','id');
        }
        if ($polls) {
            $polls = $polls[0];
        }
        return $polls;
    }
}
