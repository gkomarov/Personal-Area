<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PollsAnswer extends Model
{
    protected $fillable=[
        'answer','poll_id'
    ];
    //
}
