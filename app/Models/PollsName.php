<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Object_;

class PollsName extends Model
{
    protected $fillable=[
        'title','text','alive'
    ];
    //
    public function getAll(){
        $polls = PollsAnswer::select('id','title','text')
            ->from('polls_names')
            ->where('alive','=',true)
            ->get();
        foreach ($polls as $poll) {
            $poll->answer = PollsAnswer::from('polls_answers')
                        ->where('poll_id','=', $poll->id)
                        ->pluck('answer','id');
        }
        return $polls;
    }
}
