<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Profile extends Model
{
    protected $fillable = [
        'last_name', 
        'first_name', 
        'kin', 
        'birthday',
        'user_id',
        'number_car',
        'number_mask',
        'mark_car',
        'model_car'
    ];

    public static function getAllByUser()
    {
        return Profile::select('id', 'last_name', 'first_name', 'kin', 'number_car', 'number_mask', 'mark_car', 'model_car',
            DB::raw('DATE_FORMAT(birthday, "%d.%m.%Y") AS birthday'))
            ->where('user_id', '=', Auth::id())
            ->where('alive', '=', true)
            ->get();
    }

    public static function getAllWithCar()
    {
        return Profile::select('p.id', 'p.number_car as number', 'p.mark_car as mark', 'p.model_car as model', 's.name as street_name', 'b.building_number', 'p.number_mask')
                      ->from('profiles as p')
                      ->leftJoin('buildings as b', 'b.user_id', '=', 'p.user_id')
                      ->leftJoin('streets as s', 's.id', '=', 'b.street_id')
                      ->whereNotNull('number_car')
                      ->where('p.alive', '=', 1)
                      ->where('p.number_car', '!=', '')
                      ->orderBy('p.id', 'desc')
                      ->get();
    }
}
