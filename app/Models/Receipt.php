<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $fillable = [
        'title',
        'user_id',
        'sent',
    ];

    public function getAll()
    {
        return Receipt::select('r.title','r.user_id','r.sent','r.created_at','r.updated_at')
            ->from('receipts as r')
            ->leftJoin('users as u', 'r.user_id', '=', 'u.id')
            ->get();
    }

/*    public function setNullSentForAll()
    {
        return Receipt::
    }*/

}
