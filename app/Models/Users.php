<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Users extends Model
{
    protected $fillable = [
        'building_id',
        'password',
    ];

    public function createPhoneForConfirm($request, int $confirmationCode)
    {
        return DB::table('user_phones')->insert([
            'user_id' => Auth::user()->id,
            'phone' => $request->phone,
            'status' => 0,
            'confirmation_code' => $confirmationCode
        ]);
    }

    public function getConfirmedPhones()
    {
        return DB::table('user_phones')->select('phone')->where([
            ['user_id', '=', Auth::user()->id],
            ['status', '=', '1']
        ])->get();
    }

    public function DeleteTrashPhonesByUser($user_id){
        return DB::table('user_phones')->where([
            ['user_id','=',$user_id], ['status','=','0']
        ])->delete();
    }

    public function changeStatusPhone($request, int $status)
    {
        return DB::table('user_phones')->where('confirmation_code', '=', $request->confirmation_code)->update([
            'status' => $status,
            'confirmation_code' => NULL
        ]);
    }

    public function getAllUsers()
    {
        $users = $this->select('u.id', 'u.last_name', 'u.first_name', 'u.middle_name', 'u.invoice_num', 's.name as street_name', 'b.building_number', 'u.email', 'u_p.phone')
            ->users()->paginate(10);

        return $users;
    }

    public function getAllTest()
    {
        //$users = $this->select(DB::raw("u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, u.counteragent_key, s.name as street_name, b.id as building_id, b.building_number, b.apart_number, u.email, GROUP_CONCAT(DISTINCT u_p.phone SEPARATOR ', ') as phones"))
        $users = $this->select(DB::raw('u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, s.name as street_name, u.counteragent_key, b.building_number, b.id as building_id, b.apart_number, u.email, GROUP_CONCAT(DISTINCT u_p.phone SEPARATOR \', \') as phones'))
            ->users()
            //->where('u.email', 'like', 'grigorii.komarov@in-pk.com' )
            ->where('u.name', 'like', 'test%' )
            ->groupBy('u.id')
            ->get();

        return $users;
    }

    public function getAllLodgers()
    {
        $users = $this->select(DB::raw('u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, s.name as street_name, u.counteragent_key, b.building_number, b.id as building_id, b.apart_number, u.email, GROUP_CONCAT(DISTINCT u_p.phone SEPARATOR \', \') as phones'))
            ->users()
            ->where('u.role', '=', '3')
            ->whereNotNull('u.counteragent_key')
            ->whereNotNull('b.building_number')
            ->groupBy('u.id')
            ->get();
        return $users;
    }

    public function getUser($id)
    {
        $user = $this->select(DB::raw("u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, s.name as street_name, b.id as building_id, b.building_number, b.apart_number, u.email, GROUP_CONCAT(DISTINCT u_p.phone SEPARATOR ', ') as phones"))
          ->users()
          ->where([
                ['u.id', '=', $id],
            ])
          ->first();

        return $user;
    }

    public function getUserForEdit($id)
    {
        $user = $this->select(DB::raw("u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, s.name as street_name, b.id as building_id, b.building_number, b.apart_number"))
            ->users()
            ->where('u.id', '=', $id)
            ->first();

        return $user;
    }

    public function getUserPhoneCode(int $userId)
    {
        return $this->from('user_phones')->select('confirmation_code')->where('user_id', '=', $userId)->orderBy('id', 'desc')->first();
    }

    public function getByStreet($street)
    {
        return $this->select(DB::raw("u.role, u.id, u.name, u.last_name, u.first_name, u.middle_name, u.invoice_num, s.name as street_name, b.building_number, b.apart_number, u.email, u_p.phone, GROUP_CONCAT(DISTINCT u_p.phone SEPARATOR ', ') as phones"))
            ->users()
            ->groupBy('u.id')
            ->where('s.id', '=', $street)->paginate(10);
    }

    public function scopeUsers($query)
    {
        $query->from('users as u')
            ->leftJoin('buildings as b', 'u.id', '=', 'b.user_id')
            ->leftJoin('streets as s','s.id', '=', 'b.street_id')
            ->leftJoin('user_phones as u_p', 'u.id', '=', 'u_p.user_id')
            ->where('u.alive', '=', true)
            ->where('u.role', '>=', '3');
    }

    public function getByInvoiceNum(int $invoice_num)
    {
        return $this->select('email', 'last_name', 'first_name', 'middle_name')->where('invoice_num', '=', $invoice_num)->first();
    }

    public function getUsersWithoutIndications()
    {
        return DB::select('SELECT u.id, u.first_name, u.last_name, u.middle_name FROM users as u WHERE u.id NOT IN (SELECT i.user_id FROM indications as i) AND u.role != 1 AND u.role != 5');
    }
}
