<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CashboxServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Services\Cashbox\ICashbox',
            'App\Services\Cashbox\ChekonlineCashbox'
        );
    }
}