<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class DateTimeServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\DateTime\IDateTime',
            'App\Services\DateTime\YBDateTime'
        );
    }
}