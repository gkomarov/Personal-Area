<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FileServiceProvider extends ServiceProvider
{
    /**
     * Registers binds in service container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\File\IFile',
            'App\Services\File\YBFile'
        );
    }
}