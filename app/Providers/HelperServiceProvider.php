<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        require_once app_path() . '/Helpers/Helper.php';
    }
}