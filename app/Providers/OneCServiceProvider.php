<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OneCServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\OneC\IOneC',
            'App\Services\OneC\OneC'
        );
    }
}