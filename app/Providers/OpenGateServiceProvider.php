<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class OpenGateServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\OpenGate\IOpenGate',
            'App\Services\OpenGate\AutoMarshallOpenGate'
        );
    }
}