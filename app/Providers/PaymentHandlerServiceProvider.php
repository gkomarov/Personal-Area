<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PaymentHandlerServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\PaymentHandler\IPaymentHandler',
            'App\Services\PaymentHandler\PaymentHandler'
        );
    }
}