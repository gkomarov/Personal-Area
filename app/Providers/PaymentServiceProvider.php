<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
	/**
	 * Registers relationship interface - relization
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'App\Services\Payment\IPayment',
			'App\Services\Payment\PsbankPayment'
		);
	}
}