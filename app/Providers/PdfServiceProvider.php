<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PdfServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\Pdf\IPdf',
            'App\Services\Pdf\MPdf'
        );
    }
}