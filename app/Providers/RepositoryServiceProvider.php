<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Registers binds in service container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Models\Invoice\IInvoice',
            'App\Models\Invoice\OneCInvoice'
        );
        
        $this->app->bind(
            'App\Models\Payment\IPayment',
            'App\Models\Payment\MySQLPayment'
        );

        $this->app->bind(
            'App\Models\Balance\IBalance',
            'App\Models\Balance\OneCBalance'
        );

        $this->app->bind(
            'App\Models\Log\ILog',
            'App\Models\Log\MySQLLog'
        );
    }
}