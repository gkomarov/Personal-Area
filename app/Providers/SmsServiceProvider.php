<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SmsServiceProvider extends ServiceProvider
{
    /**
     * Registers relationship interface - relization
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Services\Sms\ISms',
            'App\Services\Sms\MobileSms'
        );
    }
}