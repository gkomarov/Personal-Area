<?php

namespace App\Services\Cashbox;

use App\Services\Cashbox\ICashbox;
use Auth;

class ChekonlineCashbox implements ICashbox
{
    /**
     * Sends cash voucher on email user
     * 
     * @param $payments - data current payments
     * @param $total_price - total price for all
     * @return status code and json
     */
    public function sendCashVoucher(array $payments, int $total_price, string $user_email)
    {
        // тестовый url
        // $url = "https://fce.starrys.ru:4443/fr/api/v2/Complex";

        $url = "https://kkt.chekonline.ru/fr/api/v2/Complex";
        
        $data = array(
            "Device" => "auto",
            'RequestId' => uniqid(),
            "Lines" => array(),
            // Qty * Price + Qty * Price... (за 2 товара 20000)
            "NonCash" => array($total_price),
            "TaxMode" => 0,
            "PhoneOrEmail" => $user_email,
        );

        foreach ($payments as $payment) {
            $data['Lines'][] = array(
                "Qty" => 1000,
                "Price" => $payment['price'],
                "PayAttribute" => 4,
                "TaxId" => 1,
                "Description"=> $payment['description'],
            );
        }
        
        $json_data = json_encode($data);

        $curl = curl_init();

        $cert = public_path() . '/ssl/cert.crt';
        $key = public_path() . '/ssl/cert.key';

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSLCERT, $cert);
        curl_setopt($curl, CURLOPT_SSLKEY, $key);

        $json_response = curl_exec($curl);

        // print "json_response: $json_response\n";

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $lastError = curl_error($curl);

        // print "lastError :$lastError\n";
        // print "Status :$status\n";

        curl_close($curl);
        return $status;
    }
}