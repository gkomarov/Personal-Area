<?php

namespace App\Services\Cashbox;

interface ICashbox
{
    public function sendCashVoucher(array $payments, int $total_price, string $user_email);
}