<?php

namespace App\Services\DateTime;

interface IDateTime
{
    public function convertPeriod(string $period);
    public function getTimestamp(string $period);
    public function formEndPeriod(string $date='', string $modify='');
    public function formStartPeriod(string $date='', string $modify='');
}