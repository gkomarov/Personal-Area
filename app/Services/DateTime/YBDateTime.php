<?php

namespace App\Services\DateTime;

use DateTime;

class YBDateTime implements IDateTime
{
    public function convertPeriod(string $period)
    {
        $date = new DateTime();
        $date->setTimestamp($period);
        
        return $date->format('Y-m-d');
    }

    public function getTimestamp(string $period)
    {
        $date = new DateTime($period);

        return $date->getTimestamp();
    }

    public function formEndPeriod(string $date='', string $modify='')
    {
        $date = new DateTime($date);

        if ($modify) {
            $date->modify($modify);
            $date->modify('last day of');
        }
        
        $period['year'] = $date->format('Y');
        $period['month'] = $date->format('m');
        $period['day'] = $date->format('d');
        $period['timestamp'] = $date->getTimestamp();

        return $period;
    }

    public function formStartPeriod(string $date='', string $modify='')
    {
        $date = new DateTime($date);

        if ($modify) {
            $date->modify($modify);
        }

        $period['year'] = $date->format('Y');
        $period['month'] = $date->format('m');
        $period['day'] = '01';
        $period['timestamp'] = $date->getTimestamp();

        return $period;
    }
}