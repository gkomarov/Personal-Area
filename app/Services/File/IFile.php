<?php

namespace App\Services\File;

interface IFile
{
	public function parseCsv(string $file_name);
}