<?php

namespace App\Services\File;

use App\Services\File\IFile;

class YBFile implements IFile
{
	/**
	 * Parses all files in format csv
	 *
	 * @param $file_name - file name
	 * @return data from file
	 */
	public function parseCsv(string $file_name)
	{
		$data = [];

        $path_to_file = public_path() . '/file/';
        $file_directory = scandir($path_to_file);

        // if (count($file_directory) > 2) {
	       //  $file_name = 'invoices.csv';
	       //  $handle = fopen($path_to_file . $file_name, 'r');

	       //  $i = 0;
	       //  while ($invoices = fgetcsv($handle)) {
	       //  	foreach ($invoices as $invoice) {
	       //  		$field = explode(';', $invoice);
	       //  		$data[$i]['num'] = (int) $field[0];
	       //  		$data[$i]['saldo_start'] = $field[1];
	       //  		$data[$i]['sum'] = $field[2];
	       //  		$data[$i]['payment'] = $field[3];
	       //  		$data[$i]['saldo_end'] = $field[4];
	       //  		$i++;
	       //  	}
	       //  }
        // }

        // array_shift($data);
        // return $data;

        if (count($file_directory) > 2) {
	        $handle = fopen($path_to_file . $file_name, 'r');

	        while ($data_file = fgetcsv($handle)) {
	        	foreach ($data_file as $df) {
	        		$transformed_data = explode(';', $df);
	        		array_push($data, $transformed_data);
	        	}
	        }
        }

        array_shift($data);
        return $data;
	}
}