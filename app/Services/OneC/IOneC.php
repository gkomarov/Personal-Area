<?php

namespace App\Services\OneC;

use App\Services\DateTime\IDateTime;

interface IOneC
{
    public function connect(string $url);
    public function formPeriod();
    public function formPeriodPdf();
    public function getClientBalance(string $invoice_num);
    public function getClientInvoicesByPeriod(array $period, string $counteragent_key);
    public function getClientInvoicePreviousMonth(array $period, string $counteragent_key);
    public function checkExistsPayment(string $num);
}