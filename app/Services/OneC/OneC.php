<?php

namespace App\Services\OneC;

use App\Services\OneC\IOneC;
use DateTime;
use SoapClient;
use Auth;

class OneC implements IOneC
{
    private $host = 'http://80.254.115.133:8070/';
    private $login = 'webclient';
    private $password = '123';

    /**
     * Connect with 1C
     *
     * @param $url - url
     * @return object for work with methods
     */
    public function connect(string $url)
    {
        $options = array(
            'login' => $this->login,
            'password' => $this->password,
            'soap_version'   => SOAP_1_2,
            'cache_wsdl'     => WSDL_CACHE_NONE,
            'exceptions'     => true,
            'trace'          => 1
        );

        return new SoapClient($url, $options);
    }

    /**
     * Forms date period
     *
     * @return array with start and stop date 
     */
    public function formPeriod()
    {
        $current_date = new DateTime();
        $current_year = $current_date->format('y');
        $current_month = $current_date->format('m');
        $last_day_month = $current_date->modify('last day of')->format('d'); 

        $start = new DateTime("$current_year-$current_month-01");
        $start_period = $start->getTimestamp();

        $end = new DateTime("$current_year-$current_month-$last_day_month" . 'T23:59:59');
        $end_period = $end->getTimestamp();

        return array(
            'StartPeriod' => $start_period, 
            'EndPeriod' => $end_period
        );
    }

    public function formPeriodPdf()
    {
        $date = new DateTime();
        $date->modify('- 1 month');
        $current_year = $date->format('y');
        $current_month = $date->format('m');
        $last_day_month = $date->modify('last day of')->format('d'); 

        $start = new DateTime("$current_year-$current_month-01");
        $start_period = $start->getTimestamp();

        $end = new DateTime("$current_year-$current_month-$last_day_month" . 'T23:59:59');
        $end_period = $end->getTimestamp();

        return array(
            'StartPeriod' => $start_period, 
            'EndPeriod' => $end_period
        );
    }

    /**
     * Gets client balance
     *
     * @return array client balance
     */
    public function getClientBalance(string $invoice_num)
    {  
        $url = $this->host . 'oookp_Donskoy/ws/inpk.1cws?wsdl';

        /*
            Сделанно так, потому что переодически падает сервер 1C
        */
        try {
            $connect = $this->connect($url);
        } catch(\Exception $e) {
            $connect = $this->connect($url);
        }
        
        $params = $this->formPeriod();
        $params['IdAccount'] = $invoice_num;
        $balance = $connect->GetClientBalance($params);
        
        return json_decode($balance->return);
    }

    public function getClientBalancePrev(string $invoice_num)
    {
        $current_date = new DateTime();
        $current_date->modify('previous month');
        $current_year = $current_date->format('y');
        $current_month = $current_date->format('m');
        $last_day_month = $current_date->modify('last day of previous month')->format('d'); 

        $start = new DateTime("$current_year-$current_month-01");
        $start_period = $start->getTimestamp();

        $end = new DateTime("$current_year-$current_month-$last_day_month" . 'T23:59:59');
        $end_period = $end->getTimestamp();

        $url = $this->host . 'oookp_Donskoy/ws/inpk.1cws?wsdl';

        try {
            $connect = $this->connect($url);
        } catch(\Exception $e) {
            $connect = $this->connect($url);
        }
        
        $params['StartPeriod'] = $start_period;
        $params['EndPeriod'] = $end_period;
        $params['IdAccount'] = $invoice_num;
        $balance = $connect->GetClientBalance($params);
        
        return json_decode($balance->return); 
    }

    /**
     * Gets client invoice for previous month from current
     *
     * @return array with invoice
     */
    public function getClientInvoicePreviousMonth(array $period, string $counteragent_key)
    {
        return $this->getClientInvoicesByPeriod($period, $counteragent_key);
    }

    /**
     * Gets client invoices by period
     *
     * @param $period - start and end period
     * @param $period - $user
     * @return array invoices
     */
    public function getClientInvoicesByPeriod(array $period, string $counteragent_key)
    {

        $start = $period['start']['year'] . '-' . $period['start']['month'] . '-' . $period['start']['day'] . 'T00:00:00';

        $end = $period['end']['year'] . '-' . $period['end']['month'] . '-' . $period['end']['day'] . 'T23:59:59';

        if (!$counteragent_key) {
            $counteragent_key = Auth::user()->counteragent_key;
        }

        /*
            Соответствует этому url: http://80.254.115.133:7080/OOOKP_Donskoy/ru_RU/odata/standard.odata/Document_РеализацияТоваровУслуг?$filter=Date ge datetime'2017-01-01T00:00:00' and Date le datetime'2017-6-31T23:59:59' and Posted eq true&$format=json
        */
        $url = $this->host . 'OOOKP_Donskoy/ru_RU/odata/standard.odata/Document_%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F%D0%A2%D0%BE%D0%B2%D0%B0%D1%80%D0%BE%D0%B2%D0%A3%D1%81%D0%BB%D1%83%D0%B3?$filter=Date%20ge%20datetime%27' . $start . '%27%20and%20Date%20le%20datetime%27' . $end . '%27%20and%20Posted%20eq%20true%20and%20%D0%98%D0%9D%D0%9F%D0%9A_%D0%92%D1%8B%D0%B3%D1%80%D1%83%D0%B6%D0%B0%D1%82%D1%8C%D0%92%D0%9B%D0%B8%D1%87%D0%BD%D1%8B%D0%B9%D0%9A%D0%B0%D0%B1%D0%B8%D0%BD%D0%B5%D1%82%20eq%20true%20and%20%D0%94%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D0%9A%D0%BE%D0%BD%D1%82%D1%80%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D0%B0_Key%20eq%20guid%27'. $counteragent_key . '%27&$format=json';

        $curl = curl_init();
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$this->login:$this->password")
        );

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl); 
        $data = json_decode($result);

        if(isset($data) && isset($data->value)) {
            if ($data->value) {
                return $data->value[0];
            } else {
                return null;  
            }
        } else {
            return null;
        }
    }

    /**
     * Gets counteragent key
     *
     * @param $user_invoice_num - user invoice num
     * @return counteragent key or null
     */
    public function getCounteragentKey(int $user_invoice_num)
    {
        /*
            Соответствует этому url: http://80.254.115.133:7080/OOOKP_Donskoy/ru_RU/odata/standard.odata/Catalog__ДоговорыКонтрагентов?$filter=Номер eq '1'
        */
        $url = $this->host . 'OOOKP_Donskoy/ru_RU/odata/standard.odata/Catalog_%D0%94%D0%BE%D0%B3%D0%BE%D0%B2%D0%BE%D1%80%D1%8B%D0%9A%D0%BE%D0%BD%D1%82%D1%80%D0%B0%D0%B3%D0%B5%D0%BD%D1%82%D0%BE%D0%B2?$filter=%D0%9D%D0%BE%D0%BC%D0%B5%D1%80%20eq%20%27' . $user_invoice_num . '%27&$format=json';

        $curl = curl_init();
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$this->login:$this->password")
        );

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl); 
        $data = json_decode($result);
        
        if (isset($data)) {
            if($data->value) {
                return $data->value[0]->Ref_Key;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Checks payment on exists
     *
     * @param $num - payment num
     * @return true or false
     */
    public function checkExistsPayment(string $num)
    {
        /*
            Соответствует этому url: http://80.254.115.133:7080/OOOKP_Donskoy/ru_RU/odata/standard.odata/Document_ПоступлениеНаРасчетныйСчет?$filter=Posted eq true and like(НазначениеПлатежа, 'oper_guid=5a72bfc57873c65')&$format=json
        */
        $url = $this->host . 'OOOKP_Donskoy/ru_RU/odata/standard.odata/Document_%D0%9F%D0%BE%D1%81%D1%82%D1%83%D0%BF%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5%D0%9D%D0%B0%D0%A0%D0%B0%D1%81%D1%87%D0%B5%D1%82%D0%BD%D1%8B%D0%B9%D0%A1%D1%87%D0%B5%D1%82?$filter=Posted%20eq%20true%20and%20like(%D0%9D%D0%B0%D0%B7%D0%BD%D0%B0%D1%87%D0%B5%D0%BD%D0%B8%D0%B5%D0%9F%D0%BB%D0%B0%D1%82%D0%B5%D0%B6%D0%B0,%20%27' . '%oper_guid=' . $num . '%' . '%27)&$format=json';

        $curl = curl_init();
        $headers = array(
            'Content-Type: application/json',
            'Authorization: Basic '. base64_encode("$this->login:$this->password")
        );

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($curl);
        curl_close($curl); 
        $data = json_decode($result);

        if (isset($data)) {
            if($data->value) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}