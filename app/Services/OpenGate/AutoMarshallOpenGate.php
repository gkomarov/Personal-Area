<?php

namespace App\Services\OpenGate;

use App\Services\OpenGate\IOpenGate;
use PDO;
use Carbon\Carbon;

class AutoMarshallOpenGate implements IOpenGate
{
    private $host = '87.117.2.183';
    private $port = 1433;
    private $database = 'default';
    private $user = 'lk_don';
    private $password = 'asdf1234@';
    private $connection;
    private $driver;

    private $vehicleTypeId = 2;
    private $createdOn;
    private $modifiedOn = '';
    private $createdById = 3;

    public function __construct()
    {
        $this->connection = new PDO("dblib:host=$this->host:$this->port;dbname=$this->database", "$this->user", "$this->password");
        $this->createdOn = Carbon::now()->timezone('Europe/Moscow')->format('Y-m-d H:i:s');
    }

    public function createCar($data)
    {
        $sql = 'INSERT INTO VehicleDataRecords (Plate, DatabaseId, VehicleTypeId, CreatedOn, ModifiedOn, CreatedById) VALUES (:plate, :databaseId, :vehicleTypeId, :createdOn, :modifiedOn, :createdById)';

        $query = $this->connection->prepare($sql);

        $query->bindValue(':plate', $data['number'], \PDO::PARAM_STR);
        $query->bindValue(':databaseId', $data['databaseId'], \PDO::PARAM_INT);
        $query->bindValue(':vehicleTypeId',  $this->vehicleTypeId, \PDO::PARAM_INT);
        $query->bindValue(':createdOn', $this->createdOn, \PDO::PARAM_STR);
        $query->bindValue(':modifiedOn', $this->modifiedOn, \PDO::PARAM_STR);
        $query->bindValue(':createdById', $this->createdById, \PDO::PARAM_INT);

        $query->execute();

        return $query->errorInfo()[1];
    }

    public function updateCar($number, $data)
    {
        $sql = 'UPDATE VehicleDataRecords SET Plate = :plate, DatabaseId = :databaseId, VehicleTypeId = :vehicleTypeId, CreatedOn = :createdOn, ModifiedOn = :modifiedOn, CreatedById = :createdById WHERE Plate = :number_car';

        $query = $this->connection->prepare($sql);

        $query->bindValue(':plate', $data['number'], \PDO::PARAM_STR);
        $query->bindValue(':databaseId', $data['databaseId'], \PDO::PARAM_INT);
        $query->bindValue(':vehicleTypeId',  $this->vehicleTypeId, \PDO::PARAM_INT);
        $query->bindValue(':createdOn', $this->createdOn, \PDO::PARAM_STR);
        $query->bindValue(':modifiedOn', $this->modifiedOn, \PDO::PARAM_STR);
        $query->bindValue(':createdById', $this->createdById, \PDO::PARAM_INT);
        $query->bindValue(':number_car', $number, \PDO::PARAM_INT);

        $query->execute();

        return $query->errorInfo()[1];
    }

    public function deleteCar($number)
    {
        $sql = 'DELETE FROM VehicleDataRecords WHERE Plate = :number_car';

        $query = $this->connection->prepare($sql);

        $query->bindValue(':number_car', $number, \PDO::PARAM_STR);

        $query->execute();

        return $query->errorInfo()[1];
    }
}