<?php

namespace App\Services\OpenGate;

interface IOpenGate
{
    public function createCar($data);
    public function updateCar($number, $data);
    public function deleteCar($id);
}