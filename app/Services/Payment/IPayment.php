<?php

namespace App\Services\Payment;
use App\Models\Invoice\IInvoice;

interface IPayment
{
    public function checkResponse(array $response);
    public function checkTotalPrice($model, $data);
    public function formPaymentParams(string $amount, string $payment_num, string $payment_description, string $redirect_url);
}