<?php

namespace App\Services\Payment;

use App\Services\Payment\IPayment;
use App\Models\Invoice\IInvoice;
use App\Services\Payment\GeneralServicesPaymentHandler;
use App\Services\Payment\AdditionalServicesPaymentHandler;
use DateTime;
use Auth;
use DB;

class PsbankPayment implements IPayment
{
    /**
     * Checks response payment system
     *
     * @param $response - response payment system
     * @return true or false
     */
    public function checkResponse(array $response)
    {
        if ($response['RC'] == 00) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check total price
     *
     * @param $data - additionals_id, total_price
     * @return true or false
     */
    public function checkTotalPrice($model, $data)
    {
        $valid_total_price = $model->getTotalPrice($data->additionals_id);

        if ($valid_total_price == $data->total_price && $data->total_price > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Forms payments params
     * 
     * @param $amount - amount payment
     * @param $payment_num - payment num
     * @param $payment_description - description payment
     * @return array params for payment system
     */
    public function formPaymentParams(string $amount, string $payment_num, string $payment_description, string $redirect_url)
    {
        $user = Auth::user();
        $date = new DateTime();

        /*  
            Для тестовой среды

            $params = [
                'amount' => $amount,
                'currency' => 'RUB',
                'order' => time(),
                'desc' => $user->invoice_num . '~' . $payment_num . '~' . $payment_description,
                'terminal' => 79036934,
                'trtype' => 1,
                'merch_name' => 'donskoy',
                'merchant' => 790367686219999,
                'email' => 'ilia.kaduk@in-pk.com',
                'timestamp' => $date->format('YmdHis'),
                'nonce' => hash('md5', 'nonce'),
                'backref' => $redirect_url,
            ];
        */

        $params = [
            'amount' => $amount,
            'currency' => 'RUB',
            'order' => time(),
            'desc' => $user->invoice_num . '~' . $payment_num . '~' . $payment_description,
            'terminal' => 35504301,
            'trtype' => 1,
            'merch_name' => 'donskoy',
            'merchant' => '000490035504301',
            'email' => 'ilia.kaduk@in-pk.com',
            'timestamp' => $date->format('YmdHis'),
            'nonce' => hash('md5', 'nonce'),
            'backref' => $redirect_url,
        ];

        $key = iconv_strlen($params['amount']) . $params['amount'] . iconv_strlen($params['currency']) . $params['currency'] . iconv_strlen($params['order']) . $params['order'] . iconv_strlen($params['merch_name']) . $params['merch_name'] . iconv_strlen($params['merchant']) . $params['merchant'] . iconv_strlen($params['terminal']) . $params['terminal'] . iconv_strlen($params['email']) . $params['email'] . iconv_strlen($params['trtype']) . $params['trtype'] . iconv_strlen($params['timestamp']) . $params['timestamp'] . iconv_strlen($params['nonce']) . $params['nonce'] . iconv_strlen($params['backref']) . $params['backref'];
        $hmac = hash_hmac('sha1', $key, pack('H*', 'CEA679D2F19731F87C0C26EDDF59BF7C'));
        $params['hmac'] = $hmac;

        return $params;
    }

    /**
     * Cancel payment
     *
     * @param $payment_model - payment model
     * @param $payment_num - payment num
     * @return response
     */
    public function cancel($payment_model, string $payment_num)
    {
        return $payment_model->remove($payment_num);
    }
}