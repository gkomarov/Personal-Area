<?php

namespace App\Services\Pdf;

interface IPdf
{
    public function create($data);
}