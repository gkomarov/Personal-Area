<?php

namespace App\Services\Pdf;

use App\Services\Pdf\IPdf;
use http\Env\Request;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Mail;

class MPdf implements IPdf
{
    private function dateFormat($date, $format='short')
    {
        $month = [
            '01' => 'январь',
            '02' => 'февараль',
            '03' => 'март',
            '04' => 'апрель',
            '05' => 'май',
            '06' => 'июнь',
            '07' => 'июль',
            '08' => 'август',
            '09' => 'сентябрь',
            '10' => 'октябрь',
            '11' => 'ноябрь',
            '12' => 'декабрь',
        ];

        $month_gentive = [
            '01' => 'января',
            '02' => 'февараля',
            '03' => 'марта',
            '04' => 'апреля',
            '05' => 'мая',
            '06' => 'июня',
            '07' => 'июля',
            '08' => 'августа',
            '09' => 'сентября',
            '10' => 'октября',
            '11' => 'ноября',
            '12' => 'декабря',
        ];
        $array_date = explode('-',$date);
        /** $array_date[0] - год.
            $array_date[1] - месяц.
            $array_date[2] - день.
            Используется в качестве ключа массива $month
         */
        if ($format == 'full') {
            $date_object = new \DateTime($date);
            return date_format($date_object,'j').' '.$month_gentive[$array_date[1]].' '.$array_date[0].'г.';
        }
        return $month[$array_date[1]].' '.$array_date[0].'г.';
    }

    public function create($data)
    {
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];
        $page_orientation = (isset($data['page_orientation'])) ? $data['page_orientation'] : 'L';

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];
        $mpdf = new \Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                __DIR__ . '/fira',
            ]),
            'fontdata' => $fontData + [
                    'frutiger' => [
                        'R' => 'times-new-roman.ttf',
                    ]
                ],
            'default_font' => 'ctimes',
            'mode' => 'utf-8',
            'format' => [210, 297],
            'orientation' => $page_orientation,
        ]);

        $view = view($data['template_pdf_content'], $data);

        $css = file_get_contents('css/pdf.css');

        $mpdf->WriteHTML($css, 1);
        $mpdf->WriteHTML($view, 2);

        return ['Mpdf' =>$mpdf];
    }

    public function output($data, $output, $emails)
    {
        $data['str_custom_date'] = $this->dateFormat($data['date'], $data['date_format']);

        $pdf_object = $this->create($data);

        if ($output == 'email') {
            $mask = "pdf/*.pdf";

            array_map("unlink", glob($mask));

            $file_name = 'pdf/' . $data['file_name'];
            $pdf_object['Mpdf']->Output($file_name, 'F');

            Mail::send($data['template_email'], array('info' => $data), function($message) use ($data, $emails, $file_name) {
                $message->to($emails)->subject($data['subject_email'] ." " . $data['str_custom_date'])->attach($file_name);
            });

            return 1;
        } elseif ($output == 'display') {
            $pdf_object['Mpdf']->Output();
        }

    }

}