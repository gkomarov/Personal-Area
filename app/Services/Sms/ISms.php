<?php

namespace App\Services\Sms;

interface ISms
{
    public function sendSmsMessage(string $content, string $phone);
}