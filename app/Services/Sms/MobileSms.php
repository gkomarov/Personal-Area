<?php

namespace App\Services\Sms;

use App\Services\Sms\ISms;

class MobileSms implements ISms
{
    public function sendSmsMessage(string $content, string $phone)
    {
        $sms = 'https://newbsms.tele2.ru/api/?operation=send&login=ht813922057&password=tW3NFvG7&msisdn='
        .preg_replace('~[^0-9]+~', '', $phone) . '&shortcode=INPK&text=' . $content;

        //dd($sms);

        $url = curl_init();
        curl_setopt($url, CURLOPT_URL, $sms);

        return curl_exec($url);
    }
}