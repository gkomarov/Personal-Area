<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Номера телефонов
    |--------------------------------------------------------------------------
    |
    | Массив номеров телефонов основных должностей, для отправки им смс.
    | Например о новых добавленных услугах или других оповещений.
    | Если у администратора сменился номер, то достаточно заменить его здесь.
    |
    */

    'phones' => [
        // 'administrator' => '+79381206787'
        'administrator' => '+79281479335'
    ],

    /*
    |--------------------------------------------------------------------------
    | Email адреса
    |--------------------------------------------------------------------------
    |
    | Массив email адресов основных должностей, для оповещений и отправки документов.
    | В коде указывается только должность, которая является ключом.
    |
    */

    'emails' => [
        'administrator' => 'tatyana.belaya@in-pk.com',
    ],

    'act_emails' => [
        'tatyana.vichkan@in-pk.com',
        'olga.bagrova@in-pk.com'
    ]
];
