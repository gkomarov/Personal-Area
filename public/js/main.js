function showLines(option) {
    /**Очистка полей и их скрытие*/
    $("[data-visible]").attr("required", false).hide();


    /**Отображение тех у которых data-visible равен option
     * option может содержать несколько значений, поэтому значение атрибута разбивается на массив,*/
    $("input[data-visible],select[data-visible]").each(function(){
        var str_visible = $(this).attr("data-visible");
        var arr_visible = str_visible.split(',');
        /** В получившемся массиве ищем выбранный option.
         * indexOf возвращает позицию элемента, поэтому делаем +1, чтобы 0-вая позиция не была false*/
        if(arr_visible.indexOf(option)+1){
            /** Enable и require инпута*/
            $(this).attr("required", true);
            $(this).show();

            /** Enable родительского material-блока*/
            $(this).parent().show();
        }
    });
}



var status_codes = {
    '500': 'Внутренняя ошибка сервера',
    '502': 'Ошибочный шлюз',
    '503': 'Сервис недоступен',
    '400': 'Плохой, неверный запрос',
    '401': 'Не авторизован',
    '402': 'Необходима оплата',
    '403': 'Запрещено',
    '404': 'Не найдено',
    '405': 'Метод не поддерживается',
    '406': 'Неприемлемо',
    '407': 'Необходима аутентификация прокси',
    '408': 'Истекло время ожидания',
    '409': 'Конфликт',
    '410': 'Удалён',
    '411': 'Необходима длина',
    '412': 'Условие ложно',
    '413': 'Полезная нагрузка слишком велика',
    '414': 'URI слишком длинный',
    '415': 'Неподдерживаемый тип данных',
    '416': 'Диапазон не достижим',
    '417': 'Ожидание не удалось',
    '418': 'Я — чайник',
    '422': 'Необрабатываемый экземпляр',
    '423': 'Заблокировано',
    '424': 'Невыполненная зависимость',
    '426': 'Необходимо обновление',
    '428': 'Необходимо предусловие',
    '429': 'Слишком много запросов',
    '431': 'Поля заголовка запроса слишком большие',
    '451': 'Недоступно по юридическим причинам',
    '500': 'Внутренняя ошибка сервера',
    '501': 'Не реализовано',
    '502': 'Плохой, ошибочный шлюз',
    '503': 'Сервис недоступен',
    '504': 'Шлюз не отвечает',
    '505': 'Версия HTTP не поддерживается',
    '506': 'Вариант тоже проводит согласование',
    '507': 'Переполнение хранилища',
    '508': 'Обнаружено бесконечное перенаправление',
    '509': 'Исчерпана пропускная ширина канала',
    '510': 'Не расширено',
    '511': 'Требуется сетевая аутентификация',
    '520': 'Неизвестная ошибка',
    '521': 'Веб-сервер не работает',
    '522': 'Соединение не отвечает',
    '523': 'Источник недоступен',
    '524': 'Время ожидания истекло',
    '525': 'Квитирование SSL не удалось',
    '526': 'Недействительный сертификат SSL',
};



$(document).ready(function () {

    // setFocusInput();

    function CalculateCommission (sum){
        var sumWithoutCommission = ((Number(sum)/101.2) * 100);
        var commission = Number(sum) - sumWithoutCommission;
        if (!sum){
            sumWithoutCommission = '';
            commission = '';
        }
        $("#commission").text(commission.toFixed(2));
        $("#sum_with_commission").text(sumWithoutCommission.toFixed(2));
    }

    $("input[name=sum]").keyup(function(){
        var sum = parseFloat($("input[name=sum]").val());
        CalculateCommission(sum);
    });

    /**Если пользователь на домашней странице, то подгружаются данные из 1С*/
    if (window.location.pathname == '/home') {

        function GetInvoice() {
            var date = new Date;
            var month = date.getMonth();

            /**Месяц должен быть двузначным*/
            if (month < 10) {
                month = "0" + month;
            }

            var period = new Date(date.getFullYear() + '-' + month + '-' + '01');

            /**timestamp в php (а может и в другиз языках) формируется иначе, чем в javascript
             * чтобы уравнять их, полученный timestamp нужно делить на 1000*/
            var timestamp = period.getTime() / 1000;

            $.ajax({
                url: '/invoices/period',
                data: {
                    'start': timestamp,
                    'type': 'json',
                },
                success: function (res) {
                    if (res.invoice != null) {
                        $(".receipt_link").show();
                    }
                    else {
                        $(".receipt_link").remove();
                    }
                },
                error: function (e) {
                    console.info("error: ", e);
                }
            });
        }

        function GetBalance() {
            $.ajax({
                url: '/users/balance',
                method: 'GET',
                success: function (res) {
                        var result = JSON.parse(res);
                        if (result['IdAccount'] != 0) {
                            /**Если пользователь с лицевым счётом, то данные отправляются на страницу.*/
                            for (var item in result) {
                                var color = '';
                                var value = result[item];
                                if (typeof(value) === 'number'){
                                  value = Math.abs(value).toFixed(2);
                                  value = new Intl.NumberFormat('ru-RU').format(value);
                                }

                                if (item == "StartBalance" || item == "EndBalance") {
                                    if (result[item] > 0) {
                                        color = "color-red";
                                        value = '<small class="color-grey">задолженность:</small>  ' + value;

                                    } else if(result[item] < 0){
                                        color = "color-green";
                                        value = '<small class="color-grey">переплата:</small>  ' + value;
                                    } else {
                                        color = "color-green";
                                    }
                                }
                                /**Ключи в массиве соответствуют классам этих элементов в html,
                                 * поэтому item используем как селектор*/
                                $("." + item).addClass(color).html("<span class='font-medium'>" + value + "<span> руб.</span></span>");
                            }
                            /**Этот блок переопределяется, потому-что в предыдущей итерации подставляется "руб"*/
                            $(".IdAccount").html(result['IdAccount']);

                            /**Заполнение полей пополнения счёта*/
                            if (result['EndBalance'] > 0) {

                              var clearDebt = result['EndBalance'];
                              var debtWithCommission = (clearDebt + (clearDebt * 0.012));

                              $('[name=sum]').val(debtWithCommission.toFixed(2));
                              CalculateCommission(debtWithCommission);
                            }
                        }
                        else {
                            /**Если пользователь не имеет лицевого счёта, то и данных быть не должно*/
                            for (var item in result) {
                                $("." + item).html("<span class='font-medium'>Нет данных<span>");
                            }
                        }
                    },
                    error: function (e) {
                        /**Если при первой попытке получить данные, сервер вернул ошибку, тут же запрос повторяется*/
                        GetBalance();
                    }
                });
        }

        GetBalance();
        GetInvoice();
    }

    UIkit.update(event = 'update');

    var Mask = {
        permition: function () {
            $("[data-mask=permition]").pickadate({
                min: true,
                max: 15,
                format: 'dd.mm.yyyy',
                formatSubmit: 'yyyy-mm-dd',
                hiddenName: true,
                onSet: function() {
                    UserForm.material();
                },
            });
        },
        date: $("[data-mask=date]").pickadate({
            min: true,
            format: 'dd.mm.yyyy',
            formatSubmit: 'yyyy-mm-dd',
            hiddenName: true,
            onSet: function() {
                UserForm.material();
            },
        }),
        pass_date: $("[data-mask=pass_date]").pickadate({
            format: 'dd.mm.yyyy',
            formatSubmit: 'yyyy-mm-dd',
            hiddenName: true,
            onSet: function() {
                UserForm.material();
            },
        }),
        money: $("input[data-mask=money]").inputmask({
            alias: 'decimal',
            groupSeparator: ' ',
            autoGroup: true,
            digits: 1,
            rightAlign: false,
            showMaskOnHover: false,
            showMaskOnFocus: false,
            autoUnmask: true,
            removeMaskOnSubmit: true,
        }),
        numeric: $("input[data-mask=numeric]").inputmask("9{1,20}",
            {
                showMaskOnHover: false,
                showMaskOnFocus: false,
            }),
        phone: $("input[data-mask=phone]").inputmask("+7(999)999-99-99", {
            "onincomplete":
                function () {
                    if (this.value != '') {
                        alert('Пожалуйста, заполните поле до конца');
                    }
                }
        }),
        email: $("input[data-mask=email]").inputmask("email", {
            "onincomplete":
                function () {
                  alert('Пожалуйста, заполните поле до конца');
                }
        }),
        birthday: function () {
            $("[data-mask=birthday]").pickadate({
                max: true,
                format: 'dd.mm.yyyy',
                formatSubmit: 'yyyy-mm-dd',
                hiddenName: true,
                selectYears: 100,
                selectMonths: true,
                onSet: function() {
                    UserForm.material();
                },
            })
        },
        length_limit: function() {
            $("input[data-mask=length_limit]").inputmask("*{1,10}",
            {
                definitions: {
                    "*": {
                        validator: ".",
                        cardinality: 1,
                    }
                },
                showMaskOnHover: false,
                showMaskOnFocus: false,
            });
        },
        number_car: function () {
          var mask_list = {
            'russian': 'A999AA99[9]',
            'dpr': 'A999AA',
            'ukrainian': 'AA9999AA',
            'armenian': '99AA999'
          }
          for (var i in mask_list) {
            $("input[data-mask=" + i + "]").inputmask(mask_list[i],
            {
              definitions: {
                  "A": {
                      validator: "[a-zA-Z]",
                      casing: "upper",
                  }
              },
              onKeyValidation: function (key, result) {
                if (!result) {
                  Alert.show("Обратите внимание, что буквы должны быть латинскими.", 'notice');
                }
              },
              placeholder: '',
              showMaskOnHover: false,
              showMaskOnFocus: false,
            });
          }
        },
        templateReplace: function (pointer, e) {
          var mask = pointer.getAttribute("data-mask");
          var $this = $(".aside-container .number-template__item[template=" + mask + "]");
          var default_template = $this.attr("default");
          var template_reminder = default_template.substr(pointer.value.length,);
          var number_in_span = '';
          for (var i in pointer.value){
            number_in_span += '<span>' + pointer.value[i] + '</span>'
          }
          $this.html(number_in_span + template_reminder);
        },
        setValueToMask: function (mask) {
          var aside = $(".aside-container");
          var current_number = aside.find(".number_car").val();
          console.info(mask);
          aside.find(".number_car").attr("data-mask", mask);
          aside.find(".number-template__item").hide();
          aside.find(".number-template__item[template=" + mask + "]").text(current_number);
          aside.find(".number-template__item[template=" + mask + "]").show();
        }
    }

    var Payment = {
        sendPay: function (url, data) {
            /**Текущая лоакция добавляется в массив для того, чтобы после оплаты, кнопка "Вернуться в магазин"
             * вернула пользователя именно на эту страницу, а не всегда на главную*/
            data['redirect_url'] = window.location.href;
            var data_action = data['data-action'];
            $.ajax({
                url: url,
                /**В контроллере проверяется, равна ли сумма, которая отправлена с фронта,
                 * сумме цен, заказанных услуг*/
                data: data,
                type: 'POST',
                success: function (res) {
                    if (data_action == 'save_order'){
                        /**Если был сохранён черновик, то выполнится перезагрузка*/
                        location.reload();
                    } else {
                        /**В ответ приходят данные для формы отправки в платёжку. Ключи массива = input name*/
                        var form = '<form class="payment-redirect" method="POST" action="https://3ds.payment.ru/cgi-bin/cgi_link">';
                        for (item in res) {
                            var name = item;
                            if (item == 'hmac') {
                                /**При отправке формы, этот параметр должен называться p_sign (такие правила платёжки)*/
                                name = 'p_sign';
                            }
                            form += '<input type="hidden" name=' + name.toUpperCase() + ' value=' + res[item] + '>';
                        };
                        form += '</form>';
                        /**Сформированная форма, добавляется в body и выполняется submit этой формы*/
                        $(form).appendTo("body");
                        $(".payment-redirect").submit();
                    }
                },
                error: function (e) {
                    Alert.show(status_codes[e.status], 'danger');
                }
            });
        },
        collectingAdditionals: function(current){
            var ids = [];
            var additionals_arr = [];
            var data_action = current.attr('data-action');


            /**Каждя выбранная доп.услуга помечается классом done.
             * На оплату собирается массив данных, собирающийся из всех с классом done.
             * 1. Additionals - информация об услугах*/
            $(".additionals-create div").find(".done").each(function () {

                /**2. Ids - массив id из этих услуг.*/
                ids.push($(this).attr("id"));

                var additional = {};
                additional['additional_id'] = $(this).attr("id");
                //additional['close_date'] = $(this).attr("close_date");
                additional['comment'] = $(this).attr("comment");
                additionals_arr.push(additional);
            });
            /**3. Total price - общая сумма*/
            var total_price = $(".additionals-pay [total-price]").attr("total-price");

            var data = {
                "data-action": data_action,
                "additionals_id": ids,
                "total_price": total_price,
                "additionals": additionals_arr,
            }
            return data;
        }
    }

    var UserForm = {
        showOverlay: function () {
            $("<overlay></overlay>").appendTo("body");
            setTimeout(function () {
                $("overlay").addClass("active");
            }, 100);
        },
        hideOverlay: function () {
            $("overlay").removeClass("active");
            setTimeout(function () {
                $("overlay").remove();
            }, 300);
        },
        clone: function (selector, clear) {
            /**Форма клонирует пустую, существующую форму, чтобы обеспечить очистку данных при открытии формы*/
            /**Сперва удаляем все временные формы tmp*/
            $(".tmp").remove();
            /**Добавляется новая форма с классом tmp и вешается маска даты на поля permition*/
            $("." + selector).clone().addClass("tmp").appendTo("body");
            // Mask.permition();
            if (!clear) {
                $(".body-overlay").addClass("load");
                setTimeout(function () {
                    $(".body-overlay").addClass("active-overlay");
                    $(".tmp").show();
                }, 100);
            }
        },
        close: function () {
            $(".tmp").fadeOut(100);
            $(".tmp").remove();
            setTimeout(function () {
                UserForm.hideOverlay();
            }, 200);

        },
        hide: function (form) {
            $(form).removeClass("active");
            $(".translate-out").removeClass("active");
            $("body").removeClass("uk-overflow-hidden");
            $(".tmp").remove();
        },
        material: function () {
            /**Получается список всех input и select, которые нужно сделать material*/
            var inputs = document.querySelectorAll('input.material-input_text,select.material-input_text');

            inputs.forEach(function (input) {
                input.addEventListener('focus', function () {
                    this.parentNode.classList.add('is-focused');
                    this.parentNode.classList.add('has-label');
                });
                if (input.value != '') {
                    input.parentNode.classList.add('is-focused');
                    input.parentNode.classList.add('has-label');
                }
                input.addEventListener('blur', function () {
                    this.parentNode.classList.remove('is-focused');
                    if (this.value == '') {
                        this.parentNode.classList.remove('has-label');
                    }
                });
            });
        }
    }

    var Slide = {
        show: function (item) {
            /**Sidebar появляется на экране, по добавлению свойства к body*/
            $("body").attr(item, true);
        },
        hide: function () {
            /**Скрыть можно не только один элемент, а несколько, поэтому в переменную arguments попадает всё,
             * что нужно скрыть*/
            for (var i = 0; i < arguments.length; i++) {
                $("body").removeAttr(arguments[i]);
            }
        },
    }

    var Alert = {
        shure: function () {
            /**Открытие формы выбора "Да/Нет"*/
            UserForm.showOverlay();
            UserForm.clone("shure-popup");
            $("body").attr("shure-popup", true);

        },
        code: function () {
            /**Открытие формы подтверждения кода из смс*/
            UserForm.showOverlay();
            UserForm.clone("code");
            $("body").attr("code", true);

        },
        show: function (message, type, callback, place) {
            /**Для отображения нужно от 2-х до 4-х параметров
             * 1. message - сам текст уведомления
             * 2. type - тип оповещения (danger, succes). Тип пришивается к классу alert и меняет его цвет
             * 3. callback - функция, которую нужно выполнить после отображения оповещения
             * 4. place - если alert нужно вставить не в body (по умолчанию), а в какое-то конкретное место*/
            UserForm.clone("alert");
            $(".tmp").addClass("alert-" + type).text(message);
            if (place) {
                /**Если указано место, то вставить alert  в это место*/
                $(".tmp").prependTo(place);
            }
            /**Скрытие оповещения после 3-х секунд*/
            setTimeout(function () {
                $(".tmp").hide();
                $(".tmp").remove();
                callback;
            }, 5000);
        },
        inputMark: function (selector) {
            /**В аргументы функции придут селектор(-ы), которые нужно пометить классом required
             * Этот класс подскажет пользователю, что он ввёл неверно и пропадёт через 3 секунды*/
            $(selector).addClass("required");
            setTimeout(function () {
                $(selector).removeClass("required");
                $(selector).val("");
            }, 5000);
        },
    }// end Alert;

    var Aside = {
        pushContent: function (title, content) {
            /**Перед вставской в сайдбар, он очищается*/
            this.clear();

            /**В сайдбар кладётся контент(content), заголовок(title) и вещаются макси
             * (material, permition). Отображается сайдбар с новым контентом.*/
            $(".aside-title-text").text(title);
            $(".aside-container").append(content);
            UserForm.material();
            Mask.permition();
            Slide.show("aside");
            UserForm.showOverlay();
        },
        clear: function () {
            /**Все элементы сайдбара обнуляются*/
            $("[data-mask=permition],[data-mask=date],[data-mask=birthday]").pickadate().stop();
            $("aside :input").val("");
            $(".aside-title-text").text("");
            $(".aside-container").html("");

            $("[data-visible]").hide();
        }
    };

    var Building = {
        selectedBuilding: function(result, form){
            /**Если редактируется пользователь, то в список свободных домов подставляется его дом,
             * так появляется возможность изменить его дом или оставить прежним*/
            if (result['invoice_num']){
                var apart_number = '';
                if (result['apart_number']){
                    apart_number += ' кв.' + result['apart_number'];
                }
                $(form).find("#selected-building").val(result['building_id']);
                $(form).find("#selected-building").text('ул.' + result['street_name'] + ' ' + result['building_number'] + apart_number);
            }
        }
    };

    var Indication = {
        pushAdress: function(result, form){
            /**Если редактируются показания, добавить в блок имя и адрес*/
            if (result['water'] || result['energy']){
                $(form).find(".indication-user_list").remove();
                if (result['apart_number']){
                    result['apart_number'] = 'кв.'+result['apart_number'];
                }
                for (var key in result) {
                    $(form).find("#indication-user_address [name=" + key + "]").text(result[key]);
                }
            }
        }
    };

    var Item = {
        del: function (current, action) {

            /**Родитель с id самой записи*/
            // var parent = current.parent().parent().parent();
            var parent = current.closest('tr');
            var id = parent.attr("id");
            /**основу action берем в активном элементе навигации (у админа) или у нажатой кнопки (она же current)*/
            if (!action) {
                var method = 'remove';
                /**Костыль! Пока, по нажатию на delete, пользователь должен просто отвязываться от дома.
                 * Чтобы не ломать логику работы в таблице (удалени), подменяем method remove на unbind,
                 * чтобы перейти на другой url.*/
                if ($(".nav_item.active a").attr('pointer') == 'users') method = 'unbind';
                /***/
                var action = (current.attr('action') || $(".nav_item.active a").attr('pointer')) + '/' + method;
            }

            $.ajax({
                url: action,
                headers: {
                    'X-CSRF-TOKEN': _token
                },
                data: {
                    id: id,
                },
                type: 'POST',
                success: function (res) {
                    if (res) {
                        /**Удаляется элемент из DOM*/
                        parent.remove();

                        var table = parent.closest('table');
                        var len = table.find("tbody tr").length;
                        if (len <= 0) {
                            location.reload();
                        }
                    }
                },
                error: function (e) {
                    /**Вывести текст ошибки*/
                    var response = JSON.parse(e.responseText);
                    Alert.show(response.message, 'danger');
                    return false;
                }
            });
        },
        errorShow: function (e) {
            /**Все ошибки в errors получаются из массива валидации*/
            var response = JSON.parse(e.responseText);
            var errors = response.errors;

            for (var i in errors) {
                /**Backend возвращает ошибки с именами полей. Ключ массива - input name*/
                var currentInput = $(".aside-container").find("[name=" + i + "]")
                var errorPlace = currentInput.parent();
                /** По умолчанию контейнер поля с ошибкой и есть родитель поля */
                var materialInput = errorPlace;
                var specialErrorPlace = currentInput.attr('error-place');

                if (specialErrorPlace != undefined){
                    errorPlace = $(".aside-container ." + specialErrorPlace)
                    /** Но если разметка ошибок не шаблонная, то контенер поля
                    будем искать внутри "нешаблонного места" */
                    materialInput = errorPlace.find('.material-input');
                }
                /**Родитель input'а помечается ошибочным*/
                Alert.inputMark(materialInput);
                var item = errors[i];
                /**Если уже есть оповещение в блоке, то удалится*/
                errorPlace.find(".alert").remove();
                errorPlace.append("<span class='alert alert-include alert-include__danger'>" + item[0] + "</span>");
            }
        },
        create: function (current) {
            var street_id = $(".street-list_item.active").attr("id");
            /**Если найдена улица, то можно приступать к удалению, иначе удаления не выйдет*/
            if (street_id) {
                /**Выбор формы для create зависит от того в какой вкладке находится пользователь
                 * Если мы в разделе buildings, то активная ссылка будет buildings,
                 * прибавится к шаблону -edit_form, что соответсвует существующей на странице, форме.
                 * После этого её можно вставить в sidebar*/
                var pointer = $(".nav_item.active a").attr('pointer');
                var form = "." + pointer + '-edit_form';
                if (pointer == 'users') {
                    form = ".users-create_form";
                }
                var title = $(form).attr("title");
                //var tmp_form = UserForm.clone(form);
                /**Добвление id улицы, при создании пользователя*/
                $(form).append("<input type='hidden' name='street_id' value=" + street_id + " />");
                Aside.pushContent(title, $(form).clone().removeClass("hidden-form"));
            } else {
                /**Если улица не выбрана, то основа формы возьмётся из переданного параметра current*/
                var form = "." + current.attr("form") + '-edit_form';
                var title = $(form).attr("title");
                Aside.pushContent(title, $(form).clone().removeClass("hidden-form"));
                Mask.birthday();
                Mask.number_car();
                deleteSpaces();
            }

            setFocusInput();
        },
        line_disable: function () {
            /**Все поля ввода с атрибутом life_update будут неактивны, рисунки и атрибут data-mode изменятся на edit*/
            $(".item__edit").attr("disabled", true);
            $("[data-mode=life_update]").text("Изменить").removeClass("button-transparent_green");
            $("[data-mode=life_update]").attr("data-mode", "life_edit");
        },
        life_edit: $("body").on("click", "[data-mode=life_edit]", function () {
            Item.line_disable();
            /**Текущее поле ввода с атрибутом станет активным, рисунки и атрибут изменятся на update*/
            $(this).attr("data-mode", "life_update").text("Сохранить").addClass("button-transparent_green");
            var name = $(this).attr("id");
            $(".item__edit[operand=" + name + "]").removeAttr("disabled");
        }),
        edit: function (current) {
            var id = current.parent().parent().parent().attr("id");
            if (id) {
                /**Если id записи не получен, то и изменять нечего*/
                var pointer = current.attr("action") || $(".nav_item.active a").attr('pointer');
                /**Формирование url = активная ссылка + id записи + edit*/
                var action = pointer + "/" + id;
                $.ajax({
                    url: action + "/edit",
                    data: {
                        id: id
                    },
                    success: function (res) {
                        var result = JSON.parse(res);
                        /**Вызывается форма редактирования (основа из pointer) и клонируется.*/
                        var cur_form = $("." + pointer + "-edit_form").clone();
                        /**Изменяются атрибуты формы для обновления. Input делает даёт знать серверу,
                         * что выполняется обновление */
                        $(cur_form).attr({
                            'action': action,
                            'method': 'POST'
                        }).append('<input name="_method" type="hidden" value="PUT">').removeClass("hidden-form");

                        /**Надстройка для buildings*/
                        Building.selectedBuilding(result, cur_form);

                        /**Надстройка для показаний. Добавляется адрес в сайдбар*/
                        Indication.pushAdress(result, cur_form);

                        for (var key in result) {
                            $(cur_form).find("[name=" + key + "]").val(result[key]);
                            /**Для шаблона*/
                            if (key === "number_mask"){
                              var mask = result[key];
                            }
                        }
                        /**Данные укладываются в форму. Key - input name*/
                        Aside.pushContent("Редактирование", cur_form);
                        if (result['type']) showLines(result['type']);
                        Mask.numeric;
                        Mask.setValueToMask(mask);
                        Mask.number_car();
                    }
                });
            } else {
                Alert.show("Не могу идентифицировать запись!", "danger");
            }
        },
        send: function (current) {
            /**Родитель от current будет формой отправки, с которой собирутся все данные*/
            var form = current.parent();
            var url = form.attr("action");
            var data = form.serialize();
            console.info('DATA', data, 'URL', url)
            $.ajax({
                url: url,
                data: data,
                type: 'post',
                success: function (res) {
                    /**При успехе опреации записи, перезагрузка страницы*/
                    location.reload();
                },
                error: function (e) {
                    if (e.status == 500) {
                      Alert.show('Что-то пошло не так. Попробуйте позже.', 'danger');
                    }
                    Item.errorShow(e);
                    if (e.status == 400) {
                        var response = JSON.parse(e.responseText);
                        Alert.show(response.overview, 'danger');
                    }
                }
            });
        },
    }

    //Активация input в стиле material
    UserForm.material();

    setFocusInput();

    /**Закрытие модальных форм*/
    $("body").on("click", ".button_close", function (e) {
        e.preventDefault();
        UserForm.close();
    });

    /**Закрытие вложенный slide внутри sidebar*/
    $("body").on("click", ".button_slide-close", function () {
        /**Обнуляется выбраная услуга при закрытии вложенного сайдбара
         * если закрывается не сайдбар, то ничего не происходит*/
        $(".new-additionals_list_item").removeClass("active").css("background-color", "white");

        Slide.hide($(this).data("type"));
    });

    $("body").on("click", "overlay", function () {
        /**Закрываются все открытые формы*/
        $(".tmp").remove();

        /** Скрываются активные slide формы*/
        $("body").removeAttr("aside aside-child shure-popup code");
        Aside.clear();
        UserForm.hideOverlay();
    });

    /**Скрытие sidebar*/
    $("body").on("click", "[data-type=aside]", function () {
        /**Скрытие вложенного сайдабара*/
        Slide.hide($(this).data("type"), 'aside-child');
        /**Скрытие и очистка содержимого основого сайдбара*/
        Aside.clear();
        UserForm.hideOverlay();
    });

    $("body").on("click", ".button_hide", function () {
        UserForm.hide($(this).parent().parent());
    });

    /**Кнопка "Сбытия" для админа*/
    $("body").on("click", "a[href='/events']", function (e) {
        /**Кнопка нахлдится в блоке ссылок,
         * поэтому останавливаем действия default*/
        e.preventDefault();
        $.ajax({
            url: "/logs",
            success: function (res) {
                var result = JSON.parse(res);

                /**Полученный результат логирования формируется в список*/
                var content = '<dl class="uk-description-list uk-description-list-divider uk-padding uk-padding-remove-vertical">';
                for (var i in result) {
                    var item = result[i];
                    content += '' +
                        '<dt class="color-green">' + item['updated_at'] + '</dt>' +
                        '<dd>' + item['overview'] + '</dd>';

                }
                content += '</dl>';

                /**и вставляется в сайдбар, с его активацией*/
                Aside.pushContent("Последние действия", content);
            },
            error: function (e) {
                Alert.show("За последние 3 дня ничего не произошло","success");
            }
        });
    });

    /**Смена маски номера машины*/
    $("body").on("change",".number-template__mask", function(){
      var mask = this.value
      $(".aside-container .number_car").val('').attr("data-mask", mask)
      $(".aside-container .number-template__item").hide();
      var default_value = $(".aside-container .number-template__item[template=" + mask + "]").attr("default");
      $(".aside-container .number-template__item[template=" + mask + "]").text(default_value).show();
      Mask.number_car();
    })

    $("body").on("keypress", ".number_car", function(e){
        Mask.templateReplace(this, e);
    });

    $("body").on("keydown", ".number_car", function(e){
      if (e.keyCode == 8) {
        Mask.templateReplace(this, e);
      }
    });

    /**Формирование начального периода для выборки*/
    $("body").on("change", ".filter__item", function () {
        /**Для получения истории от выбранной даты из 1С, нам нужно начало и конец периода. Конец периода подставляется
         * на backend-е автоматически. Начало периода нужно сформировать на frontend-е и преваратить в timestamp*/

        var month = $('.filter__item#month').val();
        var year = $('.filter__item#year').val();

        /**Чтобы сформировать timestamp начала периода, нужно иметь обязательно два параметра - месяц и год
         * поэтому формирование даты начаниается только при наличии обоих*/
        if (month && year) {
            var date = new Date();
            var period = new Date(year + '-' + month + '-' + '01');

            /**timestamp в php (а может и в другиз языках) формируется иначе, чем в javascript
             * чтобы уравнять их, полученный timestamp нужно делить на 1000*/
            var timestamp = period.getTime() / 1000;

            /**Значение укладывается в скрытый input, который будет отправлен*/
            $(".invoices-filter [name='start']").val(timestamp);
        }

    });



    /**Основыне действия**/

    /**Send отправляет уже заполненую форму*/
    $("body").on("click", "[data-mode=send]", function () {
        Item.send($(this));
    });

    /**Create открывает саму форму для создания*/
    $("body").on("click", "[data-mode=create]", function () {
        Item.create($(this));
    });

    /**Метод create типизирован и вызывает конкретную форму,
     * но на странице с buildings, есть сразу две формы, street и building,
     * поэтому для улиц написана отдельная функция*/
    $("body").on("click", "[data-mode=create-street]", function () {
        Aside.pushContent("Новая улица", $(".form_street").clone().removeClass("hidden-form"));
    });

    /**Выделено отдельной функцией, так как на создание нового пользователя совсем другая форма*/
    $("body").on("click", "[data-mode=users-create]", function () {
        UserForm.clone("users-create_form");
    });

    $("body").on("click", "[data-mode=edit]", function () {
        Item.edit($(this));
    });

    $("body").on("click", "[data-mode=update]", function () {
        Item.update($(this));
    });

    $("body").on("click", "[data-mode=delete]", function () {
        var current = $(this);
        /**Класс delete добавляется к кнопке, чтобы не потерять её после согласия на удаление.*/
        $("button").removeClass("delete");
        current.addClass("delete");
        Alert.shure();
    });

    /**По согласию вызывается удаляется элемент, который был помечен классом delete*/
    $("body").on("click", ".shure-popup .yes", function () {
        Item.del($(".delete"));

        UserForm.close();
        $("body").removeAttr("shure-popup");
    });

    $("body").on("click",".required",function(){
        $(this).removeClass("required");
        $(this).val("");
    });

    /**Создание пропуска*/
    $("body").on("click", "[data-action=permition-create]", function () {
        /**this - в этом случае, нажатая кнопка. Её родитель - форма отправки*/
        var form = $(this).parent();
        var url = form.attr("action");
        var data = form.find("input, select").serialize();
        /**У формы есть атрибут table, в котором указано имя таблицы, в которой создаётся новый пропуск*/
        var permition = form.attr("table");

        $.ajax({
            url: url,
            data: data,
            type: 'POST',

            success: function (res) {
                location.reload();
                /**В ответ приходит html новой строки таблицы, который вставляется в ту таблицу permition*/
                $(".table-permition[table=" + permition + "] tbody").append(res);
                /**Уборка сайдбара*/
                Slide.hide("aside");
                UserForm.hideOverlay();
                Aside.clear();
            },

            error: function (e) {
                if (e.status == 500) {
                  Alert.show('Что-то пошло не так. Попробуйте позже.', 'danger');
                }
                Item.errorShow(e);
                if (e.status == 400) {
                    var response = JSON.parse(e.responseText);
                    Alert.show(response.overview, 'danger');
                }
            }
        });
    });

    // YB

    /*
        Validate input last and first name for user residents. This fields mush have only russian symbols.
    */
    $('body').on('keypress', '[lang="ru"]', function() {
      var that = this;
      setTimeout(function() {
        if (that.value.match(/\w/g)){
          Alert.show("Допустимы только символы кирилицы", "notice");
        };
        var res = /[^а-яёА-ЯЁ]/g.exec(that.value);
        that.value = that.value.replace(res, '');
      }, 0);
    });

    /*
        Cursor for each first input form
    */
    function setFocusInput() {
        var firstInput = $('[focus-input=first]').find('input').filter(':visible:first');
        firstInput.focus();
    }

    /*
        Deletes spaces where creates autos
    */
    function deleteSpaces() {
        $("input[nospace]").on('keypress', function(e) {
            var newValue = $(this).val().split(' ').join('');
            $(this).val(newValue);
        });
    }

    // YB

    /**--Основыне действия--**/



    /**ОПЛАТА**/

    /**Добавление выбранной доп.услуги в список*/
    $("body").on("click", "[data-action=order]", function () {
        /**Фиксируется активная услуга*/
        var order = $(".new-additionals_list_item.active");

        var id = order.data("id");
        var price = parseFloat(order.data("price") * 1.00);
        var img = order.children("img").attr("src");
        var color = order.data("color");
        var name = order.find("[name=name]").text();
        //var close_date = $(".new-additionals_create [name=close_date]").val();
        var comment = $(".new-additionals_create [name=comment]").val();

        /**Нельзя заказать услугу, не выбрав дату выполнения*/
/*        if (close_date != '') {*/
            /**Текущая активаня конпка создания доп.услуги.
             * Её содержимое вставляется сразу же после добавление закзанной услуги*/
            var current = $(".additionals-create_item.active");
            var empty = current.html();

            /**Дублируется кнопка создания услуги*/
            $(".additionals-create").append("<div>" +
                                                "<div class='additionals-create_item uk-position-relative uk-position-z-index uk-padding-large uk-padding-remove-right uk-padding-remove-left active'>" +
                                                    empty +
                                                "</div>" +
                                            "</div>");

            var html = '<img src="' + img + '"><div>' + name + '</div>';

            /**А содержимое текущей меняется на содерживое заказанной услуги*/
            current.html(html)
                .css("background-color", color)
                .attr({
                    "id": id,
                    "price": price,
                    //"close_date": close_date,
                    "comment": comment,
                })
                .append("<div class='uk-flex-top uk-flex-right additionals-create_item-delete icon' icon=close></div>")
                .addClass("done").removeClass("active");

            /**Добавляется div с стоимостью услуги*/
            current.parent().append("<div class='font-medium price' data-price=" + price + ">" + price + "<span> руб </span></div>");

            /**Формирование новой "К оплате" при выборе ещё одной доп.услуги*/
            var total_price = $(".additionals-pay [total-price]");
            new_price = parseFloat(total_price.attr("total-price")) + price;

            total_price.attr("total-price", new_price).text(new_price + " руб");
            /**Активация блока с кнопкой оплаты*/
            $(".additionals-pay").attr("hiden", false);

            /**Поле стоимости услуги*/
            setTimeout(function () {
                current.parent().find(".price").addClass("active")
            }, 300);

            /***Скрытие панели и очистка всех следов*/
            order.removeClass("active").css("background-color", "white");
            Slide.hide("aside", "aside-child");
            Aside.clear();
            UserForm.hideOverlay();
/*        }
        else {
            Alert.show("Заполните поле", "danger");
            Alert.inputMark(".new-additionals_create [data-mask=date]");
        }*/
    });

    $("body").on("click", "[data-action=save_order]", function () {
        var data = Payment.collectingAdditionals($(this));
        /**В функцию оплаты отправлются данные (data) и url*/
        Payment.sendPay("additionals/pay", data);
    });

    $("body").on("click", "[data-action=send_act]", function(){

        if ($("#form_act [name=date]").val()) {

            $("#form_act").append("" +
                "<div class='uk-position-cover uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle'>" +
                "<span class='font-medium' uk-spinner></span>" +
                "</div>");

            $.ajax({
                url: 'additionals/send/act',
                headers: {
                    'X-CSRF-TOKEN': _token
                },
                data: {
                    'payment_id': $("#form_act [name=payment_id]").val(),
                    'date': $("#form_act [name=date]").val(),
                },
                method: 'post',
                success: function (res) {
                    var response = JSON.parse(res);
                    $("#form_act .uk-overlay .uk-spinner").remove();
                    Alert.show('Акт успешно отправлен', 'success');
                    setInterval(function () {
                        location.reload();
                    }, 2000);
                },
                error: function (e) {
                    var response = JSON.parse(e.responseText);
                    Alert.show('Что-то пошло не так, попробуйте позже.', 'danger');
                }
            });
        }
        else {
            Alert.show('Вы должны выбрать дату!','danger');
            Alert.inputMark($("#form_act .date_act"));

        }
    });

    $("body").on("click", "[data-action=pay_online]", function () {
        if ($("#agree_offer").is(":checked")){
            var data = Payment.collectingAdditionals($(this));
            /**В функцию оплаты отправлются данные (data) и url*/
            Payment.sendPay("additionals/pay", data);
        } else {
            Alert.show('Вы должны согласиться с условиями оферты', 'danger');
        }
    });

    $("body").on("click", "[data-type=pay]", function () {
        var sum = $(".balance-pay [name=sum]").val();

        /**Оплатить можно только сумму больше 1 рубля, ну и не пустую, разумеется*/
        if (sum == '') {
            Alert.show("Введите сумму оплаты!", 'danger');
        }
        else {
            if (sum < 1) {
                Alert.show("Сумма оплаты должна быть не менее 1 рубля!", 'danger');
            }
            else {
                Payment.sendPay("payments", {"sum": $(".balance-pay [name=sum]").val()});
            }
        }
    });

    /**Есть услуги, которые по каким-то причинам, были записаны в базу, но не были оплачены.
     * Эта функция переводит в оплату эти услуги*/
    $("body").on("click", "[data-type=pay_unpaid]", function () {
        if ($("[name = agree_offer]").is(":checked")){
            var data = Payment.collectingAdditionals($(this));
            /**В функцию оплаты отправлются данные (data) и url*/

            /**Работает это так же как и оплата новых, за исключением того, что информация собирается из таблицы*/
            var ids = [];
            var additionals_arr = [];
            var total_price = 0;

            $(".additionals_list tbody").find("tr[status=0]").each(function () {
                ids.push($(this).attr("additional_id"));

                var additional = {};
                additional['additional_id'] = $(this).attr("id");
                //additional['close_date'] = $(this).find(".table-close_date").text();
                additional['comment'] = $(this).find(".table-comment").text();
                additional['status'] = $(this).attr("status");

                additionals_arr.push(additional);
                total_price = total_price + parseFloat($(this).find(".table-price").text());
            });
            var data = {
                "additionals_id": ids,
                "total_price": total_price,
                "additionals": additionals_arr,
            }
            Payment.sendPay("additionals/pay-unpaid", data);
        } else {
            Alert.show('Вы должны согласиться с условиями оферты', 'danger');
        }
    });

    /**--ОПЛАТА--**/



    /**ОТОБРАЖЕНИЯ**/

    /**Подробности о счёте*/
    $("[data-type=balance]").click(function () {
        $("[data-view=balance]").addClass("active");
        $(".translate-out").addClass("active");
        $("body").addClass("uk-overflow-hidden");
    });

    /**Информация в футере.*/
    $("[data-type=information]").click(function () {
        var id = $(this).attr('id');
        /**Получается по ajax*/
        $.ajax({
            url: '/informations/' + id,
            headers: {
                'X-CSRF-TOKEN': _token
            },
            data: {
                sys_id: id,
            },
            success: function (res) {
                /** И выводится в модальном окне*/
                $(".modal_slider").html(res);
                Slide.show("modal-slider");
            }
        });
    });

    $(".password-change-form_open").click(function () {
      var userMenu = $(".user-menu_dropdown");
      userMenu.hide();
      UIkit.dropdown(userMenu).hide();

        /**Форма смены пароля генерируется на ходу и вставляется в сайдбар*/
        var content = '<div class="password-change input-form" focus-input="first">' +
            '<div class="material-content"><div class="material-input">' +
            '<label class="material-input_label" for="old_password">Старый пароль</label>' +
            '<input class="material-input_text" type="password" name="old_password" value="" >' +
            '</div>' +
            '<div class="material-input">' +
            '<label class="material-input_label" for="new_password">Новый пароль</label>' +
            '<input class="material-input_text" type="password" name="new_password" value="" >' +
            '</div>' +
            '<div class="material-input">' +
            '<label class="material-input_label" for="new_password_confirmation">Подтверждение пароля</label>' +
            '<input class="material-input_text" type="password" name="new_password_confirmation" value="" >' +
            '</div>' +
            '<div class="button-transparent" data-action="password_show"><img class="icon" src="/img/icon/view.svg"/></div></div>' +
            '<button class="button" data-action="password_change">Сменить</button></div>';
        Aside.pushContent("Смена пароля", content);
        setFocusInput();
    });

    $("body").on("click", ".shure-popup .no", function () {
        /**Скрытие формы, при отрицательном ответе*/
        UserForm.hideOverlay();
        UserForm.close(".tmp");
        $("body").removeAttr("shure-popup");
    })

    /**--ОТОБРАЖЕНИЯ--**/

    $("body").on("click", ".additionals-create_item-delete", function () {
        var item = $(this).parent();

        item.parent().remove();
        UserForm.close();

        /**Преобразование стоимости выбранной услуги к float*/
        var this_price = parseFloat(item.attr("price"));
        var total_price = $(".additionals-pay [total-price]");
        /**Вычитание текущей цены из общей стоимости и вставка новой стоимости.
         * Всё проеобразовывается в float, для валидной операции*/
        new_price = parseFloat(parseFloat(total_price.attr("total-price")) - this_price);
        total_price.attr("total-price", new_price).text(new_price + " руб");

        /**Если не осталось заказнных услуг, то установить общую стоимость в 0 и скрыть поле*/
        if ($(".additionals-create").children().length <= 1) {
            $(".additionals-pay").attr("hiden", true);
            total_price.attr("total-price", 0).text(0 + " руб");
        };
    });


    var _token = $('input[name="_token"]').val();


    /**ИЗМЕНЕНИЯ ДАННЫХ**/

    $("body").on("click", "[data-action=password_change]", function () {
        /**Отправка введённых данных на url смены пароля*/
        $.ajax({
            url: "/password/change",
            data: $(".password-change input").serialize(),
            type: 'POST',
            success: function (res) {
                Slide.hide("aside");
                Aside.clear();

                UserForm.hideOverlay();
                Alert.show(res, "success");
            },
            error: function (e) {
                Item.errorShow(e);
            }
        });
    });

    $("body").on("click", "[data-action=password_show]", function () {
        /**Отображение/скрытие пароля в полях ввода*/
        $(this).parent().find("input").each(function () {
            if ($(this).attr("type") == 'password') {
                $(this).attr("type", "text");
            }
            else if ($(this).attr("type") == 'text') {
                $(this).attr("type", "password");
            }
        });
    });


    $("body").on("click", ".phone[data-mode=life_update]", function () {
        var current = $(this);
        var item = current.attr('id');
        /**Если телефон был изменён, то отправить по url. */
        if ($("input[operand=" + item + "]").val() != $("input[operand=" + item + "]").attr('data-old')) {
            $.ajax({
                // url: 'users/confirm',
                url: 'users/phone',

                headers: {
                    'X-CSRF-TOKEN': _token
                },
                data: {
                    phone: $("input[operand=" + item + "]").val(),
                },
                type: "POST",
                success: function (res) {
                    if (res != 0) {
                        /**В ответ отобразится окно с кодом подтверждения*/
                        Alert.code();
                    }
                },
                error: function(e) {
                    var response = JSON.parse(e.responseText);
                    console.info(response);
                    //Alert.show(response.overview, 'danger');
                    Alert.show('Что то пошло не так, проверьте корректность ввода и попробуйте снвоа!', 'danger');
                }
            });
        }
        else {
            /**Если не был изменён, декативировать поля ввода*/
            Item.line_disable();
        }

        $("body").on("click", ".code_popup_close", function () {
            if ($('.code').hasClass('active')) {
                $('.code').removeClass('active');
            }
        });
    });

    $("body").on("click", "[data-action=register]", function (e) {
        e.preventDefault();
        var arr = $(".aside-container .users-create_form").serialize();
        $.ajax({
            url: '/register',
            type: 'post',
            data: arr,
            success: function (res) {
                location.reload();
            },
            error: function (e) {
                Item.errorShow(e);
            }
        })
    });

    $("body").on("click", "#email[data-mode=life_update]", function () {

        /**Если e-mail не отличается от старого, то снова сделать поля не активными*/
        if ($("input[name='email']").val() == $("input[name='email']").attr('data-old')) {
            Item.line_disable();
            return;
        } else if ($("input[name='email']").val() === '') {
          return false;
        } else {
            var item = $(this).attr('id');
            /**Вместо кнопки, подставляется div с классом uk-spinner (колесо ожидания)*/
            var button = $(this);
            /**строка ниже запоминает html кнопки,
             * чтобы после успешного обновления вернуть её в прежнее состояние*/
            var button_html = button.html();
            button.html("<div uk-spinner></div>");

            $.ajax({
                url: 'users/email',

                headers: {
                    'X-CSRF-TOKEN': _token
                },
                data: {
                    email: $("input[name=" + item + "]").val(),
                    name: $(".status").text(),
                    url: window.location.href,
                    token: _token
                },
                type: "POST",
                success: function () {
                    Alert.show("Мы отправили Вам на почту письмо подтверждения.", "success");
                    /**Возврать кнопки в прежнее состояние*/
                    button.html(button_html);
                    Item.line_disable();
                },
                error: function () {
                    Alert.show("Что-то пошло не так. Попробуйте ещё раз.", "danger");
                }
            });
        }
    });

    $("body").on("click", ".confirm_phone", function () {
        var confirmation_code = $(".code.tmp input[name=confirmation_code]");
        if (confirmation_code.val() == '') {
            Alert.inputMark(phone_request);
            confirmation_code.val("Вы ввели неверный код подтверждения.");
        } else {
            /**При вводе кода, на сервер отправляется код и новый телефон*/
            $.ajax({
                // url: 'users/selfupdate',
                url: 'users/phone-confirm',

                headers: {
                    'X-CSRF-TOKEN': _token
                },
                data: {
                    confirmation_code: confirmation_code.val(),
                    phone: $("input[name=phone]").val(),
                },
                type: "POST",
                success: function (res) {
                    if (res != 0) {
                        /**В случае совпадения кода, данные обновятся и пользователь будет уведомлён*/
                        Alert.show("Данные успешно обновлены", "success", location.reload());
                    }
                    else {
                        Alert.inputMark(confirmation_code);
                        confirmation_code.val("Вы ввели неверный код подтверждения.");
                    }
                },
                error: function(){
                    Alert.show("Данные введены неверно", "danger");

                }
            });
        }
    });

    /**--ИЗМЕНЕНИЯ ДАННЫХ--**/

    $("body").on("focus", ".material-input_text", function () {
        /**Удаление уведомлений об ошибках, по нажатию на поле ввода*/
        $(this).next("span.alert").remove();
    });


    /**ДОП.УСЛУГИ**/
    $("body").on("change", ".change_status", function () {
        /**Изменение статуса заказанной услуги (admin)*/
        var status = $(this).val();
        var id = $(this).parent().parent().attr("id");

        $(this).attr("status", null);

        $.ajax({
            url: "additionals/" + id,
            headers: {
                'X-CSRF-TOKEN': _token
            },
            data: {
                status: status,
                additional: id
            },
            type: "PUT",
            success: function (res) {
                if (res) {
                    location.reload();
                }
                else {
                    Alert.show("Обновление не удалось!", "danger");
                }
            },
            error: function (e) {
                var response = JSON.parse(e);
                Alert.show(response);
            }
        });
    });

    $("body").on("click", ".additionals-create_item.active", function () {
        /**По нажатиюб на создание новой услуги, с сервера приходит список всех услуг*/
        $.ajax({
            url: 'additionals/list',
            success: function (res) {
                var response = JSON.parse(res);

                /**и формируется html для сайдбара из них*/
                var content = '<div class="new-additionals_list uk-child-width-1-1 uk-padding uk-padding-remove-top" uk-grid>';
                for (i in response) {
                    var item = response[i];

                    content += '<div>' +
                        '<div class="new-additionals_list_item uk-flex uk-padding-small" data-color=' + item.color + ' data-id=' + item.id + ' data-price=' + item.price + '>' +
                        '<img src="/img/icon/additionals/' + item.icon + '.svg" class="new-additionals_item_icon">' +
                        '<div><div name="name">' + item.title + '</div>' +
                        '<small>' + item.description + '</small></div>' +
                        '</div>' +
                        '</div>';
                }
                content += '</div>';
                /**Укладывается в сайдбар*/
                Aside.pushContent("Дополнительные услуги", content);
                $(".aside-container").addClass("new-additionals")
                //$(".aside").addClass("wide");
            },
            error: function (e) {
                var response = JSON.parse(e);
                Alert.show(response);
            }
        })
    });

    $("body").on("click", ".new-additionals_list_item", function (e) {
        var price = $(this).data("price");
        var name = $(this).find("[name=name]").text();
        var id = $(this).data("id");

        /**Пользователь не должен заказать одну и ту же услугу*/
        var same_additionals = undefined;
        /**Если в списке услуг такая уже есть, то добавить её id в переменную same_additionals*/
        same_additionals = $(".additionals-create").find(".additionals-create_item#" + id).attr("id");
        if (same_additionals != undefined) {
            Alert.show("Услуга уже выбрана!", "danger");
            return;
        }

        /**Снятие активности с улуги в сайдбаре*/
        $(".new-additionals_list_item").removeClass("active");
        $(".new-additionals_list_item").css("background-color", "white");
        $(this).addClass("active");
        $(this).css("background-color", $(this).data("color"));

        /**Добавление значений в дочерний сайдбар*/
        $(".new-additionals_create").attr("id", id);
        $(".new-additionals_create input").val("");
        $(".aside-child [name=title]").text(name);
        $(".new-additionals_create [name=price]").text(price);
        $("body").attr("aside-child", true);
    });

    /**--ДОП.УСЛУГИ--**/


});//document.ready
