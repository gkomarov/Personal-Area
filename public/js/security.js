/**
* User custom for change input type
*/
$(".search_input").each(function(){
  if (this.getAttribute("aria-controls") === 'people-list') {
    this.setAttribute("type", "search");
  } else {
    this.setAttribute("type", "number");
  }
})
