<?php
require_once __DIR__ . '/vendor/autoload.php';

//Удаление всех файлов pdf перед созданием
$mask = '*.pdf';
array_map("unlink", glob($mask));

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];
$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/fira',
    ]),
    'fontdata' => $fontData + [
            'frutiger' => [
                'R' => 'times-new-roman.ttf',
            ]
        ],
    'default_font' => 'ctimes',
    'mode' => 'utf-8',
    'format' => [210, 297],
    'orientation' => 'L',
]);


$curl = curl_init();


/*$date->modify('- 1 month');
$start = $date->format('Y-m-01');
$end = $date->modify('last day of')->format('Y-m-d');*/

curl_setopt($curl, CURLOPT_URL, "https://lk.kp-donskoy.ru/invoices/invoice-balance");
$info = curl_exec($curl);
var_dump($info); die();
curl_close($curl);

$css = file_get_contents('pdf.css');

$mpdf->WriteHTML($css, 1);

$table = '<div>
    <table class="wide_table">
    <thead>
        <tr>
            <th colspan="9" style="text-align: center;">Расчёт платы за оказанные услуги</th></tr>
        <tr>
            <th rowspan="2">Вид услуг</th>
            <th rowspan="2">Ед.изм.</th>
            <th rowspan="2">Задолженность</th>
            <th rowspan="2">Объём услуг</th>
            <th rowspan="2">Тариф, руб</th>
            <th rowspan="2">Размер оплаты за услуги, руб</th>
            <th rowspan="2">Всего начислено за расчётный период</th>
            <th rowspan="2">Перерасчёты</th>
            <th>Итого к оплате за расчётный период</th>
        <tr>
            <th>Всего</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
        </tr>
    </thead>
    <tbody>';

$count = 0;
$sum = 0;
$start_balance = 2389.53;
foreach ($info as $item) {
    $count++;
    $sum += $item['Сумма'];
    $start_period = $item['ИНПК-НачальныеПоказания'];
    $end_period = $item['ИНПК-КонечныеПоказания'];
    $unit = $end_period - $start_period;

    $table .= '<tr>
                    <td class="left">' . $count . '. ' . $item["Содержание"] . '</td>';
    if ($unit == 0) {
        $table .= '<td class="center">руб.</td>';
    } else {
        $table .= '<td class="center">куб.м</td>';
    }

    $table .= '<td></td>';
    $table .= '<td>' . $unit . '</td>';
    $table .= '<td>' . number_format($item['Сумма'], 2, ",", "") . '</td>';
    $table .= '<td>' . number_format($item['Сумма'], 2, ",", "") . '</td>';
    $table .= '<td>' . number_format($item['Сумма'], 2, ",", "") . '</td>';
    $table .= '<td></td>';
    $table .= '<td>' . number_format($item['Сумма'], 2, ",", "") . '</td>';
    $table .= '</tr>';
}


$table .= '<tr>
            <td class="left">Итого к оплате</td>    
            <td class="center">руб.</td>
            <td><b>' . number_format($start_balance, 2, ",", "") . '</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>' . number_format($sum, 2, ",", "") . '</b></td>
           </tr>';

$sum_for_pay = number_format($sum + $start_balance, 2, ",", "");
$table .= '</tbody>
        </table>
    </div>';

$table .= '<div><b>Оплату ежемесячно производить до 25 числа месяца, следующего за месяцем оказания услуг.</b></div>
           <div><b>Показания по статье эксплуатация сетей водоснабжения/водоотведения передавать до 25 числа каждого месяца.</b></div>';

$mpdf->WriteHTML($css, 1);

$page = '
<div class="header">
    <div id="logo"><img src="login-donskoy-logo.png" width="100%"></div>
    <div style="margin-top: 100px;">Получатель</div>
    <div style="margin-top: -25px;" id="requisites">
        <div><b>Приложение №4 к договору оказания услуг №74 от "01" октября 2016 г.</b></div>
        <div><b><i>ООО "Коттеджный поселок "Донской"</i></b></div>
        <div><i>Местонахождение:</i> 346880 Ростовская область, г. Батайск, ул. М.Горького, 150, ком. 1кв</div>
        <div><i>Офис:</i> 346880 Ростовская область, Азовский район, п. Койсуг, ул. Ростовская, 2</div>
        <div>ИНН / КПП 6141050693 / 614101001 ОГРН 1166196090895 БИК 041806715</div>
        <div>Р.сч.40702810101000022896 ЮЖНЫЙ Ф-Л ПАО "ПРОМСВЯЗЬБАНК"</div>
        <div>Кор.сч. 30101810100000000715</div>
    </div>
</div>
<div style="height: 20px;">
    <div class="user_data" style="width:52%;">
        <div style="width: 100%;">
            <div>Плательщик:</div>
            <div class="user_data__item">Харитонов А.И.</div>
        </div>
        <div style="width: 100%;">
            <div>Адрес:</div>
            <div class="user_data__item">ул.Таганрогская д.1</div>
        </div>
        <div style="width: 100%;">
            <div>Номер лицевого счёта:</div>
            <div class="user_data__item">74</div>
        </div>
    </div>
</div>
<h1 class="alert">Внимание! Изменились банковские реквизиты!</h1>

<div style="width: 100%;">
    <div class="left-side">
        <div style="margin-top: -13px; text-align: center;"><b>информация о показаниях приборов учёта</b></div>
        <table class="half_table" width="100%">
            <thead>
                <tr>
                    <th rowspan="2">Виды услуг</th>
                    <th colspan="2">Показания</th>
                </tr>
                <tr>
                    <th>В пред.месяце</th>
                    <th>Текущие</th>
                </tr>
            </thead>
            <tbody>';

            foreach ($info as $item) {
                $start_period = $item['ИНПК-НачальныеПоказания'];
                $end_period = $item['ИНПК-КонечныеПоказания'];
                if ($start_period) {
                    $page .=    '<tr>
                                    <td class="left">' . $item['Содержание'] . '</td>
                                    <td>' . $start_period . '</td>
                                    <td>' . $end_period . '</td>
                                </tr>';
                }
            }
$page .=    '</tbody>
        </table>
    </div>
                
    <div class="right-side">
        <table class="half_table" width="100%">
            <thead>
            <tr>
                <th>Сумма к оплате:</th>
                <th>' . $sum_for_pay . '</th>
                <th>декабрь 2017г.</th>
            </tr>
            <tr>
              <th> </th>
              <th> </th>
              <th> </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                <td class="left">Задолженность за предыдущий период: </td>';
                if ($start_period > 0) {
                    $page .= '<td>' . $start_period . '</td>';
                } else {
                    $page .= '<td>0.00</td>';
                }
                $page .= '<td class="center">руб</td>';
                $page .= '</tr>
                                        <tr>
                                            <td class="left">Аванс на начало периода :</td>';
                if ($start_period < 0) {
                    $page .= '<td>' . $start_period . '</td>';
                } else {
                    $page .= '<td>0.00</td>';
                }
                $page .= '<td class="center">руб</td>';
                $page .= '</tr>
                            <tr>
                                <td class="left">Дата и сумма последней поступившей платы:</td>
                                <td>0,00</td>
                                <td class="center">руб</td>
                            </tr>
            </tbody>
        </table>
    </div>
</div>';

$mpdf->WriteHTML($page . $table, 2);

//$file_name = $_POST['name'].$_POST['invoice_num'].'.pdf';
$file_name = 'test.pdf';

echo $file_name;

$mpdf->Output($file_name);

?>
