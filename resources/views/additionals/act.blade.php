@php
    $count = 0;
    $sum = 0;
    include_once ('sum_in_word.php');
@endphp
<div class="act_main">
    <div class="header">
        <h2>Акт № ЛК-{{ $payment_id }} от {{ $str_custom_date }}</h2>
        <hr>
        <div style="width: 100%;">
            <div class="user_data">
                <div class="left-side width-20">Исполнитель:</div>
                <div class="right-side width-80">
                    <div><b>ООО "КП "Донской", ИНН 6141050693, 346880, Ростовская обл, Батайск г, М. Горького,</b></div>
                    <div><b>дом № 150, квартира 1 кв, р/с 40702810600200001879, в банке ПАО КБ "ЦЕНТР-ИНВЕСТ",</b></div>
                    <div><b>БИК 046015762, к/с 30101810100000000762</b></div>
                </div>
            </div>
            <br>
            <div class="user_data">
                <div class="left-side width-20">Заказчик:</div>
                <div class="right-side width-80"><b>
                        {{ $user->last_name }}
                        {{ $user->first_name }}
                        {{ $user->middle_name }}
                        ({{ $user->invoice_num }})</b>
                </div>
            </div>
            <br>
            <div class="user_data">
                <div class="left-side width-20">Основание:</div>
                <div class="right-side width-80"><b>
                        ул.{{ $user->street_name }}, {{ $user->building_number }}
                        @if ($user->apart_number != '')
                            {{",кв".$user->apart_number }}
                        @endif</b>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div>
        <table class="wide_table">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Наименование работ, услуг</th>
                    <th>Количество</th>
                    <th>Ед.</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($additionals as $item)
                {{ $count++ }}
                {{ $sum = $sum + $item->price }}
                <tr>
                    <td class="center">{{ $count }}</td>
                    <td class="left">{{ $item->title }}</td>
                    <td>1</td>
                    <td class="left">шт.</td>
                    <td>{{ $item->price }}</td>
                    <td>{{ $item->price }}</td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
    <br>
    <div class="user_data">
        <div style="text-align: right;" class="right_side"><b>Итого: {{ $sum }}</b></div>
        <div style="text-align: right;" class="right_side"><b>Без налога (НДС): -</b></div>
    </div>
    <br>
    <div>Всего оказано услуг {{ $count }}, на сумму {{ $sum }} руб.</div>
    <div><b>{{ num2str($sum) }}</b></div>
    <br>
    <div>Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания
        услуг не имеет.
    </div>
    <b>
        <hr>
    </b>
    <div class="user_data">
        <div class="left-side">
            <h2>ИСПОЛНИТЕЛЬ</h2>
            <div>директор ООО "КП Донской"</div>
            <br>
            <hr>
            <div style="text-align: center">Белая Т.А.</div>
        </div>
        <div class="right-side">
            <h2>ЗАКАЗЧИК</h2>
            <div>
                {{ $user->last_name }}
                {{ $user->first_name }}
                {{ $user->middle_name }}
                ({{ $user->invoice_num }})
            </div>
            <br>
            <hr>
        </div>
    </div>
</div>