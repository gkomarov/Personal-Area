@extends('app')

@section('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-container-large uk-padding-remove uk-margin-top">
            {{ csrf_field() }}
            <div uk-grid>
                <div class="uk-width-1-1 uk-margin-bottom uk-text-lead">Дополнительные услуги</div>
                @if($act_additionals)
                <div class="uk-width-medium@m uk-width-1-1@s">

                    <div class="uk-flex uk-flex-column uk-position-relative" id="form_act">
                        <div class="uk-text-lead uk-margin-bottom">Отправка акта</div>
                        <select class="select-default uk-select uk-margin-bottom" autocomplete="off" name="payment_id" payment_id="{{ 'payment_id' }}">
                            @foreach ($act_additionals as $act_additional)
                                <option value="{{ $act_additional->payment_id }}">{{ $act_additional->payment_id }}</option>
                            @endforeach
                        </select>
                        <input class="input-default uk-width-full uk-margin-large-bottom date_act" data-mask="pass_date" name="date" placeholder="Выберите дату" value="" />
                        <button data-action="send_act" class="button button-black">Отправить</button>
                    </div>
                </div>
                @endif
                <div class="uk-width-expand@m uk-width-1-1@s uk-overflow-auto">
                <table class="uk-table uk-table-divider" table="additionals">
                    <thead class="table-header">
                    <th>Наименование услуг</th>
                    <th>№ заказа</th>
                    <th>Статус</th>
                    <th>Цена (руб.)</th>
                    <th>Комментарий</th>
                    <th>Дата изменения</th>
                    <th>Заказчик</th>
                    </thead>
                    <tbody>
                                @php
                                    $options = [
                                        1 => "Оплаченна",
                                        2 => "В работе",
                                        3 => "Выполнена",
                                        4 => "Отменена"
                                    ];
                                @endphp
                    @foreach ($additionals as $additional)
                        <tr class="table-item" id="{{ $additional->id }}" addit_id="{{ $additional->additional_id }}">
                            <td>{{ $additional->title }}</td>
                            <td>{{ $additional->payment_id }}</td>
                            <td>

                                <select class="select-default change_status uk-select" autocomplete="off" name="status" status="{{ $additional->status }}">
                                    @foreach($options as $key => $option)
                                        @if ($additional->status <= $key)
                                            <option
                                                @if ($additional->status == $key)
                                                    selected
                                                @endif

                                                value={{ $key }}>{{ $option }}
                                            </option>
                                        @endif
                                    @endforeach
                                </select>
                            </td>
                            <td>{{ $additional->price }}</td>
                            <td>{{ $additional->comment }}</td>
                            <td>{{ $additional->updated_date }}</td>
                            <td>
                                {{ $additional->last_name }}
                                {{ $additional->first_name }}
                                {{ $additional->middle_name }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </main>
@stop
