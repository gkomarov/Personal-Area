@extends('app')

@section('content')
    <main class="uk-background-default uk-padding">
        <div id="offer" class="uk-modal-container" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <h2 class="uk-modal-title">Договор оферты</h2>
                <p>@include('additionals.offer')</p>
                <p class="uk-text-right">
                    <button class="button button-close uk-modal-close" type="button">Закрыть</button>
                </p>
            </div>
        </div>

        <div class="uk-container uk-padding-remove uk-margin-top">
            {{ csrf_field() }}
            <div class="additionals">
                <div class="title-label">Дополнительные услуги</div>
                <div class="uk-width-1-1">
                    <div class="additionals-create uk-child-width-1-3@s uk-child-width-1-4@m uk-child-width-1-5@l "
                         uk-grid>
                        <div>
                            <div class="additionals-create_item uk-position-relative uk-position-z-index uk-padding-large uk-padding-remove-right uk-padding-remove-left active">
                                <svg version="1.1" id="Capa_1" width="110px" height="110px"
                                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     x="0px" y="0px"
                                     viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
                                    <g fill="#eee">
                                        <path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26
                                            S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z"/>
                                        <path d="M38.5,25H27V14c0-0.553-0.448-1-1-1s-1,0.447-1,1v11H13.5c-0.552,0-1,0.447-1,1s0.448,1,1,1H25v12c0,0.553,0.448,1,1,1
                                            s1-0.447,1-1V27h11.5c0.552,0,1-0.447,1-1S39.052,25,38.5,25z"/>
                                    </g>
                                </svg>
                                <div>Добавить<br>услугу</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                     <form method="POST" action="https://test.3ds.payment.ru/cgi-bin/cgi_link">
                                        <input type="hidden" name="AMOUNT" value="60">
                                        <input type="hidden" name="CURRENCY" value="RUB">
                                        <input type="hidden" name="ORDER" value="1516262644">
                                        <input type="hidden" name="DESC" value="1">
                                        <input type="hidden" name="TERMINAL" value="79036934">
                                        <input type="hidden" name="TRTYPE" value="1">
                                        <input type="hidden" name="MERCH_NAME" value="donskoy">
                                        <input type="hidden" name="MERCHANT" value="790367686219999">
                                        <input type="hidden" name="EMAIL" value="ilia.kaduk@in-pk.com">
                                        <input type="hidden" name="TIMESTAMP" value="20180118080404">
                                        <input type="hidden" name="NONCE" value="cb584e44c43ed6bd0bc2d9c7e242837d">
                                        <input type="hidden" name="BACKREF" value="https://lk.kp-donskoy.ru">
                                        <input type="hidden" name="P_SIGN" value="cbf1619c20a65a78414d7a64f37634cf2d80fe57">
                                        <button type="submit">Go</button>
                                    </form> -->

                <div class="uk-margin-top uk-flex uk-flex-center@m uk-flex-right@l additionals-pay" uk-grid
                     hiden="true">
                    <div uk-grid>
                        <span class="uk-text-lead">К оплате:</span>
                        <span class="color-green uk-text-lead" total-price="0">1 000 р</span>
                        <div class="uk-flex uk-flex-center">
                            <button class="button button-transparent" data-action="save_order">Сохранить заказ</button>
                        </div>
                        <div class="color-grey uk-flex uk-flex-middle">
                            <input type="checkbox" id="agree_offer" name="agree_offer">
                            <label for="agree_offer">Я согласен с <a class="color-green" href="#offer" uk-toggle>договором оферты</a></label>
                        </div>
                        <div>
                            <button class="button" data-action="pay_online">Оплатить онлайн</button>
                        </div>
                    </div>
                </div>
                <div class="uk-divider-icon"></div>
                <div class="uk-margin uk-margin-remove-horizontal uk-flex uk-flex-between" uk-grid>
                    <div class="uk-text-lead">
                        @if(count($additionals) != 0)
                            Заказаные услуги
                        @else
                            У Вас пока нет заказанных услуг. <span class="color-gold">Самое время начать!</span>
                        @endif
                    </div>
                    @php
                        $unpaid = false;
                        $unpaid_sum = 0;
                        foreach ($additionals as $additional){
                            if ($additional['status'] == 0){
                                $unpaid_sum += $additional['price'];
                                $unpaid = true;
                            }
                        };
                    @endphp
                    @if ($unpaid == true)
                        <div uk-grid>
                            <span>Сумма неоплаченых услуг:</span>
                            <span class="color-green" unpaid-total-price="{{ $unpaid_sum }}">
                                {{ number_format($unpaid_sum,0,'',' ') }} руб.
                            </span>
                            <div class="color-grey">
                                <input type="checkbox" id="agree_offer_unpaid" name="agree_offer">
                                <label for="agree_offer_unpaid">Я согласен с <a class="color-green" href="#offer" uk-toggle>договором оферты</a></label>
                            </div>
                            <div>
                                <button data-type="pay_unpaid" class="button button-transparent">Оплатить онлайн
                                </button>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="uk-overflow-auto">
                    @if (count($additionals) != 0)
                        <table class="uk-table uk-table-divider additionals_list">
                            <thead class="table-header">
                            <th>Наименование услуги</th>
                            <th>Дата заказа</th>
                            <th>№ заказа</th>
                            <th>Статус</th>
                            <th>Цена (руб.)</th>
                            <th>Комментарий</th>
                            </thead>
                            <tbody>
                            @foreach ($additionals as $additional)
                                <tr class="table-item" id="{{ $additional->id }}"
                                    additional_id="{{ $additional->additional_id }}" status="{{ $additional->status }}">
                                    <td class="table-title">{{ $additional->title }}</td>
                                    <td class="table-updated_date">{{ $additional->updated_date }}</td>
                                    <td class="table-payment_id">{{ $additional->payment_id }}</td>
                                    @if ($additional->status == 0)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-danger">{{ "Не оплачено" }}</div>
                                        </td>
                                    @elseif ($additional->status == 1)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-warning">{{ "Оплачено" }}</div>
                                        </td>
                                    @elseif ($additional->status == 2)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-default">{{ "В работе" }}</div>
                                        </td>
                                    @elseif ($additional->status == 3)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-success">{{ "Выполнено" }}</div>
                                        </td>
                                    @elseif ($additional->status == 4)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-danger">{{ "Отменено" }}</div>
                                        </td>
                                    @elseif ($additional->status == 5)
                                        <td class="table-status">
                                            <div class="uk-label uk-label-success">{{ "Завершено" }}</div>
                                        </td>
                                    @endif
                                    {{--<td class="table-close_date">{{ $additional->close_date }}</td>--}}
                                    <td class="table-price">{{ $additional->price }}</td>
                                    <td class="table-comment">{{ $additional->comment }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table uk-table-divider table-blank additionals_list">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item table-blank__items"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-s"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-s"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-s"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            </thead>
                            <tbody>
                            @for ($i = 1; $i < 4; $i++)
                                <tr>
                                    <td>
                                        <div class="table-blank__item table-blank__items"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-s"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-s"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-s"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </main>
@stop
