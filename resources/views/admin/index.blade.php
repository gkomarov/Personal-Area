@extends ('app')


@section ('content')
    <main class="uk-background-default  uk-padding">
        <div class="uk-container uk-padding-remove uk-margin-top">
            <div class="uk-width-1-1 uk-text-lead uk-margin-bottom">Главная</div>
            <div class="" uk-grid>
                <div class="uk-width-1-3@m uk-width-1-1@s street-container">
                    <ul class="street-list uk-list">
                        @foreach ($streets as $street)
                            <a href="{{ route("buildings.index",['street'=>$street->id]) }}">
                                <li class="street-list_item @if(isset($active_street) and $street->id == $active_street) active @endif "
                                    id="{{ $street->id }}" map_id="{{ $street->map_id }}">
                                    {{ $street->name }}
                                </li>
                            </a>
                        @endforeach
                    </ul>
                    <div class="button-group">
                        <button class="button button-icon" data-mode="create">
                            <img src="img/icon/admin/plus.svg"/>Добавить дом
                        </button>
                        <button class="button button-transparent button-icon" data-mode="create-street">Новая улица
                        </button>
                    </div>
                </div>
                <div class="uk-width-2-3@m uk-width-1-1@s uk-padding uk-padding-remove-top uk-padding-remove-right uk-overflow-auto table-container">
                    <table class="uk-table uk-table-divider uk-table-hover table-border-left" table="building">
                        <thead class="table-header">
                        <th>Дом</th>
                        {{--<th class="table-header__width-30">Характеристики</th>--}}
                        <th>Владелец</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($buildings as $building)

                            <tr class="table_item" id="{{ $building->id }}">
                                <td>
                                    <div>
                                        @if ( $building->type == 'townhouse')
                                            {{ 'Таунхаус' }}
                                        @elseif ( $building->type == 'town')
                                            {{ 'Коттедж' }}
                                        @elseif ( $building->type == 'roof')
                                            {{ 'Многоэтажный дом' }}
                                        @endif

                                        @if ( $building->apart_type )
                                            ({{ $building->apart_type }})
                                        @endif
                                    </div>
                                    <div>ул.{{ $building->street_name }}, {{ $building->building_number }}
                                        @if ($building->apart_number)
                                                @if($building->floor)
                                                , этаж: {{ $building->floor }},
                                                @endif
                                             кв. {{ $building->apart_number }}
                                        @endif
                                    </div>
                                </td>

                                {{--
                            <td>
                                <div>
                                    {{ 'Площадь: ' }}{{ $building->area }}
                                    @if ( $building->space_area )
                                        {{ 'Этажей: ' }}{{ $building->floor }}
                                        <div>{{ 'Участок: ' }}{{ $building->space_area }}</div>
                                    @endif
                                </div>
                            </td>
                                --}}

                                <td>
                                    @if ( $building->last_name )
                                        <span>{{ $building->last_name }} {{ $building->first_name }} {{ $building->middle_name }}</span>
                                    @else
                                        <span></span>
                                    @endif
                                </td>

                                <td>
                                    <div class="button-group">
                                        <button class="button button-transparent button-icon" data-mode="edit"><img
                                                    src="img/icon/admin/street/edit.svg"></button>
                                        <button class="button button-transparent button-icon" data-mode="delete"><img
                                                    src="img/icon/admin/street/delete.svg"></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $buildings->appends(['street'=>$active_street])->links() }}
                </div>

                {!! Form::open(['route'=>'streets.store','class'=>'hidden-form uk-padding uk-flex uk-flex-column form_street', 'title'=>'Новая улица']) !!}
                <div class="material-input">
                    <label class="material-input_label" for="street_name">Название улицы</label>
                    <input class="material-input_text" name="street_name" id="street_name"/>
                </div>
                <button data-mode="send" type="button" class="button uk-margin-large-top">Добавить</button>
                {!! Form::close() !!}


                {!! Form::open(['route'=>'buildings.store','class'=>'hidden-form uk-padding uk-flex uk-flex-column buildings-edit_form', 'title'=>'Новый дом']) !!}
                <input type="hidden" name="id" value="" id="id">

                <div class="material-input">
                    <label class="material-input_label" for="type">Тип строения</label>
                    <select class="material-input_text" id="type" name="type" onchange="showLines(this.value)">
                        <option value="roof">Многоэтажный дом</option>
                        <option value="townhouse">Таунхаус</option>
                        <option selected value="town">Коттедж</option>
                    </select>
                </div>

                <div class="material-input" data-visible="roof">
                    <label class="material-input_label" for="apart_type">Тип квартиры</label>
                    <select class="material-input_text" id="apart_type" data-visible="roof" name="apart_type">
                        <option value="" selected></option>
                        <option value="studio">Студия</option>
                        <option value="1th">1-комнатная</option>
                        <option value="2th">2x-комнатная</option>
                        <option value="3th">3x-комнатная</option>
                        <option value="4th">4x-комнатная</option>
                    </select>
                </div>

                <div class="material-input">
                    <label class="material-input_label" for="building_number">Номер дома</label>
                    <input class="material-input_text" maxlength="7" name="building_number" id="building_number"/>
                </div>
                <div class="material-input" data-visible="roof">
                    <label class="material-input_label" for="floor">Этаж</label>
                    <input class="material-input_text" data-visible="roof" name="floor" data-input="floor" id="floor"/>
                </div>
                <div class="material-input" data-visible="roof,townhouse">
                    <label class="material-input_label" for="apart_number">Номер квартиры</label>
                    <input class="material-input_text" type="number" data-visible="roof,townhouse" name="apart_number" min="1"
                           max="150" step="1" id="apart_number"/>
                </div>

                <button type="button" data-mode="send"  class="button uk-margin-large-top">Записать</button>
                {!! Form::close() !!}

            </div>
        </div>
    </main>
@stop
