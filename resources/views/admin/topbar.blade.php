@section('topbar')
    <topbar class="tool_panel uk-position-fixed uk-flex uk-full-width uk-flex-center uk-position-z-index">

        @php
        $menu = array(
            "Дома" => "buildings",
            "Услуги" => "additionals",
            "Пользователи" => "users",
            "Пропуска" => 'permitions',
            //"Показания" => 'indications',
            "События" => 'events',
        );
        @endphp

        @foreach ($menu as $title=>$url)
            @php
                $class = strpos($_SERVER["REQUEST_URI"], $url) !== false ? " active" : "";
            @endphp
            @if ($title == 'Пропуска')
                <span class="nav_item {{ $class }}">
                    <a href="{{ $url }}">{{ $title }}</a>
                    <div uk-dropdown class="uk-padding-small">
                        <a href="{{ $url }}/list">Все пропуска</a>
                    </div>
                </span>
            @else
                <span class="nav_item {{ $class }}"><a href="/{{ $url }}" pointer="{{ $url }}">{{ $title }}</a></span>
            @endif
        @endforeach
    </topbar>
@stop