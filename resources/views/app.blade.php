<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<title>Личный кабинет КП "Донской"</title>

<head>
	<meta charset="UTF-8">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<title></title>
	<meta name="viewport" content="width=device-width initial-scale=1"/>
	<link href="https://fonts.googleapis.com/css?family=Fira+Sans:200,300,400,700&amp;subset=cyrillic" rel="stylesheet">
	<link rel="stylesheet" href="/css/template.css.php?{{filemtime('../public/css/template.less')}}"/>
	<script src="/js/lib/uikit/uikit.min.js"></script>
	<script src="/js/lib/uikit/uikit-icons.min.js"></script>
	<script src="/js/vendor/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/js/plugins/datatables-1.10.16/css/jquery.dataTables.css"/>
	<script type="text/javascript" src="/js/plugins/datatables-1.10.16/js/jquery.dataTables.js?{{filemtime('../public/js/plugins/datatables-1.10.16/js/jquery.dataTables.js')}}""></script>
	<script type="text/javascript" src="/js/plugins/datatables-1.10.16/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="/js/plugins/datatables-1.10.16/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="/js/plugins/datatables-1.10.16/js/buttons.print.min.js"></script>
	<script type="text/javascript" src="/js/plugins/datatables-1.10.16/js/jszip.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="/js/vendor/jquery.inputmask.bundle.js"></script>
	<script src="/js/vendor/picker.js"></script>
	<script src="/js/vendor/picker.date.js"></script>
	<script src="/js/vendor/picker.time.js"></script>
	<script src="/js/main.js?v=1{{filemtime('../public/js/main.js')}}"></script>

	{{--<script src="/js/plugins/ckeditor/ckeditor.js"></script>
	<script src="/js/tmp.js"></script>
	<script src='/js/plugins/moment.min.js'></script>--}}
</head>

<body>
{{ csrf_field() }}
<div class="alert"></div>


@if (Auth::check())
	<header class="uk-padding uk-padding-remove-vertical">
		<div class="header-body uk-flex-middle" uk-grid>
			<div class="uk-hidden@m uk-width-auto">

				<menu class="uk-link" href="#header-menu" uk-toggle uk-navbar-toggle-icon></menu>

				<div id="header-menu" uk-offcanvas="overlay: true">
					<div class="uk-offcanvas-bar uk-flex uk-flex-column">

						<ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
							@if (Auth::user()->role >= 3 && Auth::user()->role < 5)
								<li><a href="{{ route('home') }}">Главная</a></li>
								<li><a href="{{ route('additionals.index') }}">Услуги</a></li>
								<li><a href="{{ route('profiles.index') }}">Профиль</a></li>
								<li><a href="{{ route('permitions') }}">Пропуска</a></li>
							@endif
							<li><a id='out' href={{ route( 'logout') }}>Выход</a></li>
						</ul>

					</div>
				</div>
			</div>

			<div class="uk-width-expand uk-flex uk-flex-between uk-flex-middle">
				<logo>
					<a href='/'>ДОНСКОЙ</a>
				</logo>

				@if (Auth::user()->role >= 3 && Auth::user()->role < 5)
					<nav class="uk-visible@m">
						<ul class="uk-subnav uk-margin-remove">
<!-- 							<li><a class="color-white" href="{{ route('additionals.index') }}">Услуги</a></li>
							<li><a class="color-white" href="{{ route('permitions') }}">Пропуска</a></li> -->
						</ul>
					</nav>
				@endif

				<user class="uk-visible@s">
					<div class="user-menu" >
						@auth
							<div class='user-menu__name'>
								<div class='user-menu__name-hello'>Здравствуйте,</div>
								<div class='status' user_id="{{ Auth::id() }}"
									 role="{{ Auth::user()->role }}">{{ Auth::user()->last_name }} {{ Auth::user()->first_name }} {{ Auth::user()->middle_name }}</div>
							</div>

							<ul class='user-menu_dropdown uk-padding-remove' uk-dropdown="mode: click; pos: bottom-justify; offset: 30; animation: uk-animation-slide-top-small; duration: 300;">
								@if (Auth::user()->role >= 3 && Auth::user()->role < 5)
									<li><a href="{{ route('profiles.index') }}">Профиль</a></li>
									<li><a href="{{ route('additionals.index') }}">Услуги</a></li>
									<li class="password-change-form_open"><a>Изменить пароль</a></li>
									<li><a href="{{ route('permitions') }}">Пропуска</a></li>
								@endif
								<li><a id='out' href={{ route( 'logout') }}>Выход</a></li>
							</ul>
						@endauth
					</div>
				</user>

			</div>
		</div>
	</header>



	@if (Auth::user()->role == 4)
		<message class="danger"><a class="uk-link-reset uk-flex uk-flex-center" href="{{ route('profiles.index') }}">Для полного доступа введите эл.почту и телефон</a></message>
	@endif
@endif

@if (Auth::check())
	@if (Auth::user()->role < 3)
		@include('admin.topbar')
@section('topbar')
@show
@endif
<aside class="aside uk-width-large">
	<div style="position: relative;" class="sidebar">
		<div class="aside-content uk-full-width">
			<div class="uk-flex uk-text-lead uk-flex-between uk-padding uk-margin-bottom">
				<div class="aside-title-text"></div>
				<div data-type="aside" class="button_slide-close icon" icon="close"></div>
			</div>
			<div class="aside-container">

			</div>
		</div>
		<div class="aside-child uk-full-width">
			<div class="uk-flex uk-text-lead uk-flex-between uk-padding uk-margin-bottom">
				<div name="title">Название услуги</div>
				<div data-type="aside-child" class="button_slide-close icon"  icon="arrow"></div>
			</div>
			<div class="new-additionals_create uk-padding uk-padding-remove-vertical">
				{{--<input class="input-default uk-width-full" data-mask="date" name="close_date" placeholder="Выберите дату" value="" />--}}
				<input class="input-default uk-width-full" name="comment" placeholder="Комментарий к заказу" value="" />

				<div class="flex-line"><div class="font-medium">Цена: </div><div class="color-green font-large"><span name="price">1000 </span>руб</div></div>
				<div class="button" data-action="order">Заказать услугу</div>
			</div>
		</div>
	</div>
</aside>
@endif

<div data-type="modal_slider" class="modal_slider"></div>
@yield('content')
@if (Auth::check())
	<div class="shure-popup">
		<div class="button_close"><span></span></div>
		<article class="article">
			Вы уверены?
		</article>
		<div class="button-group">
			<button class="button yes">Да</button>
			<button class="button no">Нет</button>
		</div>
	</div>



	<footer class="footer uk-padding uk-padding-remove-vertical">

		<nav class="footer-nav uk-visible@m">
			<ul class="uk-subnav uk-margin-remove">
				<li id="footer_help" data-type="information" class="uk-padding-remove">Справочная информация</li>
				<li id="footer_member" data-type="information">Памятка</li>
				<li id="footer_official" data-type="information">Официальное уведомление</li>
				<li id="footer_confidencial" data-type="information">Конфидециальность</li>
			</ul>
		</nav>

		<copyright>
			<a href="https://www.in-pk.com/struktura-gruppy/inpk-development " target="_blank">
				<img src='/img/logo/footer_logo.svg'>
			</a>
		</copyright>

	</footer>
@endif
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47963387 = new Ya.Metrika({
                    id:47963387,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47963387" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

@if(Session::has('user_phone'))
	@php $user_phone = Session::get('user_phone') @endphp
@else
	@php $user_phone = '' @endphp
@endif

@if (Auth::check())
	<!-- BEGIN JIVOSITE CODE {literal} -->
	<script type='text/javascript'>
	(function(){ var widget_id = 'merZpoUH3q';var d=document;var w=window;function l(){
	var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
	<!-- {/literal} END JIVOSITE CODE -->

	<script type='text/javascript'>
		var name = "{{ Auth::user()->last_name . ' ' . Auth::user()->first_name . Auth::user()->middle_name }}"
	    var email = "{{ Auth::user()->email }}"

	    var user_phone = "{{ $user_phone }}"

	    function jivo_onLoadCallback() {
			jivo_api.setContactInfo({
				"name": name,
				"email": email,
				"user_phone": user_phone
			});
	    }
	</script>
@endif
</body>
</html>
