@extends('auth.app')

@section('form')
    <form method="POST" action="{{ route('login') }}" focus-input="first">
    <!-- {{ csrf_field() }} -->
        <div class="uk-text-lead">Личный кабинет</div>
        <div class="material-input">
            <label class="material-input_label" for="name">Логин</label>
            <input class="material-input_text" name="name" id="name" value="{{ old('name') }}" type="text" required/>
        </div>
        @if ($errors->has('name'))
            <span class="alert alert-include alert-include__danger">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
        <div class="material-input">
            <label class="material-input_label" for="password">Пароль</label>
            <input class="material-input_text" name="password" id="password" value="{{ old('password') }}" type="password" required/>
        </div>
        @if ($errors->has('password'))
            <span class="alert alert-include alert-include__danger">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
        <div class="uk-margin-top uk-margin-bottom color-grey uk-flex uk-flex-middle">
            <input id="remember" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}/>
            <label class="remember-checkbox"  for="remember"> Запомнить меня</label>
        </div>
        
        <div class="uk-child-width-1-1@m uk-child-width-1-2@l" uk-grid>
            <div><button type="submit" class="button button-responsive">Войти</button></div>
            <div class="uk-flex uk-flex-middle uk-flex-center"><a href="password/reset" class="button button-transparent">Восстановить пароль</a></div>
        </div>
    </form>
@stop