@extends('auth.app')

@section('form')
    <form class="login-container-form" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
        <div class="uk-text-lead">Восстановление пароля</div>
        <div class="material-input">
            <label class="material-input_label" for="email">E-mail</label>
            <input class="material-input_text" id="email" type="email" name="email" value="{{ old('email') }}" required/>
        </div>
        @if ($errors->has('email'))
            <span class="alert alert-include alert-include__danger">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @elseif (session('status'))
            <span class="alert alert-include alert-include__success">
                <strong>{{ session('status') }}</strong>
            </span>
        @endif
        <div class="uk-child-width-1-1@m uk-child-width-1-2@l uk-margin-top" uk-grid>
            <div><button type="submit" class="button button-responsive">Отправить ссылку</button></div>
            <div class="uk-flex uk-flex-middle uk-flex-center"><a href="/login" class="button button-transparent">Логин</a></div>
        </div>
    </form>
@stop