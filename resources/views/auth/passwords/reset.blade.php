@extends('auth.app')

@section('form')
    <form class="login-container-form" method="POST" action="{{ route('password.request') }}">
        {{ csrf_field() }}
        <div class="uk-text-lead">Смена пароля</div>
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="material-input">
            <label class="material-input_label" for="email">E-mail</label>
            <input class="material-input_text" id="email" type="email" name="email" value="{{ old('email') }}"
                   required/>
        </div>
        @if ($errors->has('email'))
            <span class="alert alert-include alert-include__danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
        @endif
        <div class="material-input">
            <label class="material-input_label" for="password">Пароль</label>
            <input class="material-input_text" name="password" id="password" type="password" required/>
        </div>
        @if ($errors->has('password'))
            <span class="alert alert-include alert-include__danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
        @endif
        <div class="material-input">
            <label class="material-input_label" for="password_confirm">Подтверждение пароля</label>
            <input class="material-input_text" name="password_confirmation" id="password_confirm" type="password"
                   required/>
        </div>
        @if ($errors->has('password_confirmation'))
            <span class="alert alert-include alert-include__danger">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
        @endif
        <div class="uk-child-width-1-1 uk-margin-top">
            <button type="submit" class="button button-responsive">Сменить пароль</button>
        </div>
    </form>
@stop