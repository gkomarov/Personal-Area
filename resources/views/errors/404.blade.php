@extends('errors.app')
@section('content')
    <main class="uk-background-default uk-padding uk-flex uk-flex-middle">
        <div class="uk-container uk-padding-remove ">
            <div class="uk-heading-primary uk-text-center uk-text-muted">Такой страницы не существует</div>
        </div>
    </main>
@stop