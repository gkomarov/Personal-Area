<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<title>Личный кабинет КП "Донской"</title>

<head>
    <meta charset="UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width initial-scale=1"/>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:200,300,400,700&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="/css/template.css.php?{{filemtime('../public/css/template.less')}}"/>
    <script src="/js/lib/uikit/uikit.min.js"></script>
    <script src="/js/lib/uikit/uikit-icons.min.js"></script>
    <script src="/js/vendor/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="/js/vendor/jquery.inputmask.bundle.js"></script>
</head>
<header class="uk-padding uk-padding-remove-vertical">
    <div class="header-body uk-flex-middle" uk-grid>
        <div class="uk-hidden@m uk-width-auto">

            <menu class="uk-link" href="#header-menu" uk-toggle uk-navbar-toggle-icon></menu>

            <div id="header-menu" uk-offcanvas="overlay: true">
                <div class="uk-offcanvas-bar uk-flex uk-flex-column"></div>
            </div>
        </div>


        <div class="uk-width-expand uk-flex uk-flex-between uk-flex-middle">
            <logo><a href='/'>ДОНСКОЙ</a></logo>
        </div>
    </div>
</header>

<body>
@yield('content')
<footer class="footer uk-padding uk-padding-remove-vertical">

    <copyright>
        <a href="https://www.in-pk.com/struktura-gruppy/inpk-development " target="_blank">
            <img src='/img/logo/footer_logo.svg'>
        </a>
    </copyright>

</footer>

</body>
</html>