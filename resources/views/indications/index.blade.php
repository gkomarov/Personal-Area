@extends("app")

@section("content")
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-padding-remove uk-margin-top">
            {{ csrf_field() }}
            <div class="uk-width-1-1 uk-text-lead  uk-margin-bottom">Обслуживание</div>
            <div uk-grid>
                <div class="uk-width-1-1">
                    <div uk-grid>

                        <div class="uk-width-1-3@m uk-width-1-1@s street-container">
                            <ul class="street-list uk-list">
                                @foreach ($streets as $street)
                                    <a href="{{ route("indications.index",['street'=>$street->id]) }}">
                                        <li class="street-list_item @if(isset($active_street) and $street->id == $active_street) active @endif "
                                            id="{{ $street->id }}" map_id="{{ $street->map_id }}">
                                            {{ $street->name }}
                                        </li>
                                    </a>
                                @endforeach
                            </ul>
                            <button class="button button-icon" data-mode="create">
                                <img src="img/icon/admin/plus.svg">Добавить показания
                            </button>
                        </div>

                        <div class="uk-width-2-3@m uk-width-1-1@s uk-padding uk-padding-remove-top uk-padding-remove-right uk-overflow-auto indication_show indication">

                            <table class="uk-table uk-table-divider uk-table-hover table-border-left" table="indication">
                                <thead class="table-header">
                                    <tr>
                                        <th>Пользователь</th>
                                        <th>Адрес</th>
                                        <th>Дата</th>
                                        <th>Электроэнергия</th>
                                        <th>Водоснабжение</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($indications as $indication)
                                    <tr class="table-item indication_item" id="{{ $indication->id }}"
                                        user_id="{{ $indication->user_id }}">
                                        <td>{{ $indication->last_name }} {{ $indication->first_name }} {{ $indication->middle_name }}</td>
                                        <td>ул.{{ $indication->street_name }}, {{ $indication->building_number }}
                                                @if ($indication->apart_number)
                                                    {{'кв. '}}{{ $indication->apart_number }}
                                                @endif
                                        </td>
                                        <td>{{ $indication->updated_date }}</td>
                                        @if ($indication->energy and $indication->water)
                                            <td name="energy" type="text">{{ $indication->energy }}</td>
                                            <td name="water" type="text">{{ $indication->water }}</td>
                                            <td>
                                                <div class="button-group">
                                                    <button class="button button-transparent button-icon" data-mode="edit"><img
                                                                src="img/icon/admin/street/edit.svg"></button>
                                                </div>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    {!! Form::open(['route'=>'indications.store','class'=>'hidden-form uk-padding uk-flex uk-flex-column indications-edit_form', 'title'=>'Показания пользователя']) !!}
    <input type="hidden" name="id" value="" id="id">

    <div id="indication-user_address">
        <div class="uk-text-lead">
            <span name="last_name"></span>
            <span name="first_name"></span>
            <span name="middle_name"></span>
        </div>
        <div class="uk-text-lead">
            <span name="street_name"></span>
            <span name="building_number"></span>
            <span name="apart_number"></span>
        </div>
    </div>

    <div class="material-input indication-user_list" data-view="4">
        <label for="building_id" class="material-input_label"></label>
        <select id="building_id" class="material-input_text" type="text" name="user_id">
            @foreach ($users_without_indications as $user)

                <option value="{{ $user->id }}">
                    {{ $user->last_name }}
                    {{ $user->first_name }}
                    {{ $user->middle_name }}
                </option>
            @endforeach
        </select>
    </div>

    <div class="material-input">
        <label class="material-input_label" for="water">Вода</label>
        <input class="material-input_text" name="water" id="water"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="energy">Электроэнергия</label>
        <input class="material-input_text" name="energy" id="energy"/>
    </div>

    <button type="button" data-mode="send"  class="uk-margin-large-top button">Записать</button>
    {!! Form::close() !!}

@stop