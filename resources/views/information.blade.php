
<div class="grid-container">
    @foreach($information as $item)
        <article>
            <h3 class="article__title">{!! $item->header !!}<div data-type="modal-slider" class="button_slide-close icon" icon="close"></div></h3> {!! $item->article !!}
        </article>
    @endforeach
</div>