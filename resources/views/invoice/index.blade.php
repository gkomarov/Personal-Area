@extends('app')

@section('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-padding-remove uk-margin-top">
            {{ csrf_field() }}
            <div class="uk-flex-column">
                    @php
                        $month = [
                            "01"=>"Январь",
                            "02"=>"Февраль",
                            "03"=>"Март",
                            "04"=>"Апрель",
                            "05"=>"Май",
                            "06"=>"Июнь",
                            "07"=>"Июль",
                            "08"=>"Август",
                            "09"=>"Сентябр",
                            "10"=>"Октябрь",
                            "11"=>"Ноябрь",
                            "12"=>"Декабрь",
                        ];

                        $year = [
                            "2017" => "2017",
                            "2018" => "2018",
                        ];
                        $sum = 0;
                    @endphp
                <div>
                    <div class="link-back uk-padding uk-padding-remove-horizontal color-grey"><a href="/"><img src="/img/icon/back.svg"> <span>Главная</span></a></div>
                    <div  uk-grid>
                    <form method="GET" action="/invoices/period" class="filter invoices-filter uk-width-1-1@s uk-width-1-2@m">
                        <div class="uk-child-width-1-4@m uk-child-width-1-1@s uk-flex uk-flex-middle" uk-grid>
                        <div class="uk-text-lead">Фильтр</div>
                        <div><select id="month" class="filter__item select-default">
                            @foreach($month as $key => $value)
                                <option @if ($key == $period['start']['month']) selected @endif value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select></div>
                        <div><select id="year" class="filter__item select-default">
                            @foreach($year as $key => $value)
                                <option @if ($key == $period['start']['year']) selected @endif value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select></div>
                        <input type='hidden' name='start' value='{{ $period['start']['timestamp'] }}' />
                        <div><button class="button usr-padding-xs uk-full-width" type="submit" data-action="filter">Показать</button></div>
                        </div>
                    </form>
                    </div>
                </div>
                @if ($invoices)

                <div class="uk-flex uk-flex-middle uk-text-lead uk-flex-start uk-margin-top">Счёт № {{ $invoices->Number }} на <span class="uk-margin-left color-grey font-small">{{ Carbon\Carbon::parse($invoices->Date)->format('d.m.Y') }}</span></div>
                <div class="uk-overflow-auto uk-margin-top">
                    <table class="uk-table table-border-left" table="invoice-story">
                        <thead class="table-header">
                            <th>Наименование</th>
                            <th>Ед.изм.</th>
                            <th>Объём</th>
                            <th>Тариф</th>
                            <th>Сумма</th>
                        </thead>
                        <tbody>
                        @foreach($invoices->Услуги as $invoice)
                            @php
                                    $sum += $invoice->Сумма;
                                    $unit = "руб.";
                                    $indications = $invoice->ИНПК_КонечныеПоказания - $invoice->ИНПК_НачальныеПоказания;
                                    if ($indications != 0){
                                        $unit = "куб.м.";
                                    }
                            @endphp
                            <tr class="table_item">
                                <td><div>{{ $invoice->Содержание }}</div></td>
                                <td><div class="color-grey">{{ $unit }}</div></td>
                                <td><div class="color-grey"><span class="uk-text-lead">@if ( $invoice->ИНПК_КонечныеПоказания != 0) {{ $indications }} @else {{ "" }}@endif</span></div></td>
                                <td><div><span class="uk-text-lead">@isset($invoice->Цена) {{ $invoice->Цена }} @endisset @empty($invoice->Цена) {{ "" }} @endempty</span><span class="color-grey"> {{ "руб." }}</span></div></td>
                                <td><div><span class="uk-text-lead">@isset($invoice->Сумма) {{ $invoice->Сумма }} @endisset @empty($invoice->Сумма) {{ "" }} @endempty</span><span class="color-grey"> {{ "руб." }}</span></div></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><div class="uk-text-lead">Итого:</div></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><div><span class="uk-text-lead color-green">{{ $sum }}</span><span class="color-grey"> руб.</span></div></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                @else
                    <div class="invoices uk-margin-top uk-width-1-1">
                        <div class="uk-text-lead uk-text-muted uk-flex-start uk-padding-small uk-padding-remove-horizontal">Данных нет </div>
                        <table class="uk-table uk-table-divider table-blank">
                            <thead class="table-header">
                                <th>
                                    <div class="table-blank__item table-blank__items"></div>
                                </th>
                                <th>
                                    <div class="table-blank__item table-blank__item-m"></div>
                                </th>
                                <th>
                                    <div class="table-blank__item table-blank__item-m"></div>
                                </th>
                                <th>
                                    <div class="table-blank__item table-blank__item-m"></div>
                                </th>
                                <th>
                                    <div class="table-blank__item table-blank__item-m"></div>
                                </th>
                            </thead>
                            <tbody>
                            @for ($i = 1; $i < 4; $i++)
                                <tr>
                                    <td>
                                        <div class="table-blank__item table-blank__items"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>

        </div>
    </main>
@stop