@php
    $count = 0;
    $sum = 0;
@endphp
<div class="header">
    <div id="logo"><img src="img/logo/login-donskoy-logo.png" width="100%"></div>
    <div style="margin-top: 100px;">Получатель</div>
    <div style="margin-top: -25px;" id="requisites">
        <div><b>Приложение №4 к договору оказания услуг №{{ $balance->IdAccount }} от {{ $balance->DateAccount }}</b></div>
        <div><b><i>ООО "Коттеджный поселок "Донской"</i></b></div>
        <div><i>Местонахождение:</i> 346880 Ростовская область, г. Батайск, ул. М.Горького, 150, ком. 1кв</div>
        <div><i>Офис:</i> 346880 Ростовская область, Азовский район, п. Койсуг, ул. Ростовская, 2</div>
        <div>ИНН / КПП 6141050693 / 614101001 ОГРН 1166196090895 БИК 041806715</div>
        <div>Р.сч.40702810101000022896 ЮЖНЫЙ Ф-Л ПАО "ПРОМСВЯЗЬБАНК"</div>
        <div>Кор.сч. 30101810100000000715</div>
    </div>
</div>
<div style="height: 20px;">
    <div class="user_data" style="width:52%;">
        <div style="width: 100%;">
            <div>Плательщик:</div>
            <div class="user_data__item">{{ $balance->Name }}</div>
        </div>
        <div style="width: 100%;">
            <div>Адрес:</div>
            <div class="user_data__item">
                ул.{{ $building->street_name }}, {{ $building->building_number }}
                @if ($building->apart_number != '')
                    {{",кв".$building->apart_number }}
                @endif
            </div>
        </div>
        <div style="width: 100%;">
            <div>Номер лицевого счёта:</div>
            <div class="user_data__item">{{ $balance->IdAccount }}</div>
        </div>
    </div>
</div>
<h1 class="alert">Внимание! Изменились банковские реквизиты!</h1>

<div style="width: 100%;">
    <div class="left-side">
        <div style="margin-top: -13px; text-align: center;"><b>информация о показаниях приборов учёта</b></div>
        <table class="half_table" width="100%">
            <thead>
            <tr>
                <th rowspan="2">Виды услуг</th>
                <th colspan="2">Показания</th>
            </tr>
            <tr>
                <th>В пред.месяце</th>
                <th>Текущие</th>
            </tr>
            </thead>
            <tbody>

            @foreach ($invoice->Услуги as $item)
                @php
                    $sum += $item->Сумма;
                    $start_period = $item->ИНПК_НачальныеПоказания;
                    $end_period = $item->ИНПК_КонечныеПоказания;
                @endphp
                @if ($start_period) {
                <tr>
                    <td class="left">{{ $item->Содержание }}</td>
                    <td>{{ $start_period }}</td>
                    <td>{{ $end_period }}</td>
                </tr>
                @endif
            @endforeach
            @php
                $fine = (float) $invoice->ИНПК_НачисленоПени;
                // $last_pay = ($balance->StartBalance + $sum) - $balance->EndBalance;
                $last_pay = $balance->Outcome;
                $sum_for_pay = ($sum + $balance->StartBalance + $fine) - $last_pay;
            @endphp
            <tr>
                <td class="left">Нормативное потребление</td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="right-side">
        <table class="half_table" width="100%">
            <thead>
            <tr>
                <th>Сумма к оплате:</th>
                <th>{{ number_format($sum_for_pay, 2, ",", "") }}</th>
                <th>{{ $str_custom_date }}</th>
            </tr>
            <tr>
                <th> </th>
                <th> </th>
                <th> </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="left">Задолженность за предыдущий период: </td>
                @if ($balance->StartBalance >= 0) {
                    <td>{{ number_format($balance->StartBalance, 2, ",", "") }}</td>
                @else
                    <td>0.00</td>
                @endif
                <td class="center">руб</td>
                </tr>
            <tr>
                <td class="left">Аванс на начало периода :</td>
                @if ($balance->StartBalance < 0)
                    <td>{{ number_format($balance->StartBalance, 2, ",", "") }}</td>
                @else
                    <td>0.00</td>
                @endif
                    <td class="center">руб</td>
                </tr>
            <tr>
                <td class="left">Дата и сумма последней поступившей платы:</td>
                @if ($last_pay >= 0) {
                    <td>{{ number_format($last_pay, 2, ",", "")  }}</td>
                @else
                    <td>0.00</td>
                @endif
                <td class="center">руб</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div>
    <table class="wide_table">
        <thead>
        <tr>
            <th colspan="9" style="text-align: center;">Расчёт платы за оказанные услуги</th></tr>
        <tr>
            <th rowspan="2">Вид услуг</th>
            <th rowspan="2">Ед.изм.</th>
            <th rowspan="2">Задолженность</th>
            <th rowspan="2">Объём услуг</th>
            <th rowspan="2">Тариф, руб</th>
            <th rowspan="2">Размер оплаты за услуги, руб</th>
            <th rowspan="2">Всего начислено за расчётный период</th>
            <th rowspan="2">Перерасчёты</th>
            <th>Итого к оплате за расчётный период</th>
        <tr>
            <th>Всего</th>
        </tr>
        <tr>
            <th>1</th>
            <th>2</th>
            <th>3</th>
            <th>4</th>
            <th>5</th>
            <th>6</th>
            <th>7</th>
            <th>8</th>
            <th>9</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($invoice->Услуги as $item)
        @php
            $count++;
            $start_period = $item->ИНПК_НачальныеПоказания;
            $end_period = $item->ИНПК_КонечныеПоказания;
            $indication = $end_period - $start_period;
        @endphp
        <tr>
            <td class="left">{{ $count }} . {{ $item->Содержание }}</td>
            @if ($indication == 0)
                <td class="center">руб.</td>
            @else
                <td class="center">куб.м</td>
            @endif

            <td></td>
            <td>@if($indication != 0){{ $indication }}@endif</td>
            <td>{{ number_format($item->Цена, 2, ",", "") }}</td>
            <td>{{ number_format($item->Сумма, 2, ",", "") }}</td>
            <td>{{ number_format($item->Сумма, 2, ",", "") }}</td>
            <td></td>
            <td>{{ number_format($item->Сумма, 2, ",", "") }}</td>
        </tr>
        @endforeach

        <tr>
          <td class="left">{{ $count++ }}.Пеня</td>
          <td class="center">руб.</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{ $invoice->ИНПК_НачисленоПени }}</td>
        </tr>
        <tr>
            <td class="left">Итого к оплате</td>
            <td class="center">руб.</td>
            <td><b>@if ($balance->StartBalance > 0) {{ number_format($sum_for_pay, 2, ",", "") }} @endif</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>{{ number_format($sum + $invoice->ИНПК_НачисленоПени, 2, ",", "") }}</b></td>
        </tr>
        </tbody>
    </table>
</div>
<div><b>Оплату ежемесячно производить до 25 числа месяца, следующего за месяцем оказания услуг.</b></div>
<div><b>Показания по статье эксплуатация сетей водоснабжения/водоотведения передавать до 25 числа каждого месяца.</b></div>
