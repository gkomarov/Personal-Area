@extends('app')

@section('content')
    <main>
        <div class="grid-body">
            {{ csrf_field() }}
            <div class="link-back color-grey"><a href="/"><img src="/img/icon/back.svg"> <span>Главная</span></a></div>
            <div class="title-label">Лицевой счет № 117</div>
            <div class="grid-content">
                <div class="detalization">

                    {{--<div class="text-info">
                        Ежемесячный платёж <span>{{ number_format(abs($invoices->payment),0,'',' ') }}руб.</span>
                    </div>--}}
                    <div class="detalization-item">
                        <div>Баланс <span class="color-grey font-small">на {{ date('1.m.Y') }}</span></div>
                        <div><span class="font-medium">{{ -4500 }}</span><span class="color-grey"> руб.</span></div>
                    </div>
                    <div class="detalization-item">
                        <div>Начислено</div>
                        <div><span class="font-medium">{{ -3300 }}</span><span class="color-grey"> руб.</span></div>
                    </div>
                    <div class="detalization-item">
                        <div>Начислено</div>
                        <div><span class="font-medium">{{ 3600 }}</span> <span class="color-grey"> руб.</span></div>
                    </div>
                    <div class="detalization-item">
                        <div>Текущий баланс <span class="color-grey font-small">на {{ date('d.m.Y') }}</span></div>
                        <div class="color-green"><span class="font-medium">{{ -4200 }}</span> руб.</div>
                    </div>

                <!--<a href="{{ route('pay.index') }}" class="button button-color button-round-small">Подробнее о счёте</a>-->
                </div>
                <div class="fix-content-medium">
                    <div class="solid-box detalization-pay">
                        <div class="font-large">Пополнение счёта</div>
                        <input class="input-default" name="pay_sum" placeholder="Сумма пополнения"/>
                        <button class="button" data-type="pay">Оплатить онлайн</button>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop