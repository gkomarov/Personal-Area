@extends ('app')

@section ('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-padding-remove uk-margin-top">
            <div class="uk-width-1-1">
                <div class="uk-text-lead">Временные пропуска</div>
            </div>

            <hr class="uk-divider-icon">

            <div class="uk-grid-small@s" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-1@s">
                    <div class="uk-flex uk-flex-between uk-flex-middle">

                        <div class="uk-text-lead">
                            @if(count($peoples) != 0)
                                Пешеходы
                            @else
                                <span class="color-grey">Вы ещё не добавили
                                    <span class="color-gold"> пешеходов</span>
                                </span>
                            @endif
                        </div>
                        @if (Auth::user()->role != 4)
                            <button class="button button-icon button-fit add_people" form="peoples" data-mode="create">
                                <img src="/img/icon/admin/plus.svg"/>Добавить
                            </button>
                        @endif
                    </div>

                    @if(count($peoples) != 0)
                        <table class="uk-table table-permition" table="peoples">
                            <thead class="table-header">
                            <th>Действие</th>
                            <th>фИО</th>
                            <th>Действителен</th>
                            <th>Примечание</th>
                            @if (Auth::user()->role < 3)
                                <th>Создал</th>
                            @endif
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach ($peoples as $people)
                                <tr class="table-item people_item" id="{{ $people->id }}">
                                    <td>
                                        @if (Auth::user()->role != 4)
                                            <div class="uk-flex">
                                                <button class="button-transparent button-icon" action="peoples"
                                                        data-mode="delete">
                                                    <img src="/img/icon/admin/street/delete.svg">
                                                </button>
                                                <button class="button-transparent button-icon" action="peoples"
                                                        data-mode="edit">
                                                    <img src="/img/icon/edit.svg">
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div>{{ $people->last_name }}</div>
                                        <div>{{ $people->first_name }}</div>
                                        <div>{{ $people->middle_name }}</div>
                                    </td>
                                    <td>
                                        @if ($people->personal != 1)
                                            {{ $people->close_date }}
                                            @if ($people->close_count >= 0)
                                                <span class="color-green"> ({{ $people->close_count }}д.)</span>
                                            @else
                                                <span class="color-red">(истёк)</span>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        <div>{{ $people->note }}</div>
                                    </td>
                                    @if (Auth::user()->role < 3)
                                        <td>
                                            <div>{{ $people->street_name }}<span>{{ $people->building_number }}</span>
                                            </div>
                                            <div>{{ $people->user_last_name }} {{ $people->user_first_name }} {{ $people->user_middle_name }}</div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table table-permition table-blank" table="peoples">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            </thead>
                            <tbody>
                                @for ($i = 1; $i < 4; $i++)
                                    <tr>
                                        <td>
                                            <div class="table-blank__item"></div>
                                        </td>
                                        <td>
                                            <div class="table-blank__item table-blank__item-m"></div>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    @endif
                </div>

                <div class="uk-width-1-2@m uk-width-1-1@s">
                    <div class="uk-flex uk-flex-between uk-flex-middle">
                        <div class="uk-text-lead">
                            @if(count($cars) != 0)
                                Автомобили
                            @else
                                <span class="color-grey">Вы ещё не добавили
                                    <span class="color-gold"> автомобили</span>
                                </span>
                            @endif
                        </div>
                        @if (Auth::user()->role != 4)
                            <button class="button button-icon button-fit" form="cars" data-mode="create">
                                <img src="/img/icon/admin/plus.svg"/>Добавить
                            </button>
                        @endif
                    </div>

                    @if(count($cars) != 0)
                        <table class="uk-table table-permition" table="cars">
                            <thead class="table-header">
                            <th>Дейстивие</th>
                            <th>Автомобиль</th>
                            <th>Действителен</th>
                            <th>Примечание</th>
                            @if (Auth::user()->role < 3)
                                <th>Создал</th>
                            @endif
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach($cars as $car)
                                <tr class="table-item auto_item" id="{{ $car->id }}">
                                    <td>
                                        @if (Auth::user()->role != 4)
                                            <div class="uk-flex">
                                                <button class="button-transparent button-icon" action="cars"
                                                        data-mode="delete">
                                                    <img src="/img/icon/admin/street/delete.svg"></button>

                                                <button class="button-transparent button-icon" action="cars"
                                                        data-mode="edit">
                                                    <img src="/img/icon/edit.svg"></button>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        {{ $car->number }}
                                        <div class="table-align-left car_name">{{ $car->model }}</div>
                                        <div class="table-align-left car_name">{{ $car->mark }}</div>
                                    </td>
                                    <td>
                                        @if ($car->personal != 1)
                                            {{ $car->close_date }}
                                            @if ($car->close_count >= 0)
                                                <span class="color-green"> ({{ $car->close_count }}д.)</span>
                                            @else
                                                <span class="color-red">(истёк)</span>
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{ $car->note }}</td>
                                    @if (Auth::user()->role < 3)
                                        <td>
                                            <div>{{ $car->street_name }}<span>{{ $car->building_number }}</span></div>
                                            <div>{{ $car->last_name }} {{ $car->first_name }} {{ $car->middle_name }}</div>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table table-permition table-blank" table="cars">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item table-blank__item-s"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            </thead>
                            <tbody>
                            @for ($i = 1; $i < 4; $i++)
                                <tr>
                                    <td>
                                        <div class="table-blank__item table-blank__item-s"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item-m"></div>
                                        <div class="table-blank__item"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
        </div>
    </main>
    @include('permition.perform')
@stop
