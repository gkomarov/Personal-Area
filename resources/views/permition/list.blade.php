@extends ('app')

@section ('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-margin-top uk-padding-remove ">
            <div class="uk-width-1-1">
                <div class="uk-heading-primary">Пропуска</div>
            </div>
            <div class="uk-grid-small@s uk-margin-top" uk-grid>
                <div class="uk-width-1-1@">
                    <ul uk-tab uk-switcher>
                        <li class="uk-text-lead uk-active"><a href="#">Автомобили</a></li>
                        <li class="uk-text-lead"><a href="#">Гости</a></li>
                    </ul>
                    <ul class="uk-switcher uk-margin">
                        <li>
                            <div class="uk-overflow-auto">
                            <table class="uk-table uk-table-divider uk-table-hover uk-margin-top uk-margin-bottom display" id="car-list">
                                <thead>
                                <tr>
                                    <th>Номер</th>
                                    <th>Марка</th>
                                    <th>Модель</th>
                                    <th>Статус</th>
                                </tr>
                                </thead>
                            </table>
                            </div>
                        </li>
                        <li>
                            <div class="uk-overflow-auto">
                            <table class="uk-table uk-table-divider uk-table-hover uk-margin-top uk-margin-bottom" id="people-list">
                                <thead>
                                <tr>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Примечание</th>
                                    <th>Улица</th>
                                    <th>Адрес</th>
                                    <th>Статус</th>
                                </tr>
                                </thead>
                            </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>
    <script>
        var cars = @php echo $cars; @endphp ;
        var peoples = @php echo $peoples; @endphp ;
        var lastIds = @php echo $lastIds; @endphp ;
        var car_list = [];
        var people_list = [];

        for (var i in cars) {
            switch (cars[i]['personal']) {
              case 1:
                cars[i]['personal'] = 'Житель';
                break;
              case 0:
                cars[i]['personal'] = 'Гость';
                break;
              case undefined:
                cars[i]['personal'] = 'Житель';
                break;
            }

            car_list.push([cars[i]['number'],
                            cars[i]['mark'],
                            cars[i]['model'],
                            cars[i]['personal']
                            ]);
        }

        for (var i in peoples) {
            switch (peoples[i]['personal']) {
              case 1:
                peoples[i]['personal'] = 'Житель';
                break;
              case 0:
                peoples[i]['personal'] = 'Гость';
                break;
              case undefined:
                peoples[i]['personal'] = 'Житель';
                break;
            }

            people_list.push([peoples[i]['last_name'],
                            peoples[i]['first_name'],
                            peoples[i]['note'],
                            peoples[i]['street_name'],
                            peoples[i]['building_number'],
                            peoples[i]['personal']
                            ]);
        }

        $("#car-list").DataTable({
            "dom": 'Bftrip',
            "data": car_list,
            "buttons": [
                {
                    text: 'Excel',
                    className: 'button button-default',
                    filename: 'Пропуска "КП Донской"',
                    extend: 'excel',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },],
            "language": {
                "lengthMenu": "Показать _MENU_ записей на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Страница _PAGE_ из _PAGES_",
                "infoEmpty": "Нет доступных записей",
                "search": "Поиск:",
                "paginate": {
                    "first":      "Первая",
                    "last":       "Последняя",
                    "next":       "Следующая",
                    "previous":   "Предыдущая"
                },
                "infoFiltered": "(Отфильтровано из _MAX_ событий)"
            },
        });

        $("#people-list").DataTable({
            "dom": 'Bftrip',
            "data": people_list,
            "buttons": [
                {
                    text: 'Excel',
                    className: 'button button-default',
                    filename: 'Пропуска "КП Донской"',
                    extend: 'excelHtml5',
                    exportOptions: {
                        modifier: {
                            page: 'all'
                        }
                    }
                },],
            "language": {
                "lengthMenu": "Показать _MENU_ записей на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Страница _PAGE_ из _PAGES_",
                "infoEmpty": "Нет доступных записей",
                "search": "Поиск:",
                "paginate": {
                    "first":      "Первая",
                    "last":       "Последняя",
                    "next":       "Следующая",
                    "previous":   "Предыдущая"
                },
                "infoFiltered": "(Отфильтровано из _MAX_ событий)"
            },
        });

        setInterval(function() {
            $.ajax({
                url: 'https://lk.kp-donskoy.ru/permitions/list/check',

                data:{
                    car: lastIds['car'],
                    people: lastIds['people'],
                },

                success: function(res){
                    if (res.newPermitions){
                        location.reload();
                    }
                },

                error: function(e) {
                    console.log('error', e);
                }
            })}, 50000);
    </script>
    <script src="/js/security.js?{{filemtime('../public/js/security.js')}}"></script>
@stop
