<form action="{{ route('peoples.store') }}" class="hidden-form uk-padding uk-flex uk-flex-column peoples-edit_form" focus-input="first"
      title="Новый пропуск" table="peoples">
    {{ csrf_field() }}
    <input name="id" type="hidden" value=""/>
    <div class="material-input">
        <label class="material-input_label" for="last_name">Фамилия</label>
        <input class="material-input_text" name="last_name" id="last_name" data-input="capitalize" lang="ru" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="first_name">Имя</label>
        <input class="material-input_text" name="first_name" id="first_name" data-input="capitalize" lang="ru" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="middle_name">Отчество</label>
        <input class="material-input_text" name="middle_name" id="middle_name" data-input="capitalize" lang="ru" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="close_date">Срок действия</label>
        <input class="material-input_text" name="close_date" data-mask="permition" required/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="note">Примечание</label>
        <input class="material-input_text" name="note" id="note" lang="ru" type="text"/>
    </div>
    <input class="material-input_text" name="birthday" type="hidden" value="0000-00-00"/>
    <button class="uk-margin-large-top button" type="button" data-mode="send">Оформить</button>
</form>

<form action="{{ route('cars.store') }}" class="hidden-form uk-padding uk-flex uk-flex-column cars-edit_form" focus-input="first"
      title="Новый пропуск" table="cars">
    {{ csrf_field() }}
    <input name="id" type="hidden" value=""/>
    <input name="databaseId" type="hidden" value="4"/>

    <div class="number-template uk-grid-small uk-margin-medium-top" uk-grid>
        <div class="uk-width-auto">
            <select class="number-template__mask" name="number_mask">
              <option value="russian">RU</option>
              <option value="dpr">DPR</option>
              <option value="ukrainian">UA</option>
              <option value="armenian">AM</option>
            </select>
        </div>
        <div class="uk-width-expand">
            <div class="number-template__input material-input">
                <label class="material-input_label" for="number">Номер машины</label>
                <!-- data-mask="length_limit" -->
                <input class="material-input_text number_car" name="number" id="number" error-place="number-template" data-input="uppercase" data-mask="russian" nospace required/>
            </div>
        </div>
        <div class="uk-width-auto">
            <div class="number-template__item" template="russian" default="A000AA00" style="display: block;">A000AA00</div>
            <div class="number-template__item" template="dpr" default="A000AA">A000AA</div>
            <div class="number-template__item" template="ukrainian" default="AA0000AA">AA0000AA</div>
            <div class="number-template__item" template="armenian" default="00AA000">00AA000</div>
        </div>
    </div>

    <div class="material-input">
        <label class="material-input_label" for="mark">Марка</label>
        <input class="material-input_text" name="mark" id="mark" data-input="uppercase" nospace required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="model">Модель</label>
        <input class="material-input_text" name="model" id="model" data-input="uppercase" nospace type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="close_date">Срок действия</label>
        <input class="material-input_text" name="close_date" data-mask="permition" required/>
    </div>
        <div class="material-input">
        <label class="material-input_label" for="note">Примечание</label>
        <input class="material-input_text" name="note" id="note" lang="ru" type="text"/>
    </div>
    <button class="uk-margin-large-top button" type="button" data-mode="send">Оформить</button>
</form>
