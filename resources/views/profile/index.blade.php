@extends ('app')

@section ('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-padding-remove ">
            {{ csrf_field() }}
            <div class="code">
                <div class="button_close"><span></span></div>
                <article>Введите код</article>
                <input class="input-default uk-width-1-1" name="confirmation_code" data-input="numeric" value="" required/>
                <div class="button-group uk-margin-top">
                    <button class="button confirm_phone">Подтвердить</button>
                    <button class="button button_close">Отмена</button>
                </div>
            </div>

            <div class="profile-container uk-grid-small@s">
                <div class="uk-width-1-1">
                    <div class="font-large">
                        Лицевой счёт № {{ $about->invoice_num }}
                    </div>
                </div>

                <div class="uk-child-width-expand@m uk-margin-top uk-grid" uk-grid>
                    <div>
                        <div class="uk-flex uk-flex-middle uk-margin-bottom">
                            <div class="uk-width-1-4 color-grey uk-margin-small-right">
                                ФИО:
                            </div>
                            <div class="uk-width-2-4">
                                {{ $about->last_name }} {{ $about->first_name }} {{ $about->middle_name }}
                            </div>
                        </div>
                        <div class="uk-flex uk-flex-middle uk-margin-bottom">
                            <div class="uk-width-1-4 color-grey uk-margin-small-right">
                                Адрес:
                            </div>
                            <div class="uk-width-2-4">
                                ул. {{ $about->street_name }}, {{ $about->building_number }}
                            </div>
                        </div>
                    </div>

                    <div>
                        <div class="uk-flex uk-flex-middle uk-flex-middle uk-margin-bottom">
                            <div class="color-grey uk-margin-small-right uk-width-1-4 ">
                                e-mail:
                            </div>
                            <div class="uk-width-2-4 uk-flex uk-flex-between">
                                <input name="email" class="item__edit input-default fit uk-width-2-3 uk-margin-small-right" operand="email" data-mask="email"
                                       data-old="{{ $about->email }}" value="{{ $about->email }}"
                                       disabled/>
                                <div class="profile-user-item-icon">
                                    <button id="email" class="button button-transparent button-small font-small usr-padding-xs" data-mode="life_edit">Изменить</button>
                                </div>
                            </div>
                        </div>
                        @php
                            $count = 1
                        @endphp
                        @foreach ($about->phones as $phone)
                            @if($phone != '')
                                <div class="uk-flex uk-flex-middle uk-margin-bottom">
                                    <div class="uk-width-1-4 color-grey uk-margin-small-right">
                                        Телефон:
                                    </div>
                                    <div class="uk-width-2-4 uk-flex uk-flex-between">
                                        <input class="item__edit input-default fit uk-width-2-3 uk-margin-small-right " operand="phone_{{ $count }}" name="phone" data-mask="phone" type="phone"
                                               data-old="{{ $phone }}" value="{{ $phone }}" disabled/>

                                        <div class="profile-user-item-icon">
                                            <button id="phone_{{ $count }}" class="button button-transparent button-small font-small usr-padding-xs phone" data-mode="life_edit">Изменить</button>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @php
                            $count++
                        @endphp
                        @endforeach
                        @if (count($about->phones) < 5)
                            <div class="uk-flex uk-flex-middle uk-margin-bottom">
                                <div class="uk-width-1-4 color-grey uk-margin-small-right">
                                    Телефон:
                                </div>
                                <div class="uk-width-2-4 uk-flex uk-flex-between">
                                    <input class="item__edit input-default fit uk-width-2-3 uk-margin-small-right" operand="phone_new" name="phone" data-mask="phone" type="phone"
                                           data-old="" value="" disabled/>

                                    <div class="profile-user-item-icon">
                                        <button id="phone_new" class="button button-transparent button-small font-small usr-padding-xs phone" data-mode="life_edit">Добавить</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <hr class="uk-divider-icon uk-margin-medium-top uk-margin-large-bottom">

            <div class="profile-container uk-grid-small@s" uk-grid>
                <div class="uk-width-1-2@m uk-width-1-1@s">
                    <div class="uk-flex uk-flex-middle uk-flex-between">
                        <div class="font-medium">
                            @if(count($peoples) != 0)
                                Проживает
                            @else
                                <span class="color-grey">Вы не добавили
                                    <span class="color-gold"> проживающих</span>
                                </span>
                            @endif
                        </div>

                        @if (Auth::user()->role != 4)
                            <button class="button button-icon button-fit" form="peoples" data-mode="create">
                                <img src="img/icon/admin/plus.svg"/>Добавить
                            </button>
                        @endif
                    </div>


                    @if(count($peoples) != 0)
                        <table class="uk-table table-permition" table="peoples">
                            <thead class="table-header">
                            <th>Действие</th>
                            <th>ФИО</th>
                            <th>Родство</th>
                            <th>День рождения</th>
                            </thead>
                            <tbody>
                            @foreach ($peoples as $people)
                                <tr class="table-item" id="{{ $people->id }}">
                                    <td>
                                        @if (Auth::user()->role != 4)
                                            <div class="uk-flex">
                                                <button class="button-transparent button-icon" action="peoples"
                                                        data-mode="delete">
                                                    <img src="/img/icon/admin/street/delete.svg"></button>

                                                <button class="button-transparent button-icon" action="peoples"
                                                        data-mode="edit">
                                                    <img src="/img/icon/edit.svg"></button>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div>{{ $people->last_name }}</div>
                                        <div>{{ $people->first_name }} {{ $people->middle_name }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $people->kin }}</div>
                                    </td>
                                    <td>
                                        <div> {{ $people->birthday }} </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table table-permition table-blank" table="people">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            </thead>
                            <tbody>
                            @for ($i = 1; $i < 4; $i++)
                                <tr>
                                    <td>
                                        <div class="table-blank__item"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                    <td>
                                        <div class="table-blank__item table-blank__item-m"></div>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    @endif
                </div>

                <div class="uk-width-1-2@m uk-width-1-1@s">

                    <div class="uk-flex uk-flex-middle uk-flex-between">
                        <div class="font-medium">
                            @if(count($cars) != 0)
                                Постоянный пропуск
                            @else
                                <span class="color-grey">Вы не добавили
                                    <span class="color-gold"> автомобили</span>
                                </span>
                            @endif
                        </div>

                        @if (Auth::user()->role != 4)
                            <button class="button button-icon button-fit" form="cars" data-mode="create">
                                <img src="img/icon/admin/plus.svg"/>Добавить
                            </button>
                        @endif
                    </div>

                    @if(count($cars) != 0)
                        <table class="uk-table table-permition" table="cars">
                            <thead class="table-header">
                            <th>Действие</th>
                            <th>Марка</th>
                            <th>Модель</th>
                            <th>Номер</th>
                            </thead>
                            <tbody>
                            @foreach ($cars as $car)
                                <tr class="table-item" id="{{ $car->id }}">
                                    <td>
                                        @if (Auth::user()->role != 4)
                                            <div class="uk-flex">
                                                <button class="button-transparent button-icon" action="cars"
                                                        data-mode="delete">
                                                    <img src="/img/icon/admin/street/delete.svg"></button>

                                                <button class="button-transparent button-icon" action="cars"
                                                        data-mode="edit">
                                                    <img src="/img/icon/edit.svg"></button>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div>{{ $car->mark }}</div>
                                    </td>
                                    <td>
                                        <div>{{ $car->model }}</div>
                                    </td>
                                    <td>

                                        <div>{{ $car->number }}</div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table table-permition table-blank" table="cars">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item"></div>
                            </th>
                            </thead>
                            <tbody>
                            @for ($i = 1; $i < 4; $i++)
                                <tr>
                                    <td>
                                        <div class="table-blank__item"></div>
                                    </td>
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    @endif
                </div>

                {{--
                <div class="uk-width-1-3@m uk-width-1-1@s">
                    <div class="uk-flex uk-flex-between uk-flex-middle">
                        <div class="font-medium">
                            @if(count($cars) != 0)
                                Автомобили
                            @else
                                <span class="color-grey">Вы ещё не добавили
                                    <span class="color-gold"> личные автомобили</span>
                                </span>
                            @endif
                        </div>
                        @if (Auth::user()->role != 4)
                            <button class="button button-icon button-fit" form="cars" data-mode="create">
                                <img src="img/icon/admin/plus.svg"/>
                                Добавить
                            </button>
                        @endif
                    </div>

                    @if(count($cars) != 0)
                        <table class="uk-table table-permition" table="cars">
                            <thead class="table-header">
                            <th></th>
                            <th>Номер</th>
                            <th>Марка и модель</th>
                            </thead>
                            <tbody>
                            @foreach ($cars as $car)
                                <tr class="table-item" id="{{ $car->id }}">
                                    <td>
                                        @if (Auth::user()->role != 4)
                                            <div>
                                                <button class="button-transparent button-icon" action="cars"
                                                        data-mode="delete">
                                                    <img src="/img/icon/admin/street/delete.svg">
                                                </button>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        <div>{{ $car->number }}</div>
                                    </td>
                                    <td>{{ $car->mark }} {{ $car->model }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <table class="uk-table table-permition table-blank" table="cars">
                            <thead class="table-header">
                            <th>
                                <div class="table-blank__item table-blank__item-m"></div>
                            </th>
                            <th>
                                <div class="table-blank__item table-blank__item"></div>
                            </th>
                            </thead>
                            <tbody>
                                @for ($i = 1; $i < 4; $i++)
                                    <tr>
                                        <td>
                                            <div class="table-blank__item table-blank__item-m"></div>
                                        </td>
                                        <td>
                                            <div class="table-blank__item-m"></div>
                                            <div class="table-blank__item"></div>
                                        </td>
                                    </tr>
                                @endfor
                            </tbody>
                        </table>
                    @endif
                </div>
                --}}
                @include('profile.perform')
            </div>
        </div>
    </main>
@stop
