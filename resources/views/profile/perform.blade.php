<form action="{{ route('peoples.store') }}" class="hidden-form uk-padding uk-flex uk-flex-column peoples-edit_form" focus-input="first" title="Новый пропуск" table="peoples">
    <div class="material-input">
        <label class="material-input_label" for="kin">Родство</label>
        <select class="material-input_text" id="kin" name="kin">
            <option value="Мать">Мать</option>
            <option value="Отец">Отец</option>
            <option value="Муж">Муж</option>
            <option value="Жена">Жена</option>
            <option value="Брат">Брат</option>
            <option value="Сестра">Сестра</option>
            <option value="Дедушка">Дедушка</option>
            <option value="Бабушка">Бабушка</option>
            <option value="Сын">Сын</option>
            <option value="Дочь">Дочь</option>
            <option value="Внук">Внук</option>
            <option value="Внучка">Внучка</option>
        </select>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="peoples-last_name">Фамилия</label>
        <input class="material-input_text" name="last_name" id="peoples-last_name" data-input="capitalize" lang="ru" required type="text" autofocus/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="kin-first_name">Имя</label>
        <input class="material-input_text" name="first_name" id="peoples-first_name" data-input="capitalize" lang="ru" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="middle_name">Отчество</label>
        <input class="material-input_text" name="middle_name" id="middle_name" data-input="capitalize" lang="ru" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="birthday">Дата рождения</label>
        <input class="material-input_text" name="birthday" data-mask="birthday" required class="birthday"/>
    </div>
    <input name="close_date" type="hidden" id="guest-close_date" required value="3000-01-01"/>
    <input name="personal" id="people-personal" type="hidden" value="1">
    <button class="uk-margin-large-top button" type="button" data-action="permition-create">Оформить</button>
</form>

<form action="{{ route('cars.store') }}" class="hidden-form uk-padding uk-flex uk-flex-column cars-edit_form" focus-input="first" title="Новый пропуск" table="cars">
    {{ csrf_field() }}
    <input name="id" type="hidden" value=""/>
    <input name="databaseId" type="hidden" value="2"/>
    <div class="number-template uk-grid-small uk-margin-medium-top" uk-grid>
        <div class="uk-width-auto">
            <select class="number-template__mask" name="number_mask">
              <option value="russian">RU</option>
              <option value="dpr">DPR</option>
              <option value="ukrainian">UA</option>
              <option value="armenian">AM</option>
            </select>
        </div>
        <div class="uk-width-expand">
            <div class="number-template__input material-input">
                <label class="material-input_label" for="number">Номер машины</label>
                <!-- data-mask="length_limit" -->
                <input class="material-input_text number_car" name="number" id="number" error-place="number-template" data-input="uppercase" data-mask="russian" nospace required/>
            </div>
        </div>
        <div class="uk-width-auto">
            <div class="number-template__item" template="russian" default="A000AA00" style="display: block;">A000AA00</div>
            <div class="number-template__item" template="dpr" default="A000AA">A000AA</div>
            <div class="number-template__item" template="ukrainian" default="AA0000AA">AA0000AA</div>
            <div class="number-template__item" template="armenian" default="00AA000">00AA000</div>
        </div>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="mark">Марка</label>
        <input class="material-input_text" name="mark" id="mark" data-input="uppercase" nospace data-input="capitalize" required type="text"/>
    </div>
    <div class="material-input">
        <label class="material-input_label" for="model">Модель</label>
        <input class="material-input_text" name="model" id="model" data-input="uppercase" nospace type="text"/>
    </div>
    <input name="close_date" type="hidden" id="guest-close_date" required value="3000-01-01"/>
    <input name="personal" id="cars-personal" type="hidden" value="1">
    <button class="uk-margin-large-top button" type="button" formtarget="_blank" data-action="permition-create">Оформить</button>
</form>
