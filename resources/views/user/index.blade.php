@extends('app')

@section('content')
    @php
        $start_color = 'green';
        $end_color = 'green';

        /**Вычисление цвета блока в зависимости от баланса
            Задолженность красным, переплата зелёным
        */

        /*if (isset($balance->StartBalance) and $balance->StartBalance > 0){
            $start_color = 'red';
        }

        if (isset($balance->EndBalance) and $balance->EndBalance > 0){
            $end_color = 'red';
        }*/

    @endphp
    <main class="uk-flex uk-flex-middle uk-padding uk-background-default">
          <div class="uk-container uk-padding-remove uk-margin-top"  data-view="balance">
              {{ csrf_field() }}
              <div class="uk-text-lead uk-margin-bottom">Лицевой счет №
                  <span class="IdAccount"><span uk-spinner></span></span>
              </div>
              <div uk-grid>
                  <div class="uk-width-3-4@m">
                      <div class="balance">
                          <div class="balance__item">
                              <div>Баланс <span class="color-grey font-small">на {{ date('1.m.Y') }}</span></div>
                              <div class="StartBalance">
                                  <span class="font-medium" uk-spinner></span>
                              </div>
                          </div>
                          <div class="balance__item">
                              <div>Начислено</div>
                              <div class="Income"><span class="font-medium" uk-spinner></span></div>
                              {{--<div><span class="font-medium">@isset($balance->Income) {{ $balance->Income }} @endisset @empty($balance->Income) {{ "Нет данных" }} @endempty</span><span class="color-grey"> руб.</span></div>--}}
                          </div>
                          <div class="balance__item">
                              <div>Оплачено</div>
                              <div class="Outcome"><span class="font-medium" uk-spinner></span></div>
                          </div>
                          <div class="balance__item">
                              @php
                                  $yesterday = strtotime("-1 day");
                                  $date = date("d.m.Y", $yesterday);
                              @endphp
                              <div>Текущий баланс <span class="color-grey font-small">на {{ $date }}</span></div>
                              <div class="EndBalance">
                                  <span class="font-medium" uk-spinner></span>
                              </div>
                          </div>
                          <div class="balance__item">
                              <a class="color-gold" href="/invoices">История начислений</a>
                          </div>
                          <div class="balance__item">
                              <div class="uk-alert-warning" uk-alert>
                                  <p>Настоящим уведомляем, что оплаты, проведенные через устройства приема электронных платежей, зачисляются на расчетный счет ООО "КП "Донской" до семи дней.</p>
                              </div>
                          </div>
                      </div>
                  </div>
                  @if (Auth::user()->role == 3)
                      <div class="uk-width-1-4@m">
                          <div class="solid-box balance-pay">
                              <div class="uk-text-lead">Пополнение счёта</div>
                                  <div>Комиссия: <span class="color-green" id="commission"></span></div>
                                  <div>К зачислению: <span class="color-green" id="sum_with_commission"></span></div>
                                  <input class="input-default" type="number" step="0.01" name="sum" placeholder="Сумма пополнения"/>
                                  <a href="invoices/pdf" class="receipt_link color-gold" target="_blank">Скачать квитанцию</a>
                                  <button class="button" data-type="pay">Оплатить онлайн</button>
                                  <small class="color-grey">
                                    Комиссия взимается с клиента 1,2%,согласно заявления оферты о предоставлении услуги Интернет-эквайринг ПАО Промсвязьбанк.
                                </small>
                          </div>
                      </div>
                  @endif
              </div>
          </div>
    </main>
@stop
