@extends ('app')

@section ('content')
    <main class="uk-background-default uk-padding">
        <div class="uk-container uk-padding-remove uk-margin-top">
            <div class="uk-width-1-1 uk-text-lead uk-margin-bottom">Пользователи</div>
            <div class="" uk-grid>
                <div class="uk-width-1-3@m uk-width-1-1@s street-container">
                    <ul class="street-list uk-list">
                        {{--ВСТАВИТЬ УЛИЦЫ--}}
                        @foreach ($streets as $street)
                            <a href="{{ route("users.index",['street'=>$street->id]) }}">
                                <li class="street-list_item @if( isset($active_street) and $street->id == $active_street) active @endif"
                                    id="{{ $street->id }}" map_id="{{ $street->map_id }}">
                                    {{ $street->name }}
                                </li>
                            </a>
                        @endforeach
                    </ul>
                    <button class="button button-icon" data-mode="create">
                        <img src="img/icon/admin/plus.svg">Создать пользователя
                    </button>
                </div>

                <div class="uk-width-2-3@m uk-width-1-1@s uk-padding uk-padding-remove-top uk-padding-remove-right uk-overflow-auto table-container user_list">
                    {{--USER LIST--}}
                    <table class="uk-table uk-table-divider uk-table-hover table-border-left" table="users">
                        <thead class="table-header">
                        <th>Пользователь</th>
                        <th>Лиц.счёт</th>
                        <th>Телефон</th>
                        <th>Почтовый ящик</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr class="table_item" id={{ $user->id }}>
                                <td>
                                    <div>{{ $user->last_name }} {{ $user->first_name }} {{ $user->middle_name }}</div>
                                    <div>ул.{{ $user->street_name }}, {{ $user->building_number }}
                                        @if ($user->apart_number)
                                            {{'кв. '}}{{ $user->apart_number }}
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    {{ $user->invoice_num }}
                                </td>
                                <td>
                                    {{ $user->phones }}
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td>
                                    <div class="button-group">
                                        <button class="button button-transparent button-icon" data-mode="edit"><img
                                                    src="img/icon/admin/street/edit.svg"></button>
                                        <button class="button button-transparent button-icon" data-mode="delete"><img
                                                    src="img/icon/admin/street/delete.svg"></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->appends(['street'=>$active_street])->links() }}
                </div>
            </div>
        </div>
    </main>

    <form class="hidden-form uk-padding uk-flex uk-flex-column users-create_form" method="POST" title="Регистрация" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="material-input">
            <label for="role" class="material-input_label">Роль пользователя</label>
            <select class="material-input_text" id="role" name="role" onchange="showLines(this.value)">
                <option value="1">Руководитель</option>
                <option selected value="4">Пользователь</option>
                {{--<option value="5">Работник</option>--}}
            </select>
        </div>
        <div class="material-input" data-block="auth_block">
            <label class="material-input_label" for="name">Логин</label>
            <input class="material-input_text" name="name" id="name" type="text" required autofocus/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="password">Пароль</label>
            <input class="material-input_text" name="password" id="password" type="password" pattern="^(\w+){3,32}$"
                   required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="password-confirm">Подтвердите пароль</label>
            <input class="material-input_text" id="password-confirm" type="password" name="password_confirmation"
                   required>
        </div>

        <div class="material-input">
            <label class="material-input_label" for="last_name">Фамилия</label>
            <input class="material-input_text" name="last_name" id="last_name" data-input="name" type="text" required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="first_name">Имя</label>
            <input class="material-input_text" name="first_name" id="first_name" data-input="name" type="text"
                   required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="middle_name">Отчество</label>
            <input class="material-input_text" name="middle_name" id="middle_name" data-input="name" type="text"
                   required/>
        </div>
        <div class="material-input" data-view="4">
            <label for="invoice_num" class="material-input_label">Лицевой счет</label>
            <input name="invoice_num" class="material-input_text" id="invoice_num"/>
        </div>

        <div class="material-input" data-view="4">
            <label for="building_id" class="material-input_label"></label>
            <select id="building_id" class="material-input_text" person="true" type="text" name="building_id">
                <option selected value="0">Свободные дома</option>

                @foreach ($buildings as $build)
                    <option value="{{ $build->id }}"> ул.{{ $build->street_name }}
                        {{ $build->building_number }}
                        @if ($build->apart_number)
                            {{ 'кв. ' }}{{ $build->apart_number }}
                        @endif
                    </option>
                    @endforeach
            </select>
        </div>
        <button class="button uk-margin-top" data-action="register" type="submit">Записать</button>

    </form>

    <form action="{{ route('users.store') }}" class='hidden-form uk-padding uk-flex uk-flex-column users-edit_form' title='Редактирование'>
        <input name="id" id="id" type="hidden" value="">
        <div class="material-input">
            <label class="material-input_label" for="edit-last_name">Фамилия</label>
            <input class="material-input_text" name="last_name" id="edit-last_name" data-mask="name" type="text" required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="edit-first_name">Имя</label>
            <input class="material-input_text" name="first_name" id="edit-first_name" data-mask="name" type="text" required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="edit-middle_name">Отчество</label>
            <input class="material-input_text" name="middle_name" id="edit-middle_name" data-mask="name" type="text" required/>
        </div>
        <div class="material-input">
            <label class="material-input_label" for="edit-invoice_num">Лицевой счет</label>
            <input class="material-input_text" name="invoice_num" id="edit-invoice_num"/>
        </div>

        <div class="material-input" data-view="4">
            <label for="edit-building_id" class="material-input_label"></label>
            <select id="edit-building_id" class="material-input_text" person="true" type="text" name="building_id">
                <option id="selected-building" value=""></option>
                @foreach ($buildings as $build)
                    <option value="{{ $build->id }}"> ул.{{ $build->street_name }}
                        {{ $build->building_number }}
                        @if ($build->apart_number)
                            {{ 'кв. ' }}{{ $build->apart_number }}
                        @endif
                    </option>
                @endforeach
            </select>
        </div>
        <button class="button uk-margin-large-top" data-mode="send" type="button">Записать</button>
    </form>
@stop
