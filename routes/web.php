<?php

Artisan::call('view:clear');

// Auth
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/', 'Auth\LoginController@index')->name('main');

// News
// $router->resource('news', 'NewsController')->middleware('admin');
// Route::post('news/remove', 'NewsController@remove')->middleware('admin');
// Route::get('news/{news}/edit','NewsController@edit')->name('news.edit');

// Contacts
Route::post('contacts/remove', 'ContactController@remove')->middleware('admin');
$router->resource('contacts', 'ContactController')->middleware('admin');

// Streets
$router->resource('streets', 'StreetController')->middleware('admin');

// Buildings
$router->resource('buildings', 'BuildingController')->middleware('admin');
Route::post('buildings/remove', 'BuildingController@remove')->middleware('admin');

// Users
Route::get('users/balance', 'UsersController@getBalance')->middleware('auth', 'admin_user');
Route::get('users/street/{street}', 'UsersController@street')->middleware('admin');
Route::post('users/remove', 'UsersController@remove')->middleware('admin');
Route::post('users/phone', 'UsersController@handlerConfirmationPhone')->middleware('auth');
Route::post('users/phone-confirm', 'UsersController@confirmPhone')->middleware('auth');
Route::post('users/email', 'UsersController@handlerConfirmationEmail');
Route::get('users/email-confirm/{token}', 'UsersController@confirmEmail')->name('users.email-confirm');
Route::post('users/unbind', 'UsersController@unbind')->middleware('admin');
$router->resource('users', 'UsersController')->middleware('admin');

// Autos
// Route::post('autos/remove', 'AutoController@remove')->middleware('auth', 'admin_user');
// Route::post('autos/change', 'AutoController@change')->middleware('auth', 'admin_user');
// $router->resource('autos', 'AutoController')->middleware('auth', 'admin_user');

// Guests
// Route::post('guests/remove', 'GuestController@remove')->middleware('auth', 'admin_user');
// Route::post('guests/change', 'GuestController@change')->middleware('auth', 'admin_user');
// $router->resource('guests', 'GuestController')->middleware('auth', 'admin_user');

// Cars
Route::post('cars/remove', 'CarController@remove')->middleware('auth', 'admin_user');
Route::post('cars/change', 'CarController@change')->middleware('auth', 'admin_user');
$router->resource('cars', 'CarController')->middleware('auth', 'admin_user');

// Peoples
Route::post('peoples/remove', 'PeopleController@remove')->middleware('auth', 'admin_user');
Route::post('peoples/change', 'PeopleController@change')->middleware('auth', 'admin_user');
$router->resource('peoples', 'PeopleController')->middleware('auth', 'admin_user');

// Profiles
$router->resource('profiles', 'ProfileController')->middleware('auth', 'user');
Route::get('profiles', 'ProfileController@index')->name('profiles.index')->middleware('auth', 'user');
Route::post('kins/remove', 'ProfileController@remove')->middleware('auth', 'user');

// Indications (on home page icons electricity and water)
Route::get('indications/month/{month}', 'IndicationController@month')->middleware('admin');
Route::post('indications/{indication}', 'IndicationController@update')->name('indications.update')->middleware('admin');
$router->resource('indications', 'IndicationController')->middleware('admin');

// Information
Route::get('informations/{sys_id}', 'InformationController@index')->middleware('auth');

// Indications
Route::get('indications/month/{month}','IndicationController@month')->middleware('admin');
Route::post('indications/{indication}','IndicationController@update')->name('indications.update')->middleware('admin');
$router->resource('indications','IndicationController')->middleware('admin');

// Polls
$router->resource('polls', 'PollController')->middleware('admin');
Route::post('polls/insert', 'PollController@insert');

// Additionals
Route::post('additionals/store', 'AdditionalController@store')->name('additionals.store');
Route::post('additionals/pay', 'AdditionalController@pay')->name('additional_pay');
Route::get('additionals/pdf', 'AdditionalController@makePdfAct');
Route::post('additionals/pay-unpaid', 'AdditionalController@payUnpaid');
Route::get('additionals/list', 'AdditionalController@lists');
Route::post('additionals/send/act', 'AdditionalController@sendAct')->name('additionals_send_act');
$router->resource('additionals', 'AdditionalController');

Auth::routes();
Route::post('password/change', 'UsersController@changePassword')->middleware('user');

// Access
Route::get('permitions', 'PermitionController@index')->name('permitions')->middleware('auth', 'admin_user');
Route::get('permitions/list', 'PermitionController@list')->name('permitions_list')->middleware('auth', 'admin_security');
Route::get('permitions/list/check', 'PermitionController@check');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth', 'common');

// Invoices
Route::group(['prefix' => 'invoices', 'middleware' => 'auth', 'middleware' => 'user'], function() {
    Route::get('/', 'InvoiceController@index');
    Route::get('period', 'InvoiceController@getAllByPeriod');
    Route::get('pdf', 'InvoiceController@showInPdf');
});

// Crons
Route::group(['prefix' => 'crons'], function() {
    Route::post('payments/confirm', 'CronController@confirmPayments'); 
    Route::post('users/add-counteragent-key', 'CronController@addCounteragentKey');
    Route::post('payments/mark-unpaid', 'CronController@markUnpaidPayments');
    Route::post('receipt/email', 'CronController@sentEmailReceiptPdf');
    Route::post('receipt/reset', 'CronController@resetReceipt');
    Route::post('cars/delete-expired-permition', 'CronController@deleteCarsWithExpiredPermition');
    // Route::get('test', 'CronController@test');
});

// Payments
Route::group(['prefix' => 'payments'], function() {
    Route::post('/', 'PaymentController@store')->middleware('auth', 'user');
    Route::post('handler', 'PaymentController@handler');
    // метод для тестов разной хуйни
    Route::get('cashbox', 'PaymentController@cashbox')->middleware('auth', 'user');
});

$router->resource('receipt', 'ReceiptController');

// Logs
Route::get('logs', 'LogController@index')->name('logs')->middleware('auth', 'admin');

// Route::get('streets','StreetController@index')->name('streets.index');